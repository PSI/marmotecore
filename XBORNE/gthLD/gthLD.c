/* 
GTH en matrice pleine au format Xborne en entree
needs .Rxx matrix and .sz size file
version long double modifiee pour lire toutes 
les matrices Rxx
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>



void  usageGthLD()
{
    printf("usage : Gth -f filename Suffix \n");
    printf("filename.Suffix and filename.sz must exist before. And the suffix must be Rxx \n");
    exit(1);
}

void  ProblemeMemoireGthLD()
{
    printf("Sorry, not enougth memory for the matrix and the probability vectors \n");
    exit(1);
}

/* The procedure that applies the GTH method to compute the stationary
   distribution. Produces a vector pi which is allocated inside the 
   procedure.
*/
long double* doGTH( long double **P, int NSommets )
{
  int i;
  int j;
  int k;
  int n;
  long double S;
  long double Tot;
  long double *pi;

  pi = (long double *)malloc(NSommets*sizeof(long double));
  if ( NULL == pi )
    ProblemeMemoireGthLD();

  for (n = NSommets-1; n >= 1; n--) {
    S = 0.0L;
    for (j = 0; j < n; j++) {
      S +=(*P)[n*NSommets+j];
      }
    if (S == 0.0) {
      printf("Probleme en %d %Lf\n", n,S);
    exit(1);
    }
      for (i = 0; i < n; i++)
      (*P)[i*NSommets+n] /= S;
    for (i = 0; i < n; i++) {
      for (j = 0; j < n; j++)
        (*P)[i*NSommets+j] += (*P)[i*NSommets+n] * (*P)[n*NSommets+j];
    }
  }
  Tot = 1.0L;
  pi[0] = 1.0L;
  for (j = 1; j < NSommets; j++) {
    pi[j] = (*P)[j];
    for (k = 1; k < j; k++)
      pi[j] += pi[k] * (*P)[k*NSommets+j];
    Tot += pi[j];
  }
  for (j = 0; j < NSommets; j++)
    pi[j] /= Tot;

  printf("Done GTHLD Row\n");

  return pi;
}

/* Procedure to read the model in the Xborne format.
   Initializes the matrix in the (linear) array P, and the dimension
   of the state space in the variable NSommets.
 */
void readModelGTH( char* modelName, char* suffix, long double **P, int *NSommets )
{

  int NArcs;
  int i, j, c, jx, degre;
  long double x;
  FILE *pf1;
  char s0[20];
  char s1[20];
  char s2[20];

  printf("1 : %s,   2: %s \n",modelName,suffix);

/*
On recupere le nom du modele et le suffix
*/

        strcpy(s0,modelName);
        strcpy(s1,suffix);
	/* on ajoute .sz, et le suffixe */
	strcpy(s2,s0);
	strcat(s2,".sz");
	printf( "****1\n");
	printf( "s2: %s\n",s2);
	/* on verifie si il existe un fichier .sz*/
	if ((pf1=fopen(s2,"r"))==NULL)
	  {
	    usageGthLD();
	  }
	else {
	  fclose(pf1);
	}
	printf( "****2\n");

	/* on verifie si le suffixe est Rxx */
	if (s1[0]!='R')  
	  {
	    usageGthLD();
	  }
	printf( "****3\n");

	/* on verifie que le fichier model.suffix existe */
	strcat(s0,".");
	strcat(s0,s1);
	if ((pf1=fopen(s0,"r"))==NULL)
	  {
            usageGthLD();
	  }
	else {
	  fclose(pf1);
	}



/*
On recupere les tailles dans le fichier filename.sz
*/
  pf1=fopen(s2,"r");
  fscanf(pf1,"%d\n", &NArcs);
  fscanf(pf1,"%d\n", NSommets);
  fclose(pf1);	
  printf("%d\n", NArcs);
  printf("%d\n", *NSommets);

/* 
On cree les objets ou on exit(1) en cas de pb
*/

  if (!(*P=(long double *)calloc((*NSommets)*(*NSommets), sizeof(long double)))) ProblemeMemoireGthLD();
  
/*
Lecture de la matrice dans le fichier filename.dt
*/
  pf1=fopen(s0,"r");
  for (i = 0; i < (*NSommets); i++) {
  /* 
  on commence par le numero et le degre
  */
    fscanf(pf1,"%d", &c);
    fscanf(pf1,"%d", &degre);
    for (j = 0; j < degre; j++) {
      fscanf(pf1,"%Lf", &x);
      fscanf(pf1,"%d", &jx);
/*  printf("Lect %ld %ld %ld %lf \n", c,degre, jx, x);  */
      (*P)[c*(*NSommets)+jx]=x; /* P[c,jx]=x */
    }
    getc(pf1);
  }
  fclose(pf1);
}

/* Call to the GTH procedure and write the result in a ".pi" file
 */
void mainGthLDOld(char * modelName, char * suffix, int write )
{
  int i;
  int NSommets;
  long double *P;
  long double *pi;
  FILE *pf1;
  char s3[20];

  readModelGTH( modelName, suffix, &P, &NSommets );

  pi = doGTH( &P, NSommets );

  /* prepare file for writing result */
  strncpy(s3, modelName, 20 );
  strncat(s3,".pi", 20 );
  printf( "s3: %s\n",s3);

  pf1=fopen(s3,"w");
  for (i = 0; i < NSommets; i++) 
    fprintf(pf1," %.18Le\n", pi[i]);
  fclose(pf1);

  /* cleanup */
  if ( NULL != P ) {
    free(P);
  }

}

/* Call to the GTH procedure without putting the result in a vector pi
 */
void mainGthLD(char * modelName, char * suffix, long double **pi,
	       int *NSommets )
{
  long double *P;

  readModelGTH( modelName, suffix, &P, NSommets );

  (*pi) = doGTH( &P, (*NSommets) );

  /* cleanup */
  if ( NULL != P ) {
    free(P);
  }

}
