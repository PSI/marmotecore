/*
Algorithme de sousrelaxation
V2 02/05/08 
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>
// #include "verif_ext_fic.h"
#include "SOR.h"

// Possible Extensions
#define DEF_EXT "Cuu"

struct element {
  int ori; /**< end node of the transition */
  double prob; /**< probability of the transition */
};

typedef struct element element;
typedef double *probavector; /* de la taille du nombre d'etats */
typedef int *indexvector; /* de la taille du nombre d'etats */
typedef element *elmtvector; /* de la taille du nombre d'arcs */

void  ProblemeMemoireSOR()
{
	printf("Sorry, not enough memory for the matrix and/or probability storage\n");
	exit(1);
}

/* Procedure to read the model in the Xborne format.
   Initializes ...
   the matrix in the (linear) array P, and the dimension
   of the state space in the variable NSommets.
 */
void readModelSOR( char* modelName,
		elmtvector *arc, indexvector *debut, indexvector *taille,
		int *size )
{
  FILE *pf1;
  char *NOMsz;
  char *NOM;
  int  NArcs;
  int  NSommets;
  int  i;
  int  j;
  int  c;
  int  ji;
  int  degre;

  NOMsz = (char*)malloc( 2*( strlen(modelName) + 5 ) );
  NOM = (char*)malloc( 2*( strlen(modelName) + 5 ) ); // 5 chars because of \0
  sprintf( NOMsz, "%s.sz", modelName );
  sprintf( NOM, "%s.Cuu", modelName );

/*
On recupere les tailles dans le fichier filename.sz
*/
  
  if ((pf1=fopen(NOMsz,"r"))==NULL) {
    perror("fopen");
    return;
  }
  fscanf(pf1,"%d\n", &NArcs);
  fscanf(pf1,"%d\n", &NSommets);
  (*size) = NSommets;
  free(NOMsz);
  fclose(pf1);	

printf("Nb arcs = %12d\n", NArcs);
printf("Nb som. = %12d\n", NSommets);
printf("Iteration accuracy = %.0e\n",maxdiff);
printf("Max number of iterations = %d\n",maxiter);
/* 
On cree les objets ou on exit(1) en cas de pb
*/

  if (!((*debut)=(int *)malloc(NSommets*sizeof(int)))) ProblemeMemoireSOR();
  if (!((*taille)=(int *)malloc(NSommets*sizeof(int)))) ProblemeMemoireSOR();
  if (!((*arc)=(element *)malloc(NArcs*sizeof(element)))) ProblemeMemoireSOR();

/*
Lecture de la matrice
*/
fprintf(stderr,"Lecture de %s\n",NOM);

 if ((pf1=fopen(NOM,"r"))==NULL) {
   perror("fopen");
   return;
 }
c = 0;

for (i = 0; i < NSommets; i++) {
    fscanf(pf1,"%d", &ji);
    fscanf(pf1,"%d", &degre );
    (*debut)[ji] = c;
    (*taille)[ji] = degre;
    for (j = 1; j <= degre; j++)  {
      fscanf(pf1,"%lg", &(*arc)[c].prob);
      fscanf(pf1,"%d", &(*arc)[c].ori);
    	c++;
    }
    getc(pf1);
}
 fclose(pf1);
 free(NOM);
}

double* doSOR( elmtvector arc, indexvector debut, indexvector taille, 
		    int NSommets )
{
  int i, j, iter;
  double diff, som, y, wb, norm;

/*
Les structures dynamiques a creer par malloc
*/
  probavector p, op, vop;
  
  if (!(p=(double *)malloc(NSommets*sizeof(double)))) ProblemeMemoireSOR();
  if (!(op=(double *)malloc(NSommets*sizeof(double)))) ProblemeMemoireSOR();
  if (!(vop=(double *)malloc(NSommets*sizeof(double)))) ProblemeMemoireSOR();

// Initialisation du vecteur op 
for (i = 0; i < NSommets; i++)  op[i] = 1.0;

// Normalisation du vecteur op  
som = 0.0;
for (i = 0; i < NSommets; i++)  som += op[i];
for (i = 0; i < NSommets; i++) {
    op[i] /= som;
    vop[i]=op[i];
}
/*
On commence la boucle sur la precision 
*/
iter = 0;
wb=1-Omega;
do {
    iter++;
//Iteration de Gauss Seidel 
    for (i = 0; i < NSommets; i++) {
	som = 0.0;
	norm = 1.0;
        for (j = debut[i]; j < debut[i] + taille[i]; j++) 
	    {
	    if (arc[j].ori != i) {
	            if (arc[j].ori > i) y = op[arc[j].ori];
	            else y = p[arc[j].ori];
	            som += y * arc[j].prob;
		}
		else norm = 1.0 - arc[j].prob;
             }
          p[i] = som/norm;
      }
/*
Sous relaxation 
*/
      for (i = 0; i < NSommets; i++) p[i] = Omega * p[i] + wb * op[i];
    
//Normalisation et test de la norme de la difference
//apres mmm etapes
      if ((iter%mmm)==0) {
	  som = 0.0;
	  for (i = 0; i < NSommets; i++) som += p[i];
	  for (i = 0; i < NSommets; i++) p[i] /= som;
#if (TestFin==0) 
	  diff = 0.0;
	  for (i = 0; i < NSommets; i++) diff += fabs(p[i] - vop[i]);
#endif
#if (TestFin==1) 
	  diff = 0.0;
	  for (i = 0; i < NSommets; i++) {
	      if (vop[i]>0) {
		  diff += fabs(p[i] - vop[i])/vop[i];
	      }
	      else {
		  diff += fabs(p[i]);
	      }
	  }
#endif
      } 
      else {diff=1.0;} 
     
printf("Iteration %d %e \n", iter,(float)diff);

     for (i = 0; i < NSommets; i++)  op[i] = p[i];
     if ((iter % mmm)==0) {
	 for (i = 0; i < NSommets; i++)  vop[i] = p[i];
     }
  } while ((iter != maxiter) && (diff >= maxdiff));

 free(op);
 free(vop);

 return p;
}

/* Call to the SOR procedure and puts the result in a vector pi
 */
void mainSOR(char* modelName, double **pi, int *NSommets,
	     int writeResult )
{
  int i;
  FILE *pf1;
  char *NOM_FIC_PI;
  elmtvector arc;
  indexvector debut;
  indexvector taille;

  readModelSOR( modelName, &arc, &debut, &taille, NSommets );

  (*pi) = doSOR( arc, debut, taille, *NSommets );

  if ( writeResult ) {
    // Ecriture du resultat
    NOM_FIC_PI = (char*)malloc( strlen(modelName) + 4 );
    if (NOM_FIC_PI == NULL) ProblemeMemoireSOR();
    sprintf( NOM_FIC_PI, "%s.pi", modelName );
    if ((pf1=fopen(NOM_FIC_PI,"w")) == NULL) {
      perror("fopen");
      fprintf(stderr,"\n");
      return;
    }
    for (i = 0; i < (*NSommets); i++) {
      fprintf(pf1,"% .20E\n", (*pi)[i]);
    }
    fclose(pf1);
    free( NOM_FIC_PI );
  }

  /* cleanup */
  if ( NULL != arc ) { free(arc);}
  if ( NULL != debut ) { free(debut);}
  if ( NULL != taille ) { free(taille);}

}
