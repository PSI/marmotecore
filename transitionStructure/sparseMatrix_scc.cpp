/* Marmote is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Marmote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Marmote. If not, see <http://www.gnu.org/licenses/>.

Copyright 2015 Alain Jean-Marie, Jean-Michel Fourneau, Jean-Marc Vincent, Issam Rabhi */
#include "../system.h"
#include "sparseMatrix.h"
#include "transitionStructure.h"

#include <algorithm>
#include <vector>
#include <queue>
#include <unistd.h>
#include <sys/time.h>


int getIndex(std::vector<int> values, int value);
int gcd(int a, int b);
int lcm(int a, int b);



/**
 * @brief Process jobs from qjob, and put results to qscc
 * @param ignore methods ignoger all thansitions with probability less or equal to ignore
 * @author Hlib Mykhailenko
 */
void sparseMatrix::sccSeeker(boost::lockfree::queue<set<int> *> * qjob,
                             boost::lockfree::queue<SCC *> * qscc,
                             sparseMatrix * sparse, sparseMatrix * reverted,
                             boost::atomic<int> * counter, double ignore) {
  bool minused = false;

  while (counter->load() > 0) {
    set<int> * vertices;
    if (qjob->pop(vertices)) {
      if (minused) {
        counter->fetch_add(1, boost::memory_order_relaxed);
        minused = false;
      }
      if (vertices->size() == 1) {
        SCC * scc = new SCC();
        scc->states = *vertices;
        delete vertices;
        scc->period = 1;
        qscc->push(scc);
      } else {
        // forward trim
        while (trimer(reverted, vertices, qscc, ignore)) {
        }

        // backward trim
        while (trimer(sparse, vertices, qscc, ignore)) {
        }

        if (vertices->size() > 1) {

          set<int>::iterator it = vertices->begin();
          int index = rand() % vertices->size();
          for (int i = 0; i < index; ++i) {
            ++it;
          }
          int pivot = (*it);

          set<int> * predSet = reverted->bfs(pivot, vertices, ignore);
          set<int> * descSet = sparse->bfs(pivot, vertices, ignore);
          set<int> * remSet = new set<int>();
          for (set<int>::const_iterator iter = vertices->begin();
              iter != vertices->end(); ++iter) {
            if (descSet->find(*iter) == descSet->end()
                && predSet->find(*iter) == predSet->end()) {
              remSet->insert(*iter);
            }
          }

          set<int> * sccStates = new set<int>();
          set_intersection(descSet->begin(), descSet->end(), predSet->begin(),
                           predSet->end(),
                           inserter(*sccStates, sccStates->begin()));
          set<int> * descSetDiff = new set<int>();
          set_difference(descSet->begin(), descSet->end(), sccStates->begin(),
                         sccStates->end(),
                         std::inserter(*descSetDiff, descSetDiff->end()));
          set<int> * predSetDiff = new set<int>();
          set_difference(predSet->begin(), predSet->end(), sccStates->begin(),
                         sccStates->end(),
                         inserter(*predSetDiff, predSetDiff->end()));

          delete predSet;
          delete descSet;
          SCC * scc = new SCC();
          scc->states = *sccStates;
          scc->period = period(this, &scc->states);
          qscc->push(scc);
          set<int> * threeSets[] = { descSetDiff, predSetDiff, remSet };
          for (int i = 0; i < 3; ++i) {
            set<int> * newVertices = threeSets[i];
            if (newVertices->size() > 0) {
              qjob->push(newVertices);
            }else{
              delete newVertices;
            }
          }
        }
      }
      delete vertices;
    } else {  // no jobs
      if (!minused) {
        counter->fetch_sub(1, boost::memory_order_relaxed);
        minused = true;
      }
    }
  }
}



int sparseMatrix::period() {
  std::pair<std::vector<SCC> *, sparseMatrix *>  sccs = sparseMatrix::getStronglyConnectedComponents(0.0);
  vector<SCC>::const_iterator it = sccs.first->begin();
  int period = it->period;
  for (; it != sccs.first->end(); ++it) {
    period = lcm(period, it->period);
  }

  return period;
}

/**
 * @brief Implementation of ModifiedDCSC algorithm from "Finding Strongly Connected Components in Distributed Graphs".
 * paper: http://www.sandia.gov/~sjplimp/papers/jpdc05.pdf
 * @param ignore methods ignoger all thansitions with probability less or equal to ignore
 * @author Hlib Mykhailenko
 */
std::pair<std::vector<SCC> *, sparseMatrix *> sparseMatrix::getStronglyConnectedComponents(double ignore) {

  if ((vector<SCC> *) NULL == _sccs.first ||
      (sparseMatrix *)NULL == _sccs.second) {
    clearComputedSCCs();


    set<int> * ids = new set<int>();
    for (int i = 0; i < origSize(); ++i) {
      ids->insert(i);
    }

    sparseMatrix * reverted = this->getReverted();

    boost::lockfree::queue<set<int> *> * qjob = new boost::lockfree::queue<
        set<int> *>(1000);

    qjob->push(ids);

    boost::lockfree::queue<SCC *> * qscc = new boost::lockfree::queue<SCC *>(
        1000);

    long ncores = sysconf(_SC_NPROCESSORS_ONLN);
    int threads = 1;
    if (ncores > 2) {
      threads = ncores - 2;
    }
//    threads = 1;

    boost::atomic<int> * counter = new boost::atomic<int>(threads);
    boost::thread ** pool = new boost::thread *[threads];
    for (int i = 0; i < threads; ++i) {
      pool[i] = new boost::thread(&sparseMatrix::sccSeeker, this, qjob, qscc,
                                  this, reverted, counter, ignore);
    }

    for (int i = 0; i < threads; ++i) {
      pool[i]->join();
    }
    _sccs.first = new vector<SCC>();

    int * stateToComm = new int[origSize()];    // default constructor

    SCC * scc;
    int id = 0;
    while (qscc->pop(scc)) {
      scc->id = id++;
      _sccs.first->push_back(*scc);
      for (set<int>::iterator it = scc->states.begin(); it != scc->states.end();
          ++it) {
        stateToComm[*it] = scc->id;
      }
    }
    std::sort(_sccs.first->begin(), _sccs.first->end());
    _sccs.second = new sparseMatrix(_sccs.first->size());
    for (int from = 0; from < origSize(); ++from) {
      int n = getNbElts(from);
      for (int col = 0; col < n; ++col) {
        int to = getCol(from, col);
        double prob = getEntryByCol(from, col);
        if (prob > ignore) {
          if ((*_sccs.first)[stateToComm[from]].id != (*_sccs.first)[stateToComm[to]].id) {
            int fromSCC = (*_sccs.first)[stateToComm[from]].id;
            int toSCC = (*_sccs.first)[stateToComm[to]].id;
            _sccs.second->setEntry(fromSCC, toSCC, 1.0);
          }
        }
      }
    }
    delete counter;
    delete stateToComm;

  }
  return _sccs;
}

/**
 * @brief Trims "leafs" of markov chain.
 * @param ts contains whole transition structure of original markov chain.
 * @param vertices set of vertices which should be considered while traversine ts (this set works as a filter)
 * @param qscc each state that was found trimmed appears as SCC, and placed in qscc
 * @param ignore methods ignoger all thansitions with probability less or equal to ignore
 * @author Hlib Mykhailenko
 */
int sparseMatrix::trimer(transitionStructure * ts, set<int> * vertices,
                              boost::lockfree::queue<SCC *> * qscc,
                              double ignore) {
  int deleted = 0;
  std::set<int> todelete;
  for (set<int>::iterator it = vertices->begin(); it != vertices->end();
      ++it) {
    int v = *it;
    bool toTrim = true;
    int n = ts->getNbElts(v);
    for (int i = 0; i < n; ++i) {
      int neighbour = ts->getCol(v, i);
      if (vertices->find(neighbour) != vertices->end() && v != neighbour) {
        double prob = ts->getEntry(v, neighbour);
        if (prob > ignore) {
          toTrim = false;
          break;
        }
      }
    }
    if (toTrim) {
      ++deleted;
      todelete.insert(v);
      SCC * scc = new SCC();
      set<int> * states = new set<int>();
      states->insert(v);
      scc->states = *states;
      scc->period = 1;
      qscc->push(scc);
    }
  }
  for (set<int>::iterator iter = vertices->begin(); iter != vertices->end();) {
    if (todelete.find(*iter) != todelete.end()) {
      vertices->erase(iter++);
    } else {
      ++iter;
    }
  }

  return deleted;
}

set<int> * sparseMatrix::bfs(int pivot,
                       set<int> * vertices, double ignore) {
  std::set<int> * result = new std::set<int>();
  queue<int> container;
  set<int> neverAgain;
  container.push(pivot);
  while (!container.empty()) {
    int v = container.front();
    container.pop();
    result->insert(v);
    int n = this->getNbElts(v);
    for (int i = 0; i < n; ++i) {
      int neighbour = this->getCol(v, i);
      double prob = this->getEntryByCol(v, i);
      if ((vertices == NULL || vertices->find(neighbour) != vertices->end()) && prob > ignore
          && result->find(neighbour) == result->end()
          && neverAgain.find(neighbour) == neverAgain.end()) {
        container.push(neighbour);
        neverAgain.insert(neighbour);
      }
    }
  }
  return result;
}

void sparseMatrix::clearComputedSCCs() {
  delete _sccs.first;
  _sccs.first = (vector<SCC> *) NULL;
  delete _sccs.second;
  _sccs.second = (sparseMatrix *) NULL;
  delete _reverted;
  _reverted = (sparseMatrix *) NULL;
}



int sparseMatrix::period(transitionStructure * ts, set<int> * sccStates) {
  if (sccStates->size() <= 2) {
    return sccStates->size();
  } else {
    int period = -1;
    map<int, int> levels;
    set<int>::const_iterator it = sccStates->begin();
    int pivot = *it;
    for (; it != sccStates->end(); ++it) {
      levels[*it] = -1;
    }
    levels[pivot] = 0;
    queue<int> queue;
    queue.push(pivot);
    while (!queue.empty()) {
      int front = queue.front();
      queue.pop();
      int level = levels[front];
      int N = ts->getNbElts(front);
      for (int i = 0; i < N; ++i) {
        int neighbour = ts->getCol(front, i);
        if (sccStates->find(neighbour) != sccStates->end()) {
          double p = ts->getEntry(front, neighbour);
          if (p > 0.0) {
            if (levels[neighbour] == -1) {
              levels[neighbour] = level + 1;
              queue.push(neighbour);
            } else {
              int val = level - levels[neighbour] + 1;
              if (period == -1) {
                period = val;
              } else {
                period = gcd(period, val);
              }
              if (period == 1) {
                return 1;
              }
            }
          }
        }
      }
    }
    return period;
  }
}



sparseMatrix * sparseMatrix::newFiltered(std::vector<int> states) {
  std::sort(states.begin(), states.end());
  sparseMatrix * result = new sparseMatrix(states.size());
  for (unsigned int newId = 0; newId < states.size(); ++newId) {
    int oldId = states[newId];
    int n = this->getNbElts(oldId);
    for (int i = 0; i < n; ++i) {
      int neighbourOldId = this->getCol(oldId, i);
      unsigned int neighbourNewId = getIndex(states, neighbourOldId);
      if (neighbourNewId < states.size()) {
        double prob = this->getEntry(oldId, neighbourOldId);
        result->setEntry(newId, neighbourNewId, prob);
      }
    }
  }

  return result;
}


sparseMatrix* sparseMatrix::reverted(){
  sparseMatrix * reverted = new sparseMatrix( _origSize);

  for ( int i=0; i<_origSize; i++ ) {
      for ( int j=0; j<_nbElts[i]; j++ ) {
        int neighbour = _elts[i][j];
        if(_vals[i][j] > 0.0){
          reverted->setEntry(neighbour, i, _vals[i][j]);
        }
      }
  }

  return reverted;
}

sparseMatrix * sparseMatrix::getReverted(){
  if(_reverted == NULL){
    _reverted = reverted();
  }
  return _reverted;
}

int getIndex(std::vector<int> values, int value) {
  int pos = std::find(values.begin(), values.end(), value) - values.begin();
  return pos;
}


int gcd(int a, int b) {
  while (a && b) {
    if (a > b) {
      a = a % b;
    } else {
      b = b % a;
    }
  }
  return a + b;
}

int lcm(int a, int b) {
  return (a < 0 ? -a : a) * (b < 0 ? -b : b) / gcd(a, b);
}


