/* Marmote is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Marmote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Marmote. If not, see <http://www.gnu.org/licenses/>.

Copyright 2015 Alain Jean-Marie, Jean-Michel Fourneau, Jean-Marc Vincent, Issam Rabhi */

#include "markovChain.h"
#include <sstream>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <math.h>
#include <cstdlib>
#include <fstream>
#include "../system.h"
#include "../transitionStructure/sparseMatrix.h"
#include "../transitionStructure/transitionStructure.h"
#include "../Distribution/diracDistribution.h"
#include "../Distribution/uniformDiscreteDistribution.h"
#include <algorithm>    // std::set_intersection
#include <vector>       // std::vector



//#include "PSI/psi_alias.h"
//#include "PSI/psi_traj.h"

#include "../libXborne.h"
//#include "libPsi.h"

//#include "XBORNE/generMarkov/generMarkov.c"
//#include "XBORNE/gthLD/gthLD.c"
//#include "XBORNE/SOR/SOR.c"


#include<vector>
//#include <RInside.h>                    // for the embedded R via RInside

#ifdef WITH_R
#include <RInside.h>
using namespace Rcpp;
#endif

#define SPARSE		1
#define FULL		2

#ifdef WITH_R
RInside* markovChain::_Rmotor = (RInside*)NULL;
#else
typedef void* RInside;
typedef void* SEXP;
#endif

markovChain::markovChain( int sz, timeType t )
{
  _stateSpaceSize = sz;
  _type = t;
  _debug = false;
  _isAbstract = false;
  _abstractNbre = 0;
  _abstract = (string*)NULL;

  _generator = (transitionStructure*)NULL;
  // fprintf( stderr, "Setting generator to NULL\n");
  _initDistribution = (discreteDistribution*)NULL;
}


markovChain::markovChain( transitionStructure* tr )
{
  _stateSpaceSize = tr->size();
  _type = tr->type();
  _debug = false;
  _isAbstract = false;
  _abstractNbre = 0;
  _abstract = (string*)NULL;

  _generator = tr;
  // printf( stderr, "Setting generator to some generator with type %d\n",
  // _generator->type() );
  _initDistribution = (discreteDistribution*)NULL;
}


markovChain::markovChain(string format, string param[], int nbParam,
			 string modelName, bool isAbstract)
{
  setModelName(modelName);
  setFormat(format);
  _isAbstract = isAbstract;
  _abstractNbre = 0;
  _type = UNKNOWN;

  if( _isAbstract )
    {
      _generator = (transitionStructure*)NULL;

      // arbitrary initialization of _initDistribution
      _initDistribution = new diracDistribution( 0 );

      // ERS format
      if ( format == "Ers" ) {

	setAbstractNbre(2);
	string tmp[2] = { modelName, modelName + ".mcl" };
	setAbstract(tmp);

      }
      
      // PSI input files
      else if( format == "PSI" ) {

	// TO BE REWRITTEN !!
	setAbstractNbre(1);
	string tmp[1] = { modelName };
	setAbstract(tmp);

	setSizeType( modelName + ".marca" );

	/*
	string ext = param[0].substr(param[0].find('.'),param[0].length() 
	- param[0].find('.'));
	string fileOut= param[0].substr(0,param[0].length()-ext.length());

	if(ext==".simu")
	  {
	    _abstract[0]=param[0];
	    setAbstractNbre(1);
	  }
	else if(ext == ".hbf_r" || ext ==".hbf_c" || ext ==".marca")
	  {
	    precomputation(param[0],modelName,ext);
	    
	    _abstract[0]=modelName+".simu";
	    
	    setAbstractNbre(1);
	    
	  }
	else
	  {
	    //to do
	    cout<<"other extension";
	  }
	*/
      }
      
      // XBORNE C-language format
      else if ( format == "XBORNEPres" ) {

	setAbstractNbre(4);
	string tmp[4] = { modelName, "const.h", "var.h", "fun.c" };
	setAbstract(tmp);

      }

      // XBORNE matrix format
      else if ( format == "XBORNE" ){

	setAbstractNbre(4);
	string tmp[4] = { modelName, modelName+".Rii", modelName+".sz",
			  modelName+".cd" };
	setAbstract(tmp);

      }

    }
  else {
    // if not abstract...
    // Initializing the object to its minimal (incomplete) structure.
    _debug = true;
    _stateSpaceSize = 0;
    _generator = (transitionStructure*)NULL;
    _initDistribution = (discreteDistribution*)NULL;

#define ABORT(x) { printf("Error in markovChain: %s. Generator not initialized.\n", x ); \
  if ( _generator != (transitionStructure*)NULL ) { \
    delete _generator; }; \
  _generator = (transitionStructure*)NULL; \
  return; \
  }
#define MARMOTE_MC_WARNING(x) { printf("Warning in markovChain: %s.\n", x ); }

    if ( format == "Ers" ) {

      setFormat("Ers");

      FILE		*input;
      bool		reward = false;
      bool		readOK = true;
      double	epsilon = 1e-6;
      char		message[128];
      char		token[128];

      if ( modelName.size() == 0 )
        input = stdin;
      else {
        string fileName = modelName + ".mcl";

        if ( NULL == ( input = fopen( fileName.c_str(), "r" ) ) ) {
          sprintf( message, "could not open input file %s", fileName.c_str() );
          ABORT( message );
        }

        /* determine chain type */
        fscanf( input, "%128s", token );
        if ( ( !strcmp( token, "discrete" ) )
             || ( !strcmp( token, "1" ) ) ) {
          _type = DISCRETE;
        }
        else if ( ( !strcmp( token, "continuous" ) )
                  || ( !strcmp( token, "2" ) ) ) {
          _type = CONTINUOUS;
        }
        else {
          ABORT("could not read chain type");
        }

        if ( _debug )
          printf("Found time type = %s\n",
                 ( _type == CONTINUOUS ? "continuous" :
                                         ( _type == DISCRETE ? "discrete" :
                                                               ( _type == UNKNOWN ? "unknown" : "not set"
                                                                                    ))));

        /* find out if reward or not */
        fscanf( input, "%128s", token );
        if ( !strcmp( token, "reward" ) ) {
          reward = true;
          /* read next token */
          fscanf( input, "%128s", token );
        }

        /* determine size, or sparse indicator */
        int matrixType = SPARSE; // this is the default
        if ( !strcmp( token, "sparse" ) ) {
          matrixType = SPARSE;
          readOK = ( 1 == fscanf( input, "%d", &(_stateSpaceSize) ) );
        }
        else {
          matrixType = FULL;
          readOK = ( 1 == sscanf( token, "%d", &(_stateSpaceSize) ) );
        }

        if ( !readOK )
          ABORT("could not read chain size");

        if ( reward ) {
          MARMOTE_MC_WARNING( "Markov Reward processes not implemented. Ignored." );
        }

        switch( matrixType ) {
        case FULL:
          MARMOTE_MC_WARNING( "Full matrices not implemented. Ignored." );

        case SPARSE:
          _generator = new sparseMatrix( _stateSpaceSize );
        }

        bool allocOK = ( (sparseMatrix*)NULL != _generator );

        if ( !allocOK )
          ABORT("could not allocate memory for matrix");

        /* ignore for the time being
  if ( Matrix_Type == FULL ) {
    for ( i=0; readOK && ( i < _stateSpaceSize ); i++ ) {
      for ( j=0; readOK && ( j < _stateSpaceSize ); j++ ) {

    if ( ( _type == DISCRETE )
         || ( ( _type == CONTINUOUS ) && ( i != j ) ) ) {

      readOK = Read_Full_Matrix_Entry( input, _generator, i, j );

      if ( !readOK ) {
        sprintf( message, "failed reading matrix element (%d,%d)", i, j );
        ABORT( message );
      } else if (Debug ) {
        printf("Got entry (%d,%d) = %f\n",
           i, j, Get_Sparse_Matrix_Entry( _generator, i, j ) );
      }
    }
      }
    }
  }
  else if ( Matrix_Type == SPARSE ) {
  */
        do {
          int row;
          int col;
          double value;
          if ( 3 == fscanf( input, "%d %d %lf", &row, &col, &value ) ) {
            _generator->setEntry( row, col, value );
          }
          else
            readOK = false;

          if ( readOK && reward ) {
            /* get "wr" token, then read reward */
            fscanf( input, "%s", token );
            if ( strcmp( token, "wr" ) ) {
              MARMOTE_MC_WARNING( "expecting \"wr\" before reward but got" );
              fprintf( stderr, "%s.\n", token );

              /* Reading reward from this token... */
              if ( 1 != sscanf( token, "%lf", &value ) ) {
                snprintf( message, 128,
                          "Error in markovChain: unexpected token %s.",
                          token );
                ABORT( message );
              }
            }
            else if ( 1 != fscanf( input, "%lf", &value ) ) {
              snprintf( message, 128,
                        "Error in markovChain: could not read reward" );
              ABORT( message );
            }

            // Set_Sparse_Matrix_Entry( _Reward, row, col, value );
          }
        } while ( readOK );

        // skip stopping mark
        fscanf( input, "%s", token );

        // read initial state for the chain
        int initState;
        readOK = ( 1 == fscanf( input, "%d", &(initState) ) );
        if ( !readOK ) {
          ABORT("could not read initial state");
        }
        else {
          _initDistribution = new diracDistribution( initState );
        }

        /* now checking/adjusting the generator */
        for ( int i=0; readOK && ( i < _stateSpaceSize ); i++ ) {
          double sum = _generator->rowSum( i );

          switch( _type ) {
          case DISCRETE:
            if ( fabs( sum - 1.0 ) > epsilon ) {
              printf("Warning in markovChain: row %d does not sum up to 1.0. Dif = %f.\n",
                     i, sum - 1.0 );
            }
            break;

          case CONTINUOUS:
            _generator->setEntry( i, i, -sum );
            break;

          case UNKNOWN:
            // nothing to do
            break;
          }
        }

	fclose( input );
      }
    }
    else if ( format == "MARCA" ) {

      setFormat("MARCA");

      FILE		*input;
      char		message[128];

      if ( modelName.size() == 0 )
	input = stdin;
      else {
	string fileName = modelName + ".marca";

	if ( NULL == ( input = fopen( fileName.c_str(), "r" ) ) ) {
	  snprintf( message, 128,
		    "could not open input file %s", fileName.c_str() );
	  ABORT( message );
	}

	// determine chain dimensions
	int rowDim;
	int colDim;
	int nbTrans;

	if ( 3 != fscanf( input, "%d%d%d", &colDim, &rowDim, &nbTrans ) ) {
	  ABORT("could not read chain size");
	}

	bool allocOK = ( (sparseMatrix*)NULL != _generator );

	if ( !allocOK )
	  ABORT("could not allocate memory for matrix");

	// reading transition values
	bool readOK = true;
	bool hasNegative = false;
	for ( int i = 0; ( i < nbTrans ) && readOK; i++ ) {
	  int row;
	  int col;
	  double value;
	  if ( 3 == fscanf( input, "%d %d %lf", &row, &col, &value ) ) {
	    _generator->setEntry( row, col, value );
	    hasNegative = hasNegative || ( value < 0.0 );
	  }
	  else {
	    readOK = false;
	    char message[256];
	    snprintf( message, 256,
		      "incomplete transition list: %d read, %d expected",
		      i, nbTrans );
	    ABORT(message);
	  }
	}
	
	// initial state for the chain is 0 by convention
	int initState = 0;
	_initDistribution = new diracDistribution( initState );

	// determine chain type
	if ( !hasNegative ) {
	  _type = DISCRETE;
	} 
	else {
	  _type = CONTINUOUS;
	}
	if ( _debug ) 
	  printf("Found time type = %s\n",
		 ( _type == CONTINUOUS ? "continuous" :
		   ( _type == DISCRETE ? "discrete" :
		     ( _type == UNKNOWN ? "unknown" : "not set"
		       ))));

	fclose( input );
      }
    }
    else if ( format == "HBF" ) {

      // reader not implemented
      ABORT("reader for HBF format not implemented");

    }
    else if ( format == "XBORNEPres" ) {

      // model is in C-language. Call the MC generator, then read result
      char *gmp= (char *)(modelName.c_str());
      // Assumption: files const.h and var.h are present --> CHECK
      // Must **recompile** the genereMarkov program/object, then execute it.
      // Static call to mainGenerMarkov(gmp) will not work

      printf("Error: generation of model '%s' from format '%s' not implemented.\n",
	     gmp, format.c_str() );
      // reader not implemented...

    }
    else if ( format == "XBORNE" ) {

      // reader not implemented
      ABORT("reader for XBORNE format not implemented");

    }
    else {
      ABORT("Markov chain format unknown");
    }
  }
}

markovChain::markovChain(int mulitpleOfPeriod, int nStates) {
  int nBuckets = mulitpleOfPeriod;
  int bucketSize = nStates / nBuckets;
  int finalNStates = nBuckets * bucketSize;

  _stateSpaceSize = finalNStates;
  _type = DISCRETE;
  _debug = false;
  _isAbstract = false;
  _abstractNbre = 0;
  _abstract = (string*) NULL;
  _generator = new sparseMatrix(finalNStates);
  _initDistribution = (discreteDistribution*) NULL;

  vector < vector<int> > buckets(nBuckets, vector<int>(bucketSize));
  std::srand(unsigned(std::time(0)));
  for (int i = 0; i < nBuckets; ++i) {
    for (int j = 0; j < bucketSize; ++j) {
      int stateID = i * bucketSize + j;
      buckets[i][j] = stateID;
    }
    random_shuffle(buckets[i].begin(), buckets[i].end());
  }

  for (int i = 0; i < nBuckets; ++i) {
    int fromBucketID = i;
    int toBucketID = (i + 1) % nBuckets;
    for (int j = 0; j < bucketSize; ++j) {
      _generator->setEntry(buckets[fromBucketID][j], buckets[toBucketID][j],
                           1.0);
    }
  }
}


markovChain::~markovChain()
{
  if ( (discreteDistribution*)NULL != _initDistribution ) {
    delete _initDistribution;
  }
  if ( (transitionStructure*)NULL != _generator ) {
    delete _generator;
  }
  /*
  if ( (Distribution*)NULL != _initDistribution ) {
    delete _initDistribution;
  }
  */
  if ( _abstractNbre > 0 ) {
    delete[] _abstract;
  }
}


void markovChain::setAbstract( string abstract[] )
{
  _abstract = new string[_abstractNbre];

  for(int i=0; i<_abstractNbre;i++)
    {
      _abstract[i] = abstract[i];
    }
}


RInside* markovChain::Rmotor()
{
#ifdef WITH_R
  if ( _Rmotor == (RInside*)NULL ) {
    _Rmotor = new RInside(0,NULL);
    // _Rmotor->setVerbose(true);
  }
  return _Rmotor;
#else
  try{
    throw "Error: functionality available only when compiled with R";
  }
  catch (char const* e)
  {
    cout << "An exception occurred.  " << e
         << "." << endl;
  }
#endif
  return NULL;
}

//*****************
// Solution methods
//*****************

// Methods wrapped from the 'markovchain' package of R.

// Conversion utility

int markovChain::charVectorElt2State(SEXP elt, std::string function ) {

#ifdef WITH_R
  int val;
  const char* format = (Rcpp::as<std::string>(elt)).c_str();
  if ( 1 != sscanf( format, "%d", &val ) ) {
    fprintf( stderr,
             "Internal error in %s: '%s' does not contain a state number. Ignored.\n",
             function.c_str(), format );
    val = -1;
  }
  return val;
#else
try{

throw "Error: cannot find RInside";

}
catch (char const* e)
  {
    cout << "An exception occurred.  " << e
         << ".\n";
  }
#endif
  return 0;
}

bool markovChain::isaccessibleR( int stateFrom, int stateTo )
{
  bool result = true;

#ifdef WITH_R
  // retreive R engine, with creation if needed.
  _Rmotor = Rmotor();

  std::string txt = "suppressMessages(library(markovchain))"; // importer le package markovchain
  std::string str = toString("R");
  _Rmotor->parseEvalQ(txt);              // executer sous R sans retour
  _Rmotor->parseEvalQ(str);

  std::ostringstream tmp;
  tmp << "is.accessible(mcA,\"" << stateFrom << "\",\"" << stateTo << "\")";
  std::string cmd = tmp.str();
  result=_Rmotor->parseEval(cmd);

#else

  throw "markovChain::isaccessibleR requires that project was compiled using WITH_R macros.";

#endif

  return result;
}

bool markovChain::isaccessible(int stateFrom, int stateTo) {

  sparseMatrix * sparse = dynamic_cast<sparseMatrix *>(_generator);
  if (!sparse) {
    throw "Error: stronglyConnectedComponents works only with sparseMatrix as a transitionStructure";
  }
  std::pair<std::vector<SCC> *, sparseMatrix *> _sccs = sparse->getStronglyConnectedComponents();
  int communityFrom = -1;
  int communityTo = -1;
  for(std::vector<SCC>::iterator it = _sccs.first->begin(); it != _sccs.first->end(); ++it){
    if(it->states.find(stateFrom) != it->states.end()){
      communityFrom = it->id;
    } else if(it->states.find(stateTo) != it->states.end()){
      communityTo = it->id;
    }
  }
  if(communityFrom == -1 || communityTo == -1){
    return false;
  }else if(communityFrom == communityTo){
    return true;
  }else{
    set<int> * descSet = _sccs.second->bfs(communityFrom);
    bool answer = descSet->find(communityTo) != descSet->end();
    delete descSet;
    return answer;
  }
}


bool  markovChain::isirreducibleR()
{
  bool result = true;

#ifdef WITH_R
  // retreive R engine, with creation if needed.
  _Rmotor = Rmotor();

  std::string txt = "suppressMessages(library(markovchain))"; // importer le package markovchain
  std::string str = toString("R");
  _Rmotor->parseEvalQ(txt);              // executer sous R sans retour
  _Rmotor->parseEvalQ(str);

  result=_Rmotor->parseEval("is.irreducible(mcA)");

#else

  throw "markovChain::isirreducibleR requires that project was compiled using WITH_R macros.";

#endif

  return result;
}

bool  markovChain::isirreducible()
{
  sparseMatrix * sparse = dynamic_cast<sparseMatrix * >(_generator);
  if(!sparse){
    throw "Error: stronglyConnectedComponents works only with sparseMatrix as a transitionStructure";
  }


  return sparse->getStronglyConnectedComponents().first->size() == 1;
}

std::vector< std::vector<int> > markovChain::recurrentClassesR()
{
  std::vector< std::vector<int> > result;

#ifdef WITH_R
  // retreive R engine, with creation if needed.
  _Rmotor = Rmotor();

  std::string txt = "suppressMessages(library(markovchain))"; // importer le package markovchain
  std::string str = toString("R"); // get the Markov chain in R format

  _Rmotor->parseEvalQ(txt);              // executer sous R sans retour
  _Rmotor->parseEvalQ(str);

  List L = _Rmotor->parseEval("recurrentClasses(mcA)");
  // Or you can replace it with: List L = R->parseEval("communicatingClasses(mcA)");

  for(int i=0; i < L.size(); i++) {

    std::vector<int> tmpVector;
    SEXP ll = L[i];
    Rcpp::CharacterVector y(ll);

    for(int j=0; j < y.size(); j++) {
      int val = charVectorElt2State( y[j], "recurrentClasses()" );
      if ( val != -1 ) {
        // tmpVector.push_back(Rcpp::as<std::string>(y[j]));
        tmpVector.push_back( val );
      }
    }
    result.push_back(tmpVector);
  }

#else

  throw "markovChain::recurrentClassesR requires that project was compiled using WITH_R macros.";

#endif

  return result;
}

std::vector< std::vector<int> > markovChain::recurrentClasses()
{
  std::vector< std::vector<int> > result;

  sparseMatrix * sparse = dynamic_cast<sparseMatrix * >(_generator);
  if(!sparse){
    throw "Error: stronglyConnectedComponents works only with sparseMatrix as a transitionStructure";
  }
  std::pair<std::vector<SCC> *, sparseMatrix *> _sccs = sparse->getStronglyConnectedComponents();

  for(unsigned int i = 0; i < _sccs.first->size(); ++i){
	  if(_sccs.second->getNbElts(_sccs.first->at(i).id) == 0){
		  std::vector<int> states;
		  std::copy(_sccs.first->at(i).states.begin(), _sccs.first->at(i).states.end(), back_inserter(states));
		  result.push_back(states);
	  }
  }

  return result;
}


std::vector< std::vector<int> > markovChain::communicatingClassesR()
{
  std::vector< std::vector<int> > result;

#ifdef WITH_R
  // retreive R engine, with creation if needed.
  _Rmotor = Rmotor();

  std::string txt = "suppressMessages(library(markovchain))"; // importer le package markovchain
  std::string str = toString("R"); // get the Markov chain in R format

  _Rmotor->parseEvalQ(txt);              // executer sous R sans retour
  _Rmotor->parseEvalQ(str);

  List L = _Rmotor->parseEval("communicatingClasses(mcA)");

  for(int i=0; i < L.size(); i++) {

    std::vector<int> tmpVector;
    SEXP ll = L[i];
    Rcpp::CharacterVector y(ll);

    for(int j=0; j < y.size(); j++) {
      int val = charVectorElt2State( y[j], "recurrentClasses()" );
      if ( val != -1 ) {
        // tmpVector.push_back(Rcpp::as<std::string>(y[j]));
        tmpVector.push_back( val );
      }
    }
    result.push_back(tmpVector);
  }
#else

  throw "markovChain::communicatingClassesR requires that project was compiled using WITH_R macros.";

#endif

  return result;
}

std::vector< std::vector<int> > markovChain::communicatingClasses()
{
  std::vector< std::vector<int> > result;

  sparseMatrix * sparse = dynamic_cast<sparseMatrix * >(_generator);
  if(!sparse){
    throw "Error: stronglyConnectedComponents works only with sparseMatrix as a transitionStructure";
  }

  std::pair<std::vector<SCC> *, sparseMatrix *>  _sccs = sparse->getStronglyConnectedComponents();

  for(unsigned int i = 0; i < _sccs.first->size(); ++i){
    std::vector<int> states;
    std::copy(_sccs.first->at(i).states.begin(), _sccs.first->at(i).states.end(), back_inserter(states));
    result.push_back(states);
  }

  return result;
}

std::vector<int> markovChain::absorbingStatesR()
{
  std::vector<int> result;

#ifdef WITH_R
  // retreive R engine, with creation if needed.
  _Rmotor = Rmotor();

  std::string txt = "suppressMessages(library(markovchain))"; // importer le package markovchain
  std::string str = toString("R"); // get the Markov chain in R format

  _Rmotor->parseEvalQ(txt);              // executer sous R sans retour
  _Rmotor->parseEvalQ(str);

  CharacterVector stateNames ((SEXP) _Rmotor->parseEval("absorbingStates(mcA)"));

  for (int i=0; i<stateNames.size(); i++) {
    int val = charVectorElt2State( stateNames[i], "absorbingStates()" );
    if ( val != -1 ) {
      // result.push_back(Rcpp::as<std::string>(stateNames[i]));
      result.push_back( val );
    }
  }
#else

  throw "markovChain::absorbingStatesR requires that project was compiled using WITH_R macros.";


#endif

  return result;
}


std::vector<int> markovChain::absorbingStates()
{
  std::vector<int> result;

  std::vector< std::vector<int> > recurrents = this->recurrentClasses();
  for(unsigned int i = 0; i < recurrents.size(); ++i){
	  if(recurrents[i].size() == 1){
		  int state = recurrents[i][0];
		  result.push_back(state);
	  }
  }

  return result;
}


double markovChain::transitionProbability(int stateFrom, int stateTo)
{
  double result = 0.0;

#ifdef WITH_R
  // retreive R engine, with creation if needed.
  _Rmotor = Rmotor();

  std::string txt = "suppressMessages(library(markovchain))"; // importer le package markovchain
  std::string str = toString("R"); // get the Markov chain in R format

  _Rmotor->parseEvalQ(txt);              // executer sous R sans retour
  _Rmotor->parseEvalQ(str);

  std::ostringstream tmp;
  tmp << "transitionProbability(mcA,\"" << stateFrom << "\",\"" << stateTo << "\")";
  std::string cmd = tmp.str();

  // CharacterVector tp=R->parseEval("outs2=transitionProbability(mcB,\"b\",\"c\");outs2");
  // input= "\"b\",\"c\""
  // CharacterVector tp=_Rmotor->parseEval("outs2=transitionProbability(mcB,"+input+");outs2");
  CharacterVector tp=_Rmotor->parseEval( cmd );
 
  if ( tp.size() != 1 ) {
    fprintf( stderr, "Internal error in transitionProbability(): vector of size %ld\n.", tp.size() );
    result = 0.0;
  }
  else {
    const char* format = (Rcpp::as<std::string>(tp[0])).c_str();
    if ( 1 != sscanf( format, "%lf", &result ) ) {
      fprintf( stderr,
               "Internal error in transitionProbability(): '%s' does not contain a number. Ignored.\n",
               format );
      result = 0.0;
    }
  }
#else
try{

throw "Error: cannot find RInside";

}
catch (char const* e)
  {
    cout << "An exception occurred.  " << e
         << ".\n";
  }

#endif

  return result;
}


Distribution* markovChain::stationaryDistributionR()
{
  Distribution*	distRes = (Distribution*)NULL;

#ifdef WITH_R
  // retreive R engine, with creation if needed.
  _Rmotor = Rmotor();

  // create Markov chain in R format
  std::string str = toString("R");
	
  std::string txt = "suppressMessages(library(markovchain))"; // importer le package markovchain

  _Rmotor->parseEvalQ(txt);              // executer sous R sans retour
  _Rmotor->parseEvalQ(str);

  std::string steadyS = "steadyStates(mcA);";
  NumericMatrix matt = _Rmotor->parseEval(steadyS);              // load with no return value

/*
     cout << "the steadyStates has "
      << matt.nrow()<<" rows and "
      << matt.ncol()<<" cols "<< std::endl;
*/

  // initialize the values and the distributions, then fill the tables
  double *values = (double*) malloc( matt.ncol() * sizeof(double) );
  double *pi = (double*) malloc( matt.ncol() * sizeof(double) );
        
  for( int i=0; i<matt.nrow(); i++) {
    for( int j=0; j<matt.ncol(); j++) {
      values[j] = (double)j;
      pi[j]=matt(i,j);
    }
  }

  distRes = new discreteDistribution( _stateSpaceSize, values, pi );

  free( pi );
  free( values );

#else
  try {
    throw "Error: cannot find RInside";
  }
  catch (char const* e)
    {
      cout << "An exception occurred.  " << e << "."
	   << " Null distribution returned." << endl;
    }
#endif

  return distRes;
}


simulationResult* markovChain::simulateChainR( double tMax,
                                               bool stats, bool traj, bool trace )
{
  simulationResult* res = (simulationResult*)NULL;
#ifdef WITH_R
  double* dates;
  int* states;
  double *cumTime;
  double *proba;

  // prepare result
  res = new simulationResult( _stateSpaceSize, DISCRETE, stats );
  if ( traj ) {
    /* allocate memory for entire trajectory: from 0 to tMax inclusive */
    res->setTrajectory(true);
    res->setTrajectorySize(1+tMax);
    dates = (double*)malloc( (size_t)(1+tMax) * sizeof(double) );
    states = (int*)malloc( (size_t)(1+tMax) * sizeof(int) );
  }
  if ( stats ) {
    cumTime = (double*) calloc( (size_t)_stateSpaceSize, sizeof( double ) );
    proba = (double*) calloc( (size_t)_stateSpaceSize, sizeof( double ) );
  }

  // retreive R engine, with creation if needed.
  _Rmotor = Rmotor();

  std::string txt = "suppressMessages(library(markovchain))"; // importer le package markovchain
  std::string str = toString("R");
  _Rmotor->parseEvalQ(txt);              // executer sous R sans retour
  _Rmotor->parseEvalQ(str);

  // CharacterVector rmarkov=R.parseEval("outs=rmarkovchain(n=20, object=mcA); outs");
  // input = "20"

  // Sample from initial distribution
  if ( (discreteDistribution*)NULL == _initDistribution ) {
    cerr << "Warning in simulateChainR: no initial distribution. Dirac(0) assumed." << endl;
    _initDistribution = new diracDistribution(0);
  }
  int iState = _initDistribution->sample();

  std::ostringstream tmp;
  tmp << "rmarkovchain(n=" << (int)floor(tMax) << ",object=mcA,t0=\"" << iState
      << "\",include.t0=TRUE)";
  std::string cmd = tmp.str();

  CharacterVector rmarkov=_Rmotor->parseEval(cmd);

  // retreiving and converting the sequence of states
  if ( rmarkov.size() != (tMax+1) ) {
    fprintf( stderr,
             "Warning in simulateChainR: inconsistent length for list of states (%ld<>%ld).",
             rmarkov.size(), (long int)tMax+1 );
    fprintf( stderr, " Smallest one taken.\n");
  }
  int trajLength = ( rmarkov.size() >= tMax ? tMax : rmarkov.size() );
  for (int i=0; i < trajLength; i++ ) {
    // convert the state
    int curState = charVectorElt2State( rmarkov[i], "simulateChainR" );
    if ( traj ) {
      if ( trace ) {
        printf("%6d %4d\n", i, curState );
      }
      states[i] = curState;
      dates[i] = i;
    }
    if ( stats ) {
      cumTime[curState]++;
    }
  }

  if ( stats ) {
    for ( int i = 0; i < _stateSpaceSize; i++ ) {
      proba[i] = cumTime[i] / tMax;
    }

    // at the moment, there is no generic state space at this level of
    // abstract Markov Chains. So create one for the purpose.
    double* stateSpace = (double*)malloc( _stateSpaceSize * sizeof(double) );
    for ( int i = 0; i < _stateSpaceSize; i++ ) {
      stateSpace[i] = i;
    }

    discreteDistribution *dis =
      new discreteDistribution( _stateSpaceSize, stateSpace, proba );
    res->setDistribution( dis );

    free( cumTime );
    free( stateSpace );
    free( proba );
  }

  if ( traj ) {
    res->setTrajectorySize( (rmarkov.size()));
    res->setTrajectory(dates,states);
  }
#else
try{
    throw "Error: cannot find RInside";
  }
  catch (char const* e)
  {
    cout << "An exception occurred.  " << e
         << ".\n";
  }

#endif

  return res;
}

// Method from the 'uvand' functions of L. Cerda-Alabern
Distribution* markovChain::transientDistributionR(int stateFrom, double t)
{
  Distribution* distRes = (Distribution*)NULL;
#ifdef WITH_R

  if ( _type == DISCRETE ) {
    fprintf( stderr, "warning in transientDistributionR(): cannot be applied to discrete-time MC.");
    fprintf( stderr, " Dirac(%d) returned.", stateFrom );

    distRes = new diracDistribution( stateFrom );
  }

  // Assert: ( _type == CONTINUOUS )
  // retreive R engine, with creation if needed.
  _Rmotor = Rmotor();

  try {
    std::string txt = "suppressMessages(library(Matrix))"; // importer le package markovchain
    _Rmotor->parseEvalQ(txt);
  } catch ( std::runtime_error &err ) {
    fprintf( stderr, "Error: %s. ", err.what() );
    fprintf( stderr, "Possible solution: install.packages(\"Matrix\",\"R.oo\",\"R.matlab\") in R.\n");
  }

  try {
    _Rmotor->parseEvalQ("source(\"oo-markov-undetermined-coefficients.R\")");
  } catch ( std::runtime_error &err ) {
    fprintf( stderr, "Error: %s. ", err.what() );
    fprintf( stderr, "Possible solution: install missing file in the current directory.\n");
  }

  // SEXP ans;
  // int code;
  std::string str = "matrice <- " + generator()->toString("R");
  _Rmotor->parseEvalQ(str);
  // cout << "----- Now matrice";
  // _Rmotor->parseEvalQ("matrice");
  // cout << "----- Now solve.uc";
  // _Rmotor->parseEvalQ("uc.s <- solve.uc( Q=matrice, method='vand', i=0, unif=FALSE )");
  // _Rmotor->parseEvalQ("uc.s <- solve.uc( Q=matrice )");
  // Solving instruction. Warning: R matrix indices start at 1; ours start at 0.
  std::ostringstream tmp1;
  tmp1 << "uc.s <- solve.uc( Q=matrice, i=" << (stateFrom+1) << ")";
  _Rmotor->parseEvalQ(tmp1.str());
  // cout << "----- Now print(uc.s)";
  // _Rmotor->parseEvalQ("print(uc.s)");
  // cout << "----- Now uc.s$prob(0.5)";
  std::ostringstream tmp2;
  tmp2 << "ptrans <- uc.s$prob( " << t << ")";
  //_Rmotor->parseEvalQ("ptrans <- uc.s$prob(0.5)");
  _Rmotor->parseEvalQ(tmp2.str());
  NumericMatrix matt = _Rmotor->parseEval("as.matrix( ptrans )");
  // fprintf( stdout, "Got result as a %d x %d matrix\n", matt.nrow(), matt.ncol() );

  // initialize the values and the distributions, then fill the tables
  double *values = (double*) malloc( matt.ncol() * sizeof(double) );
  double *pi = (double*) malloc( matt.ncol() * sizeof(double) );

  for( int i=0; i<matt.nrow(); i++) {
    for( int j=0; j<matt.ncol(); j++) {
      values[j] = (double)j;
      pi[j]=matt(i,j);
    }
  }

  distRes = new discreteDistribution( _stateSpaceSize, values, pi );

  free( pi );
  free( values );
#else
try{

throw "Error: cannot find RInside";

}
catch (char const* e)
  {
    cout << "An exception occurred.  " << e <<'\n';
  }
#endif

  return distRes;
}

//*****************************************
// marmoteCore's own methods
//*****************************************
Distribution* markovChain::transientDistributionDT( int fromState, int t )
{
  discreteDistribution* distRes;
  discreteDistribution* distTmp;

  if ( (discreteDistribution*)NULL == _initDistribution ) {
    cerr << "Warning in markovChain::transientDistributionDT(): "
	 << "no initial distribution. Dirac(0) assumed." << endl;
    distRes = new diracDistribution( 0 );
  }
  else {
    // Making a copy of the distribution so that this procedure can handle
    // its own memory and provoke the net creation of only one distribution object
    distRes = _initDistribution->copy();
  }

  for ( int i = 1; i <= t; i++ ) {
    distTmp = generator()->evaluateMeasure( distRes );
    delete distRes;
    distRes = distTmp;
  }

  return distRes;
}

//*****************************************
// Methods captured from the Xborne package
//*****************************************
Distribution* markovChain::stationaryDistributionGthLD()
{
  Distribution*	distRes = (Distribution*)NULL;

  if ( _isAbstract ) {

    if ( format() == "XbornePres" ) {

      // have to convert to "Xborne". To be done

    }
    else if ( format() == "Xborne" ) {

      char *mgp=(char *)(modelName().c_str());
      char *ext=(char *)"Rii";

      // call to the GTH method. Instantiates the stationary distribution
      // and the size of the chain (before that, it is not known)
      long double *pi;
      mainGthLD( mgp, ext, &pi, &(_stateSpaceSize) );

      // initialize the values and the distributions
      double *values = (double*) malloc( _stateSpaceSize * sizeof(double) );
      double *probas = (double*) malloc( _stateSpaceSize * sizeof(double) );
      for ( int i = 0; i < _stateSpaceSize; i++ ) {
        values[i] = (double)i;
        probas[i] = (double)pi[i];
      }

      distRes = new discreteDistribution( _stateSpaceSize, values, probas );

      free( values );
      free( probas );
      // eliminate temporary probability vector
      free( pi );
    }
  }
  else {
    // have to write as Xborne then call method on new "abstract" MC
  }

  return distRes;
}

Distribution* markovChain::stationaryDistributionSOR()
{
  Distribution*	distRes = (Distribution*)NULL;

  if ( _isAbstract ) {
    
    if ( format() == "XbornePres" ) {

      // have to convert to "Xborne". To be done

    }
    else if ( format() == "Xborne" ) {

      char *mgp=(char *)(modelName().c_str());

      // call to the SOR method. Instantiates the stationary distribution
      // and the size of the chain (before that, it is not known)
      double *pi;
      mainSOR( mgp, &pi, &(_stateSpaceSize), /* writeResult? false */ 0 );

      /* initialize the values and the distributions */
      double *values = (double*) malloc( _stateSpaceSize * sizeof(double) );
      for ( int i = 0; i < _stateSpaceSize; i++ ) {
	values[i] = (double)i;
      }

      distRes = new discreteDistribution( _stateSpaceSize, values, pi );

      free( pi );
      free( values );
    }
  }
  else {
    // have to write as Xborne then call method on new "abstract" MC
    
    distRes = (discreteDistribution*)NULL;
  }

  return distRes;
}

//*************************************
// Methods adapted from the ERS package
//*************************************
Distribution* markovChain::stationaryDistribution( bool progress )
{
  Distribution*	distRes = (Distribution*)NULL;

  switch( _type )
  {
  case DISCRETE:
    // calling the power method with default parameters
    distRes = stationaryDistribution_iterative( "Power", 0, 0, "Zero",
                                                (discreteDistribution*)NULL, progress );
    break;

  case CONTINUOUS:
    distRes = stationaryDistributionCT( progress );
    break;

  default:
    printf("Error: unknown Chain type (%d). No solution.\n", _type );
  }

  return distRes;
}


Distribution* markovChain::stationaryDistributionCT( bool progress )
{
  markovChain* unif;
  Distribution* res;

  unif = uniformize();

  res = unif->stationaryDistribution( progress );

  delete unif;

  return res;
}


Distribution* markovChain::stationaryDistributionCT_embedding(int tMax, double epsilon,
                                                              discreteDistribution* iDis,
                                                              bool progress )
{
  markovChain* embedMC;
  discreteDistribution* tmp;
  Distribution* res;
  double *probas;
  double *values;

  embedMC = embed();

  tmp = dynamic_cast<discreteDistribution*>(embedMC->stationaryDistribution_power( tMax, epsilon,
                                                                                   iDis, progress ) );
  // renormalize the distribution, set up values
  probas = (double*) malloc( _stateSpaceSize*sizeof(double) );
  values = (double*) malloc( _stateSpaceSize*sizeof(double) );
  double total = 0.0;
  for ( int i = 0; i < _stateSpaceSize; i++ ) {
    double nu = - _generator->getEntry(i,i);
    if ( nu == 0.0 ) {
      // absorbing state. Embedding does not work. User has been warned.
      probas[i] = tmp->getProba(i); // arbitrary value
    }
    else {
      probas[i] = tmp->getProba(i) / nu;
    }
    values[i] = tmp->getValue(i);
    total += probas[i];
  }
  for ( int i = 0; i < _stateSpaceSize; i++ ) {
    probas[i] = probas[i] / total;
  }

  res = new discreteDistribution( _stateSpaceSize, values, probas );

  delete embedMC;
  delete tmp;

  return res;
}

// Markov chain transformation methods
/**
 * @brief Copy a Markov Chain, including the generator and the initial distribution.
 * @author Alain Jean-Marie
 * @return a new Markov chain, uniformized version of the original
 */
markovChain *markovChain::copy()
{
  markovChain* res;

  res = new markovChain( _stateSpaceSize, _type );
  if ( (transitionStructure*)NULL != _generator ) {
    transitionStructure* tr = _generator->copy();
    res->setGenerator( tr );
  }

  if ( (discreteDistribution*)NULL != _initDistribution ) {
    discreteDistribution* init = _initDistribution->copy();
    res->setInitDistribution( init );
  }

  return res;
}

markovChain* markovChain::uniformize() 
{
  markovChain* res;

  if ( _type == DISCRETE ) {
    fprintf( stderr, "Error in uniformize: " );
    fprintf( stderr, "cannot uniformize discrete-time Markov chain. " );
    fprintf( stderr, "Copy of the original returned.\n" );
    res = copy();
  }
  else {
    // continuous-time object

    if ( _generator == NULL ) {
      // have to create generator. But from what?
      // makeMarkovChain();
      fprintf( stderr, "Error: no generator. Null returned.\n" );
      res = (markovChain*)NULL;
    }
    else {
      transitionStructure* uGen = _generator->uniformize();

      // possibly, uniformization did not work: for instance with infinite
      // state spaces. 
      if ( (transitionStructure*)NULL != uGen ) {
        res = new markovChain( uGen );
      }
      else {
        fprintf( stderr, "Error: could not uniformize transition structure. Null returned.\n" );
        res = (markovChain*)NULL;
      }
    }
  }

  return res;
}

markovChain* markovChain::embed()
{
  markovChain* res;

  if ( _type == DISCRETE ) {
    fprintf( stderr, "Error: cannot embed discrete-time Markov chain. Copy of the original returned.\n" );
    res = copy();
  }
  else {
    // continuous-time object

    if ( _generator == NULL ) {
      // have to create generator. But from what?
      // makeMarkovChain();
      fprintf( stderr, "Error: no generator. Null returned.\n" );
      res = (markovChain*)NULL;
    }
    else {
      transitionStructure* eGen = _generator->embed();

      // possibly, uniformization did not work: for instance with infinite
      // state spaces.
      if ( (transitionStructure*)NULL != eGen ) {
        res = new markovChain( eGen );
      }
      else {
        fprintf( stderr, "Error: could not embed in transition structure. Null returned.\n" );
        res = (markovChain*)NULL;
      }
    }
  }

  return res;
}

Distribution* markovChain::stationaryDistribution_power( int tMax, double epsilon,
                                                         discreteDistribution *iDis, bool progress )
{
  bool		continueIt;
  double	*values;
  double	*pi;
  double	*pi_it;
  discreteDistribution  *res;
  discreteDistribution  *d1;
  discreteDistribution  *d2;
  double	err;
  int		time;

  values = (double*) malloc( _stateSpaceSize * sizeof(double) );
  pi = (double*) calloc( _stateSpaceSize, sizeof(double) );
  pi_it = (double*) calloc( _stateSpaceSize, sizeof(double) );

  if ( _debug ) {
    fprintf( stdout, "Approximating stationary distribution with iterations. Chain:\n");
    _generator->write( stdout, "Ers" );
  }

  // Initialize Pi, the values and the distributions.
  // The distribution provided must have the proper number of values/probas
  if ( iDis->nbVals() == _stateSpaceSize ) {
    for ( int i = 0; i < _stateSpaceSize; i++ ) {
      values[i] = (double)i;
      pi[i] = iDis->getProba(i);
    }
  }
  else {
    fprintf( stderr, "Error in stationaryDistribution_power: ");
    fprintf( stderr, "incorrect number of values for initial distribution ");
    fprintf( stderr, "(expected %d <> %d got)\n", _stateSpaceSize, iDis->nbVals() );
    return (Distribution*)NULL;
  }
  d1 = new discreteDistribution( _stateSpaceSize, values, pi );
  d2 = new discreteDistribution( _stateSpaceSize, values, pi_it );

  continueIt = true;

  for ( time=0; (time<tMax) && continueIt; time++ ) {

    _generator->evaluateMeasure( pi, pi_it );
    
    if ( _debug ) {
      printf("# Pi(%d) = ", time );
      for ( int i=0; i<_stateSpaceSize; i++ ) {
        printf("%8.6f ", pi_it[i] );
      }
      printf("\n");
    }

    // compute distance
    // err = d1->distanceL1( d2 );
    err = 0.0;
    for ( int i = 0; i < _stateSpaceSize; i++ ) {
      err += fabs( pi[i] - pi_it[i] );
    }

    if ( progress ) {
      fprintf( stderr, "Iteration #%3d: distance %10e\n", time, err );
      fflush( stderr );
    }
    if ( err < epsilon ) {
      continueIt = false;
    } 
    else {
      // swap arrays and distribution objects
      discreteDistribution* dTmp = d1; d1 = d2; d2 = dTmp;
      double* tmp = pi_it; pi_it = pi; pi = tmp;
    }
  }
  if ( time >= tMax ) {
    fprintf( stderr, "Warning: Maximum number of iterations reached. ");
    fprintf( stderr, "Distribution may be very imprecise.\n");
  }

  // creating result. The array that is shipped away is a copy of pi, which
  // can be freed
  res = new discreteDistribution( _stateSpaceSize, values, pi );

  /* cleanup */
  free( pi );
  free( pi_it );
  free( values );
  delete d1;
  delete d2;

  /* returning result */
  return( res );

}

Distribution* markovChain::stationaryDistribution_iterative( string method,
                                                int tmax,
                                                double precision,
                                                string initDistribType,
                                                discreteDistribution* initDistrib,
                                                bool progress )
{
  Distribution* res;

  // setting default values
  int max_iter;
  if ( tmax <= 0 )
    max_iter = 1000;
  else
    max_iter = tmax;

  double epsilon;
  if ( precision <= 0.0 )
    epsilon = 1e-7;
  else
    epsilon = precision;

  discreteDistribution *iDis;
  if ( ( initDistribType == "Custom") && ( initDistrib != (Distribution*)NULL) ) {
    iDis = initDistrib;
  }
  else {
    // preparing initial distribution
    double *values = (double*) malloc( _stateSpaceSize * sizeof(double) );
    double *probas = (double*) calloc( _stateSpaceSize, sizeof(double) );
    for ( int i = 0; i < _stateSpaceSize; i++ ) {
      values[i] = (double)i;
    }

    if ( ( initDistribType == "Custom") && ( initDistrib == (Distribution*)NULL ) ) {
      fprintf( stderr, "Warning in stationaryDistribution_iterative(): " );
      fprintf( stderr, "no distribution provided for 'Custom' option. Zero assumed.\n" );
      probas[0] = 1.0;
    }
    else if ( initDistribType == "Zero" ) {
      probas[0] = 1.0;
    }
    else if ( initDistribType == "Max" ) {
      probas[_stateSpaceSize-1] = 1.0;
    }
    else if ( initDistribType == "Uniform" ) {
      double pr = 1.0/_stateSpaceSize;
      for ( int i = 0; i < _stateSpaceSize; i++ ) probas[i] = pr;
    }
    else {
      fprintf( stderr, "Warning in stationaryDistribution_iterative(): " );
      fprintf( stderr, "unknown option '%s'. Zero assumed.", initDistribType.c_str() );
      probas[0] = 1.0;
    }
    iDis = new discreteDistribution( _stateSpaceSize, values, probas );

    // cleanup of temporary arrays: they have been copied at creation of discreteDistribution
    free( values );
    free( probas );
  }

  // dispatch call to methods
  if ( method == "Power" ) {
    res = stationaryDistribution_power( max_iter, epsilon, iDis, progress );
  }
  else {
    fprintf( stderr, "Error in stationaryDistribution_iterative(): unknown method '%s'.",
             method.c_str() );
    res = (Distribution*)NULL;
  }

  // cleanup
  if ( iDis != initDistrib ) {
    delete iDis;
  }

  // exit
  return res;
}

simulationResult* markovChain::simulateChainDT( int tMax,
						bool stats, bool traj,
						bool trace )
{
  int		curState;
  double	*cumTime;
  int		*states;
  double	*dates; // are needed for a common interface to simulationResult
  simulationResult *res;
  discreteDistribution** transDis;

  // prepare transition distributions
  transDis = 
    (discreteDistribution**) malloc( _stateSpaceSize * sizeof(discreteDistribution*) );
  for ( int i = 0; i < _stateSpaceSize; i++ ) {
    transDis[i] = _generator->getTransDistrib( i );
  }

  res = new simulationResult( _stateSpaceSize, DISCRETE, stats );
  if ( traj ) {
    /* allocate memory for entire trajectory: from 0 to tMax inclusive */
    res->setTrajectory(true);
    res->setTrajectorySize(1+tMax);
    dates = (double*)malloc( (size_t)(1+tMax) * sizeof(double) );
    states = (int*)malloc( (size_t)(1+tMax) * sizeof(int) );
  }

  // Sample from initial distribution
  if ( (discreteDistribution*)NULL == _initDistribution ) {
    cerr << "Warning in simulateChainDT: no initial distribution. Dirac(0) assumed." << endl;
    _initDistribution = new diracDistribution(0);
  }
  curState = _initDistribution->sample();
  
  if ( stats ) {
    cumTime = (double*) calloc( (size_t)_stateSpaceSize, sizeof( double ) );
  }

  for ( int time = 0; time < tMax; time++ ) {
    if ( traj ) {
        if ( trace ) {
          printf("%6d %4d\n", time, curState );
        }

        dates[time] = (double)time;
        states[time] = curState;
    }
    if ( stats ) {
      cumTime[ curState ]++;
    }

    curState = transDis[curState]->sample();

  }

  /* adjust end of simulation */
  if ( traj ) {
      if ( trace ) {
          printf("%6d %4d\n", tMax, curState );
        }

      dates[tMax] = (double)tMax;
      states[tMax] = curState;

  }

  // create result
  if ( stats && ( tMax > 0 ) ) {
    double *proba = 
      (double*) calloc( (size_t)_stateSpaceSize, sizeof( double ) );

    for ( int j=0; j<_stateSpaceSize; j++ ) {
      proba[j] = (double)cumTime[j]/(double)tMax;
      if ( trace ) {
	printf("State %3d:   cum time = %5d   ( %8.4f%%)\n",
	       j, (int)rint(cumTime[j]), 100.0*proba[j] );
      }
    }

    // at the moment, there is no generic state space at this level of 
    // abstract Markov Chains. So create one for the purpose.
    double* stateSpace = (double*)malloc( _stateSpaceSize * sizeof(double) );
    for ( int i = 0; i < _stateSpaceSize; i++ ) {
      stateSpace[i] = i;
    }

    discreteDistribution *dis =
      new discreteDistribution( _stateSpaceSize, stateSpace, proba );
    res->setDistribution( dis );
    
    free( stateSpace );
    free( cumTime );
    free( proba );
  }

  if ( traj ) {
    res->setTrajectory(dates,states);
  }

  // cleanup
  for ( int i = 0; i < _stateSpaceSize; i++ ) {
    delete transDis[i];
  }
  free( transDis );

  // exit
  return res;
}


simulationResult* markovChain::simulateChainCT( double tMax,
						bool stats, bool traj,
						bool withIncrements,
						bool trace )
{
  int		curState;
  double	time;
  double	adv;
  double	*cumTime;
  double	*proba;
  double	*dates;
  int		*states;
  double	*increments;
  simulationResult* res;
  int		maxTrajLength;
  int		curTrajLength = 0;
  discreteDistribution** transDis;

  // prepare transition distributions
  transDis = 
    (discreteDistribution**) malloc( _stateSpaceSize * sizeof(discreteDistribution*) );
  for ( int i = 0; i < _stateSpaceSize; i++ ) {
    transDis[i] = _generator->getTransDistrib( i );
  }

  // prepare result and temporary tables
  res = new simulationResult( _stateSpaceSize, CONTINUOUS, stats );
  maxTrajLength = 1024;
  if ( traj ) {
    /* allocate initial memory (1k words) for trajectory */
    res->setTrajectory(true);
    dates = (double*)malloc( maxTrajLength * sizeof(double) );
    states = (int*)malloc( maxTrajLength * sizeof(int) );
    if ( withIncrements ) {
      increments = (double*)malloc( maxTrajLength * sizeof(double) );
    }
  }

  // Sample from initial distribution
  if ( (discreteDistribution*)NULL == _initDistribution ) {
    cerr << "Warning in simulateChainCT: no initial distribution. Dirac(0) assumed." << endl;
    _initDistribution = new diracDistribution(0);
  }
  curState = _initDistribution->sample();
  
  time = 0.0;

  if ( stats ) {
    cumTime = (double*) calloc( (size_t)_stateSpaceSize, sizeof( double ) );
    proba = (double*) calloc( (size_t)_stateSpaceSize, sizeof( double ) );
  }

  do {

    if ( traj && ( curTrajLength >= maxTrajLength ) ) {
      maxTrajLength += 1024;
      dates = (double*)realloc( dates, maxTrajLength * sizeof(double) );
      states = (int*)realloc( states, maxTrajLength * sizeof(int) );
      if ( withIncrements ) {
        increments = (double*)realloc( increments,
				       maxTrajLength * sizeof(double) );
      }
    }
    int nextState = transDis[curState]->sample();
    double nu = - _generator->getEntry( curState, curState );
    if ( nu == 0.0 ) {
      // this is an absorbing state. Set time at "infinity"
      adv = tMax;
    }
    else {
      adv = Distribution::exponential( 1.0/nu );
    }

    if ( stats ) {
      cumTime[ curState ] += ( time+adv >= tMax ? tMax - time : adv );
    }

    if ( traj ) {

      dates[curTrajLength] = time;
      states[curTrajLength] = curState;
      if ( withIncrements ) {
        increments[curTrajLength] = adv;
      }

      if ( trace ) {
        printf("[%4d] %12.6f %4d", curTrajLength, time, curState );

        if ( withIncrements )
          printf(" %10.6f", adv );

        printf("\n");
      }
    }

    time += adv;
    curState = nextState;

    curTrajLength++;
  } while ( time < tMax );

  /* adjust end of simulation */
  if ( traj ) {
    // By convention: last time is tMax. Have to clip the last interval.
    if ( curTrajLength >= maxTrajLength ) {
      maxTrajLength += 1;
      dates = (double*)realloc( dates, maxTrajLength * sizeof(double) );
      states = (int*)realloc( states, maxTrajLength * sizeof(int) );
      if ( withIncrements ) {
        increments = (double*)realloc( increments,
				       maxTrajLength * sizeof(double) );
      }
      dates[curTrajLength] = tMax;
      states[curTrajLength] = curState;
      if ( withIncrements )
        increments[curTrajLength] = tMax-dates[curTrajLength-1];
      
      if ( trace ) {
        printf("[%4d] %12.6f %4d", curTrajLength, time, curState );

        if ( withIncrements )
          printf(" %10.6f", adv );

        printf("\n");
      }

      curTrajLength++;
    }
  }

  if ( stats ) {
    for ( int i = 0; i < _stateSpaceSize; i++ ) {
      proba[i] = cumTime[i] / tMax;
    }

    // at the moment, there is no generic state space at this level of 
    // abstract Markov Chains. So create one for the purpose.
    double* stateSpace = (double*)malloc( _stateSpaceSize * sizeof(double) );
    for ( int i = 0; i < _stateSpaceSize; i++ ) {
      stateSpace[i] = i;
    }

    discreteDistribution *dis =
      new discreteDistribution( _stateSpaceSize, stateSpace, proba );
    res->setDistribution( dis );

    free( cumTime );
    free( stateSpace );
    free( proba );
  }

  if ( traj ) {
    res->setTrajectorySize(curTrajLength);
    res->setTrajectory(dates,states);
  }

  // cleanup
  for ( int i = 0; i < _stateSpaceSize; i++ ) {
    delete transDis[i];
  }
  free( transDis );

  // exit
  return res;
}

simulationResult* markovChain::simulatePSI( int tMax,
                                            bool stats, bool Traj,
                                            bool print )
{
  simulationResult* res = (simulationResult*)NULL;
  char command[1024];

  switch( _type ) {

  case DISCRETE:

    if ( Traj ) {
      // write model as MARCA file

      // call to psi_alias then psi_traj
      snprintf( command, 1024, "psi_alias -i ./%s.marca -o ./%s", 
		_abstract[0].c_str(), _abstract[0].c_str() );
      cerr << "--- Executing: " << command << endl;
      system( command );
      snprintf( command, 1024, "psi_traj -i ./%s.simu -o ./%s -s %d -l %d",
		_abstract[0].c_str(), _abstract[0].c_str(),
		(int)rint(_initDistribution->sample()), tMax );
      cerr << "--- Executing: " << command << endl;
      system( command );
      res = new simulationResult( "PSI", _abstract[0], stats );

    }
    else {
      // call to psi_mcmc
      // res = simulatePSI( rint(tMax), Stats, Traj, print);

    }
    break;

  case CONTINUOUS:
    printf("Error in markovChain::simulatePSI: cannot handle continuous-time chains. No simulation.\n" );

    break;

  default:
    printf("Error in markovChain::simulatePSI: unknown Chain type. No simulation.\n" );
  }

  return res;
}

simulationResult* markovChain::simulateChain( double tMax,
					      bool Stats, bool Traj, 
                          bool withIncrements, bool print )
{
  simulationResult* res = (simulationResult*)NULL;

  switch( _type ) {
  case DISCRETE:
    res = simulateChainDT( (int)rint(tMax), Stats, Traj, print);
    break;

  case CONTINUOUS:
    res = simulateChainCT( tMax, Stats, Traj, withIncrements, print);
    break;

  default:
    printf("Error in markovChain::simulate: unknown Chain type. No simulation.\n" );
  }

  return res;
}



void markovChain::scilab_classmarkov(std::vector<double> & perm, std::vector<double> & rec, int & tr, std::vector<double> & indsRec, std::vector<double> & indsT)
{

  std::string str = toString("SCILAB"); // get the Markov chain in SCILAB format

  str=str+"\n[perm,rec,tr,indsRec,indsT]=classmarkov(mcA)\n"; // add the call of  classmarkov
  str=str+"tr \n";
  str=str+"fprintfMat(\"VecPerm\", perm, \"%.9f\")\n";
  str=str+"fprintfMat(\"VecRec\", rec, \"%.9f\") \n";
  str=str+"fprintfMat(\"VecIndsRec\", indsRec, \"%.9f\") \n";
  str=str+"fprintfMat(\"VecIndsT\", indsT, \"%.9f\") ";
  // test if the input exist
  FILE * out;
  std::string fileName = "modelName.sce";
  out = fopen( fileName.c_str(), "w" );
    if ( NULL != out ) {
      fprintf( out, "%s", str.c_str() );
      fclose( out );
    }

   char command[1024];
   

  ///home/irabhi/Devel/scilab-5.5.2/bin/scilab -nw < /home/irabhi/Devel/Scilab/test.sce > /home/irabhi/Devel/Scilab/testResut

  snprintf( command, 1024, "/home/irabhi/Devel/scilab-5.5.2/bin/scilab -nw < %s > scilab_classmarkov_output", fileName.c_str());
  cerr << "--- Executing: " << command << endl;
  system( command );



  std::ifstream in("./scilab_classmarkov_output");
  string line;

 while( std::getline( in, line ) ) {

   if (line.find("tr  =") != string::npos) {
     std::getline( in, line );
     std::getline( in, line );
     char * z=new char[line.length()+1];  
     strcpy( z, line.c_str() );
     sscanf(z,"%d",&tr);
   					  }
			          }

//std::cout<<"The value of tr is : "<<tr<<"\n";

   
// parse output:

   std::ifstream in1("./VecPerm");
  string line1;

		while( std::getline( in1, line1 ) ) {

       		char * z1=new char[line1.length()+1];  
       		strcpy( z1, line1.c_str() );
       		double tmpd1=0;
      		
   	        //strtok() need #include <cstring>
    		   for ( char *s1= strtok( z1, " " ); s1 != 0; s1= strtok( 0, " " ) )
			{
			sscanf(s1,"%lf",&tmpd1);
    		        perm.push_back(tmpd1);
			}
	         
       		                                   }
   					  


  std::ifstream in2("./VecRec");
  string line2;


       		while( std::getline( in2, line2 ) ) {

       		char * z2=new char[line2.length()+1];  
       		strcpy( z2, line2.c_str() );
       		double tmpd2=0;
      		
   	        //strtok() need #include <cstring>
    		   for ( char *s2= strtok( z2, " " ); s2 != 0; s2= strtok( 0, " " ) )
			{
			sscanf(s2,"%lf",&tmpd2);
    		        rec.push_back(tmpd2);
			}
	          
       		                                     }

 
  std::ifstream in3("./VecIndsRec");
  string line3;


       		while( std::getline( in3, line3 ) ) {

       		char * z3=new char[line3.length()+1];  
       		strcpy( z3, line3.c_str() );
       		double tmpd3=0;
      		
   	        //strtok() need #include <cstring>
    		   for ( char *s3= strtok( z3, " " ); s3 != 0; s3= strtok( 0, " " ) )
			{
			sscanf(s3,"%lf",&tmpd3);
    		        indsRec.push_back(tmpd3);
			}
	      
       		                                     }

 std::ifstream in4("./VecIndsT");
  string line4;


       		while( std::getline( in4, line4 ) ) {

       		char * z4=new char[line4.length()+1];  
       		strcpy( z4, line4.c_str() );
       		double tmpd4=0;
      
   	        //strtok() need #include <cstring>
    		   for ( char *s4= strtok( z4, " " ); s4 != 0; s4= strtok( 0, " " ) )
			{
			sscanf(s4,"%lf",&tmpd4);
    		        indsT.push_back(tmpd4);
			}
	      
       		                                     }


}

std::vector< std::vector<double> >  markovChain::scilab_grand(int n, std::string x0)
{

  std::vector< std::vector<double> > result;

  std::string str = toString("SCILAB"); // get the Markov chain in SCILAB format


/* input:
mcA=[0.3 0.7 ;0.6 0.4];
n=4;
x0=[1 1 1 1 1 1];
Y = grand(n,'markov',mcA,x0)

*/

  ostringstream convert;
  convert<<n;
  str=str+"\nn="+convert.str()+"\n"+"x0=["+x0+"]\n"+"X = grand(n,'markov',mcA,x0) \n"; // add the parameters and the call of  grand
  str=str+"fprintfMat(\"Mat\", X, \"%.9f\")";

  FILE * out;
  std::string fileName = modelName() + ".sce";
  out = fopen( fileName.c_str(), "w" );
    if ( NULL != out ) {
      fprintf( out, "%s", str.c_str() );
      fclose( out );
    }

   char command[1024];
 
  snprintf( command, 1024, "/home/irabhi/Devel/scilab-5.5.2/bin/scilab -nw < %s > scilab_grand_output", fileName.c_str());
  cerr << "--- Executing: " << command << endl;
  system( command );


  // parse output
  
 std::ifstream in("./Mat");
  string line;


        int compteur=0;
       		while( std::getline( in, line ) ) {

       		char * z=new char[line.length()+1];  
       		strcpy( z, line.c_str() );
       		double tmpd=0;
      		 std::vector<double> tmpVector;
   	        //strtok() need #include <cstring>
    		   for ( char *s= strtok( z, " " ); s != 0; s= strtok( 0, " " ) )
			{
			sscanf(s,"%lf",&tmpd);
    		        tmpVector.push_back(tmpd);
			}
	          result.push_back(tmpVector);
       		  compteur++;                      }
   					  

			          

/* // just for test 

cout<<"Matrice result";
 for(int i=0;i<result.size();++i) {
 	for(int j=0;j<result[i].size();++j) {
		std::cout <<"column "<<j<<" :"<<result[i][j] << ' ';
					     }
                 std::cout <<" \n *********** nouvelle ligne "<<i<<" ************ \n";
				  }
*/

 return result;
}



std::vector< std::vector<double> >  markovChain::scilab_genmarkov(int a, int b)
{

  std::vector< std::vector<double> >  result;

  ostringstream convert1, convert2;
  convert1<<a;
  convert2<<b;

  std::string str="X=genmarkov("+convert1.str()+","+convert2.str()+") \n";
  str=str+"fprintfMat(\"Mat\", X, \"%.9f\")";


  // test if the input exist
  FILE * out;
  std::string fileName = "modelname.sce";
  out = fopen( fileName.c_str(), "w" );
    if ( NULL != out ) {
      fprintf( out, "%s", str.c_str() );
      fclose( out );
    }

   char command[1024];
   
 
  ///home/irabhi/Devel/scilab-5.5.2/bin/scilab -nw < /home/irabhi/Devel/Scilab/test.sce > /home/irabhi/Devel/Scilab/testResut

  snprintf( command, 1024, "/home/irabhi/Devel/scilab-5.5.2/bin/scilab -nw < %s > scilab_genmarkov_output", fileName.c_str());
  cerr << "--- Executing: " << command << endl;
  system( command );

   
// parse output:

  std::ifstream in("./Mat");
  string line;


        int compteur=0;
       		while( std::getline( in, line ) ) {

       		char * z=new char[line.length()+1];  
       		strcpy( z, line.c_str() );
       		double tmpd=0;
      		 std::vector<double> tmpVector;
   	        //strtok() need #include <cstring>
    		   for ( char *s= strtok( z, " " ); s != 0; s= strtok( 0, " " ) )
			{
			sscanf(s,"%lf",&tmpd);
    		        tmpVector.push_back(tmpd);
			}
	          result.push_back(tmpVector);
       		  compteur++;                      }
   					  

			          

/* // just for test 

cout<<"Matrice result";
 for(int i=0;i<result.size();++i) {
 	for(int j=0;j<result[i].size();++j) {
		std::cout <<"column "<<j<<" :"<<result[i][j] << ' ';
					     }
                 std::cout <<" \n *********** nouvelle ligne "<<i<<" ************ \n";
				  }

*/


return result;


}


//Cette fonction eigenmarkov renvoie la distribution d’une chaîne de Markov irréductible de matrice de transition P
std::vector< std::vector<double> >  markovChain::scilab_eigenmarkov()
{ 

std::vector< std::vector<double> >  result;

  std::string str = toString("SCILAB"); // get the Markov chain in SCILAB format
  // we need to save the string matrice to the input file. E.g:
  /*
	mcA=[0.3 0.7 ;0.6 0.4]
	X=eigenmarkov(mcA)
	fprintfMat("Mat", X,"%.9f")
	a1= fscanfMat("Mat")
  */
  str=str+"\n X=eigenmarkov(mcA) \n"; // add the call of  eigenmarkov
  str=str+"fprintfMat(\"Mat\", X, \"%.9f\")";
  // test if the input exist
  FILE * out;
  std::string fileName = modelName() + ".sce";
  out = fopen( fileName.c_str(), "w" );
    if ( NULL != out ) {
      fprintf( out, "%s", str.c_str() );
      fclose( out );
    }

   char command[1024];
   
 
  ///home/irabhi/Devel/scilab-5.5.2/bin/scilab -nw < /home/irabhi/Devel/Scilab/test.sce > /home/irabhi/Devel/Scilab/testResut

  snprintf( command, 1024, "/home/irabhi/Devel/scilab-5.5.2/bin/scilab -nw < %s > scilab_eigenmarkov_output", fileName.c_str());
  cerr << "--- Executing: " << command << endl;
  system( command );

   
// parse output:

  std::ifstream in("./Mat");
  string line;


        int compteur=0;
       		while( std::getline( in, line ) ) {

       		char * z=new char[line.length()+1];  
       		strcpy( z, line.c_str() );
       		double tmpd=0;
      		 std::vector<double> tmpVector;
   	        //strtok() need #include <cstring>
    		   for ( char *s= strtok( z, " " ); s != 0; s= strtok( 0, " " ) )
			{
			sscanf(s,"%lf",&tmpd);
    		        tmpVector.push_back(tmpd);
			}
	          result.push_back(tmpVector);
       		  compteur++;                      }
   					  

			          

/* // just for test 

cout<<"Matrice result";
 for(int i=0;i<result.size();++i) {
 	for(int j=0;j<result[i].size();++j) {
		std::cout <<"column "<<j<<" :"<<result[i][j] << ' ';
					     }
                 std::cout <<" \n *********** nouvelle ligne "<<i<<" ************ \n";
				  }

*/

return result;

}


void markovChain::scilab_eigenmarkov(std::vector< std::vector<double> > & M, std::vector< std::vector<double> > & Q)
{ 


  std::string str = toString("SCILAB"); // get the Markov chain in SCILAB format
  // we need to save the string matrice to the input file. E.g:
 
  str=str+"\n[M,Q]=eigenmarkov(mcA) \n"; // add the call of  eigenmarkov
  str=str+"fprintfMat(\"Mat1\", M, \"%.9f\") \n";
  str=str+"fprintfMat(\"Mat2\", Q, \"%.9f\")";
  // test if the input exist
  FILE * out;
  std::string fileName = modelName() + ".sce";
  out = fopen( fileName.c_str(), "w" );
    if ( NULL != out ) {
      fprintf( out, "%s", str.c_str() );
      fclose( out );
    }

   char command[1024];
   

  ///home/irabhi/Devel/scilab-5.5.2/bin/scilab -nw < /home/irabhi/Devel/Scilab/test.sce > /home/irabhi/Devel/Scilab/testResut

  snprintf( command, 1024, "/home/irabhi/Devel/scilab-5.5.2/bin/scilab -nw < %s > scilab_eigenmarkov_output", fileName.c_str());
  cerr << "--- Executing: " << command << endl;
  system( command );

   

 
  std::ifstream in1("./Mat1");
  string line1;

		while( std::getline( in1, line1 ) ) {

       		char * z1=new char[line1.length()+1];  
       		strcpy( z1, line1.c_str() );
       		double tmpd1=0;
      		 std::vector<double> tmpVector1;
   	        //strtok() need #include <cstring>
    		   for ( char *s1= strtok( z1, " " ); s1 != 0; s1= strtok( 0, " " ) )
			{
			sscanf(s1,"%lf",&tmpd1);
    		        tmpVector1.push_back(tmpd1);
			}
	          M.push_back(tmpVector1);
       		                                   }
   					  


  std::ifstream in2("./Mat2");
  string line2;


       		while( std::getline( in2, line2 ) ) {

       		char * z2=new char[line2.length()+1];  
       		strcpy( z2, line2.c_str() );
       		double tmpd2=0;
      		 std::vector<double> tmpVector2;
   	        //strtok() need #include <cstring>
    		   for ( char *s2= strtok( z2, " " ); s2 != 0; s2= strtok( 0, " " ) )
			{
			sscanf(s2,"%lf",&tmpd2);
    		        tmpVector2.push_back(tmpd2);
			}
	          Q.push_back(tmpVector2);
       		                                     }
			          

/* // just for test 

std::cout <<"MATRICE M: \n";

 for(int i=0;i<M.size();++i) {
 	for(int j=0;j<M[i].size();++j) {
		std::cout << M[i][j] << ' ';
					     }
                 std::cout <<" \n";
				  }


std::cout <<"MATRICE Q: \n";

 for(int i=0;i<Q.size();++i) {
 	for(int j=0;j<Q[i].size();++j) {
		std::cout << Q[i][j] << ' ';
					     }
                 std::cout <<" \n";
				  }


*/

}

void markovChain::NCDProperty(double epsilon)

{
  
  char command[1024];

  //write( "XBORNE", _abstract[0].c_str() );

  // call to NCD and suffix is set to "r1"

  snprintf( command, 1024, "./XBORNE/NCD -f %s Rii r1 %f", modelName().c_str(), epsilon);
  cerr << "--- Executing: " << command << endl;
  system( command );
  
}


void markovChain::BandIMSUB(std::string modelName)
{


  char command[1024];

  //write( "XBORNE", _abstract[0].c_str() );


  snprintf( command, 1024, "./XBORNE/BandIMSUB -f %s", modelName.c_str());
  cerr << "--- Executing: " << command << endl;
  system( command );

}


void markovChain::Vincent()
{
  std::ostringstream cmd;

  cmd << "./XBORNE/Vincent";
  cmd << " -f " << modelName();
 // the suffix is set to Rii
  cmd << " Rii";

  cerr << "--- Executing: " << cmd.str() << endl;
  system( cmd.str().c_str() );
 
}

void markovChain::RowVincent()
{
  std::ostringstream cmd;

  cmd << "./XBORNE/RowVincent";
  cmd << " -f " << modelName();

  cerr << "--- Executing: " << cmd.str() << endl;
  system( cmd.str().c_str() );

}

void markovChain::Absorbing()
{
  std::ostringstream cmd;

  cmd << "./XBORNE/Absorbing";
  cmd << " -f " << modelName();

  cerr << "--- Executing: " << cmd.str() << endl;
  system( cmd.str().c_str() );

}

void markovChain::ProdFundSW(std::string modelName)
{
  char command[1024];

  //write( "XBORNE", _abstract[0].c_str() );i

  snprintf( command, 1024, "./XBORNE/ProdFundSW -f %s", modelName.c_str());
  cerr << "--- Executing: " << command << endl;
  system( command );

}

void markovChain::RowSum(std::string modelName)
{
  char command[1024];

  //write( "XBORNE", _abstract[0].c_str() );i

  snprintf( command, 1024, "./XBORNE/RowSum -f %s", modelName.c_str());
  cerr << "--- Executing: " << command << endl;
  system( command );

}

simulationResult* markovChain::stationaryDistributionSample( int nbSamples )
{
  char command[1024];
  simulationResult* res;

  write( "MARCA", _abstract[0].c_str() );

  // call to psi_alias then psi_sample
  snprintf( command, 1024, "psi_alias -i ./%s.marca -o ./%s",
    _abstract[0].c_str(), _abstract[0].c_str() );
  cerr << "--- Executing: " << command << endl;
  system( command );
  snprintf( command, 1024, "psi_sample -i ./%s.simu -o ./%s -d %d",
    _abstract[0].c_str(), _abstract[0].c_str(), nbSamples );
  cerr << "--- Executing: " << command << endl;
  system( command );
  res = new simulationResult( "PSI-sample", _abstract[0], /* stats? */ true );

  return res;
}


int* markovChain::simulateHittingTime( int iState, bool *hittingSet,
                                       int nbSamples, int tMax )
{
  int* res = (int*)malloc( nbSamples*sizeof(int) );
  discreteDistribution* jump = (discreteDistribution*)NULL;

  for ( int i = 0; i < nbSamples; i++ ) {
    int curState = iState;
    int hitT = 0;
    bool newState = true;

    while( !hittingSet[curState] && ( hitT < tMax ) ) {
      hitT++;

      if ( newState ) {
        // small trick to avoid re-creating the same distrib in the case
        // of a self-loop.
        if ( jump != (discreteDistribution*)NULL) delete(jump);
        jump = _generator->getTransDistrib(curState);
      }
      int nextState = jump->sample();

      newState = ( nextState != curState );
      curState = nextState;

    }
    res[i] = hitT;
  }

  return res;
}

double* markovChain::averageHittingTimeDT( bool *hitSetIndicator )
{

    // counting the number of states in the hitting set
    int nbHit = 0;
    for ( int i = 0; i < _stateSpaceSize; i++ ) {
        if ( hitSetIndicator[i] ) nbHit++;
    }

    int nbTrans = _stateSpaceSize - nbHit;

    // Building the "I-Q" matrix
    // First constructing the alias table of transient states
    int* alias = (int*)malloc( nbTrans*sizeof(int) );
    int numRow = 0;
    for ( int i = 0; i < _stateSpaceSize; i++ ) {
      if ( !hitSetIndicator[i] ) {
        alias[numRow] = i;
        numRow++;
      }
    }
    // Creating and filling the matrix at the same time
    double** ImQ = (double**) malloc( nbTrans*sizeof(double*) );
    for ( int i = 0; i < nbTrans; i++ ) {
        ImQ[i] = (double*) calloc( nbTrans, sizeof(double) );
        ImQ[i][i] = 1.0;
        for ( int j = 0; j < nbTrans; j++ ) {
          ImQ[i][j] = ImQ[i][j] - _generator->getEntry( alias[i], alias[j] );
        }
    }
    // Solving the system (I-Q).x = 1 with the Gauss method
    double *tau = (double*) malloc( nbTrans*sizeof(double) );
    for ( int i = 0; i < nbTrans; i++ ) {
      tau[i] = 1.0;
    }

#define DUMP if ( false ) { \
    for( int ii=0; ii<nbTrans; ii++ ) { \
      for( int jj=0; jj<nbTrans; jj++ ) { \
        printf(" %10f", ImQ[ii][jj] ); \
      } \
      printf("| %10f\n", tau[ii] ); \
    } \
}

    if ( _debug ) {
      printf("Init Gauss = \n");
      DUMP
    }

    for( int j=0; j<nbTrans-1; j++ ) {
        // Finding the pivot
        double Max_Val = fabs( ImQ[j][j] );
        int argMax = j;
        for( int k=j; k<nbTrans; k++ )
            if ( fabs( ImQ[k][j] ) > Max_Val ) {
                Max_Val = fabs( ImQ[k][j] );
                argMax = k;
            }
        if ( Max_Val == 0.0 ) {
            printf("Error! Singular matrix ImQ.\n");
            exit(-4);
        }
        if ( argMax != j ) {
            /* need to permute rows. beark. */
            if ( _debug )
                printf("Swapping rows %d <-> %d\n", j, argMax );
            for ( int l=j; l<nbTrans; l++ ) {
                double tmp = ImQ[j][l]; ImQ[j][l] = ImQ[argMax][l]; ImQ[argMax][l] = tmp;
            }
            double tmp = tau[j]; tau[j] = tau[argMax]; tau[argMax] = tmp;
        }
       for( int i=j+1; i<nbTrans; i++ ) {
         double ratio = -ImQ[i][j] / ImQ[j][j];
         for ( int k=j; k<nbTrans; k++ ) {
           ImQ[i][k] += ratio * ImQ[j][k];
         }
         tau[i] += ratio * tau[j];
       }
       if ( _debug ) {
         printf("Gauss/down ImQ[%d] = \n", j );
         DUMP
       }
     }
     /* Gauss phase 2: normalizing */
     for( int i=0; i<nbTrans; i++ ) {
       double rat = 1.0/ImQ[i][i];
       tau[i] *= rat;
       for ( int j=i; j<nbTrans; j++ )
         ImQ[i][j] *= rat;
     }
     if ( _debug ) {
       printf("Gauss/Matrix B/N = \n" );
       DUMP
     }
     /* Gauss phase 2: going up! */
     for( int j=nbTrans-1; j>=0; j-- ) {
       for( int i=0; i<j; i++ ) {
         tau[i] -= ImQ[i][j] * tau[j];
       }
       if ( _debug ) {
         printf("Gauss2/Matrix H[%d] =", j );
         for( int k=0; k<nbTrans; k++ )
           printf(" %f", tau[k] );
         printf("\n");
       }
     }

     // expand the tau vector into the complete hitting time vector
     double *tau_complete = (double*)calloc( _stateSpaceSize, sizeof(double) );
     for( int i = 0; i < nbTrans; i++ ) {
       tau_complete[alias[i]] = tau[i];
     }

     // cleanup
     free( tau );
     free( alias );
     for ( int i = 0; i < nbTrans; i++ ) {
       free( ImQ[i] );
     }
     free( ImQ );

     // done!
     return tau_complete;
}

double* markovChain::averageHittingTimeDT_iterative( bool *hitSetIndicator )
{
  double *tau = (double*)malloc( _stateSpaceSize * sizeof(double) );
  double *ntau = (double*)malloc( _stateSpaceSize * sizeof(double) );

  // computing an initial value for tau from a lower bound
  double minPro = 1.0;
  for ( int i = 0; i < _stateSpaceSize; i++ ) {
    if ( !hitSetIndicator[i] ) {
      double total = 0.0;
      int nbTrans = _generator->getNbElts(i);
      for ( int k = 0; k < nbTrans; k++ ) {
        if ( !hitSetIndicator[_generator->getCol(i,k)] ) {
          total += _generator->getEntryByCol(i,k);
        }
      }
      if ( _debug ) {
        fprintf( stdout, "Internal proba from state %3d: %8.6f\n", i, total );
      }

      if ( total < minPro ) minPro = total;
    }
  }
  double minTau = 1.0/(1.0-minPro);
  fprintf( stdout, "Minimal tau: %10f\n", minTau );
  for ( int i = 0; i < _stateSpaceSize; i++ ) {
    tau[i] = ( hitSetIndicator[i] ? 0.0 :minTau );
  }

  // performing iterations
  double distL1;
  int nbIter = 0;
  int maxIter = 10000;

  do {
    // evaluate P x tau
    _generator->evaluateValue( tau, ntau );

    // add the vector of ones and correct the new tau,
    // calculating L1 distance at the same time.
    distL1 = 0.0;
    for ( int i = 0; i < _stateSpaceSize; i++ ) {
      if ( hitSetIndicator[i] ) {
        ntau[i] = 0.0;
      }
      else {
        ntau[i] += 1.0;
        distL1 += fabs( tau[i] - ntau[i] );
      }
    }
    nbIter++;

    // swap tau and new tau
    double *tmp = tau; tau = ntau; ntau = tmp;

    if ( _debug ) {
      fprintf( stderr, "Hitting time iteration #%5d, distance = %12e\n", nbIter, distL1 );
    }

  } while( ( distL1 > 1e-8 ) && ( nbIter < maxIter ) );

  if ( nbIter == maxIter ) {
    fprintf( stderr, "Warning in markovChain::averageHittingTimeDT_iterative: " );
    fprintf( stderr, "maximum number of iterations reached. Result may be imprecise\n" );
  }

  // cleanup
  free( ntau );

  // done!
  return tau;
}

double* markovChain::averageHittingTime( bool *hitSetIndicator )
{
  double*	distRes = (double*)NULL;

    switch( _type )
    {
    case DISCRETE:
      distRes = averageHittingTimeDT( hitSetIndicator );
      break;

    case CONTINUOUS: {
      markovChain* unif = uniformize();
      distRes = unif->averageHittingTimeDT( hitSetIndicator );
      // divide by the uniformization factor

      double nu = unif->generator()->uniformizationRate();
      for ( int i = 0; i < _stateSpaceSize; i++ ) {
        distRes[i] = distRes[i] / nu;
      }

      delete unif;
    }
      break;

    default:
      fprintf( stdout, "Error: unknown Chain type. No solution.\n" );
    }


    return distRes;

}

void markovChain::write( FILE *out, bool withReward )
{
  if ( _type == UNKNOWN ) {
    fprintf( stdout, "Error: unknown chain type. No output.\n" );
    return;
  }
  if ( withReward ) {
    MARMOTE_MC_WARNING( "Rewards not handled. Ignored." );
  }

  switch( _type ) {
  case CONTINUOUS:
    fprintf( out, "continuous sparse\n" );
    break;

  case DISCRETE:
    fprintf( out, "discrete sparse\n" );
    break;

  case UNKNOWN:
    break;
  }

  fprintf( out, "%d\n", _stateSpaceSize );

  _generator->write( out, "Ers" );
  // write reward when there is one

  fprintf( out, "stop\n" );
}

void markovChain::write(string format, string modelName)
{
  if ( _isAbstract ) {
    // exit with no action. Could check if format is the same as the abstract one, though...
    fprintf( stdout, "Error in write(): cannot handle abstract chains. No output.\n");
    return;
  }

  // assert: chain is not abstract
  FILE* out;

  if ( format == "Ers" ) {
    string fileName = modelName + ".mcl";
    out = fopen( fileName.c_str(), "w" );
    if ( NULL != out ) {
      write( out, /*reward?*/ false );

      if ( (Distribution*)NULL != _initDistribution ) {
        fprintf( out, "%d\n", (int)rint(_initDistribution->sample()) );
      }
      else {
        fprintf( out, "0\n" );
      }
      fclose( out );

    }
    else {
      fprintf( stderr, "Error: in markovChain::write(): cannot open file '%s'' for writing.\n",
               fileName.c_str() );
    }
  }
  else if ( format == "R" ) {
    string fileName = modelName + ".R";
    out = fopen( fileName.c_str(), "w" );
    if ( NULL != out ) {
      fprintf( out, "%s", toString("R").c_str() );

      // no initial distribution in the R format

      fclose( out );
    }
    else {
      fprintf( stderr, "Error: in markovChain::write(): cannot open file '%s'' for writing.\n",
               fileName.c_str() );
    }
  }else if ( format == "SCILAB" ) {
    string fileName = modelName + ".sce";
    out = fopen( fileName.c_str(), "w" );
    if ( NULL != out ) {

      fprintf( out, "%s", toString("SCILAB").c_str() );

      fclose( out );
    }
  else {
    fprintf(  stderr, "Error: in markovChain::write(): cannot open file '%s'' for writing.\n",
             format.c_str() );

       }
} else {
    fprintf( stderr, "Error: in markovChain::write(): format '%s' not supported.\n",
             format.c_str() );

  }

}

Distribution *  markovChain::read()
{

    Distribution * d=NULL;

if(format()=="XbornePres")
    {
      int sz=0;
      double * vals=NULL;
       double * probas=NULL;

//deserialization

      // char *mgp=(char *)(modelName().c_str());


      // mainGthLD(mgp,"Rii", &probas, &sz);


     setFormat("Xborne");

    d= new discreteDistribution(sz,vals,probas);

     }


    return d;
}


void markovChain::setSizeType(const string path)
{
  ifstream fp(path.c_str());
  double value;
  int in,ent,NbRow,NbCol,n;

  fp >> NbRow; // rows number
  fp >> NbCol; // columns number
  _stateSpaceSize = NbRow;
  fp >> n;     // non zero values number

  // read transition entries until a diagonal element is read
  bool keepReading = true;
  for(int i = 0; ( i < n ) && keepReading; i++)
    {
      fp >> in;
      fp >> ent;
      fp >> value;

      if ( in == ent ) {
	if ( value < 0.0 ) {
	  _type = CONTINUOUS;
	}
	else {
	  _type = DISCRETE;
	}
	keepReading = false;
      }
    }
  fp.close();

  cerr << "File " << path << ": found chain of size " << _stateSpaceSize
       << " and type " << ( _type == CONTINUOUS ? "continuous" : "discrete" )
       << endl;

}

std::string markovChain::toString( std::string format )
{
  std::string res;
  
  if ( format == "R" ) {
    
    std::string matrice= _generator->toString("R"); // the matrix

    // creation of the list of state names
    std::string statesNames="statesNames<-c(";

    for( int i=0; i<_stateSpaceSize; i++ ) {
      std::ostringstream tmp;
      tmp<<i;
      statesNames=statesNames+"\""+tmp.str()+"\"";

      if( i != _stateSpaceSize-1){
	statesNames=statesNames+",";
      }
    }
    statesNames=statesNames+")";

    // the complete R instruction
    res = statesNames + ";" + "matrice<-" + matrice + ";" +
      "mcA<-new(\"markovchain\", states=statesNames, transitionMatrix=matrice);";
  }else if ( format == "SCILAB" ) 
       {
     res= _generator->toString("SCILAB"); 
  }
  else {
    fprintf( stderr, "Warning in markovChain::toString(): format %s not recognized. Ignored.",
             format.c_str() );
  }

  return res;
}

int markovChain::period() {
  sparseMatrix * sparse = dynamic_cast<sparseMatrix *>(_generator);
  if (!sparse) {
    throw "Error: period() works only with sparseMatrix as a transitionStructure";
  }
  return sparse->period();
}

std::vector<markovChain *> * markovChain::subChains() {
  sparseMatrix * sparse = dynamic_cast<sparseMatrix *>(_generator);
  if (!sparse) {
    throw "Error: subChains() works only with sparseMatrix as a transitionStructure";
  }

  std::vector < std::vector<int> > recurrents = this->recurrentClasses();

  std::vector<markovChain *> * result = new std::vector<markovChain *>();
  for (unsigned int i = 0; i < recurrents.size(); ++i) {
    markovChain * chain = new markovChain(recurrents[i].size(), DISCRETE);
    chain->setGenerator(sparse->newFiltered(recurrents[i]));
    result->push_back(chain);
  }

  return result;
}



