/* Marmote is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Marmote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Marmote. If not, see <http://www.gnu.org/licenses/>.

Copyright 2015 Alain Jean-Marie, Jean-Michel Fourneau, Jean-Marc Vincent, Issam Rabhi */

/*****************************************************************************/
/* Ajout de fonctionalites sur les matrices BD1D (1-Dimensional Birth&Death) */
/* Ce sont des matrices qui sont caracterisees par:
   - leur taille N
   - un taux de saut a droite lambda
   - un taux de saut a gauche mu
*/

#include "poissonProcess.h"
#include "../Distribution/diracDistribution.h"
#include "../Distribution/poissonDistribution.h"
#include <stdlib.h>


poissonProcess::poissonProcess( double lambda )
  : homogeneous1DBirthDeath( lambda, 0.0 )
{
  _stateSpace = (double*)NULL;
  _initDistribution = new diracDistribution( 0 );

}

poissonProcess::~poissonProcess()
{
  // delete _initDistribution; // NO: is done in mother class desctructor
}

Distribution* poissonProcess::transientDistribution( double t )
{
  Distribution* res;

  double avg = _lambda*t;
  res = new poissonDistribution( avg );

  return res;
}

geometricDistribution* poissonProcess::stationaryDistribution()
{
  geometricDistribution* res;

  res = new geometricDistribution( 1.0 );

  return res;
}

simulationResult* poissonProcess::simulateChain( double tMax,
			 bool stats, bool traj,
			 bool withIncrements,
			 bool trace )
{
  int		curState;
  int		nextState;
  int		maxState; // the maximum attained state. NOT the maximum possible value
  double	time;
  double	adv;
  double	*cumTime;
  double	*proba;
  double	*dates;
  int		*states;
  double	*increments;
  simulationResult* res;
  int		maxTrajLength;
  int		curTrajLength = 0;

  // arbitrary initialization
  maxState = 1;

  res = new simulationResult( _size, CONTINUOUS, stats );
  maxTrajLength = 1024;
  if ( traj ) {
    /* allocate initial memory (1k words) for trajectory */
    res->setTrajectory(true);
    dates = (double*)malloc( maxTrajLength * sizeof(double) );
    states = (int*)malloc( maxTrajLength * sizeof(int) );
    if ( withIncrements ) {
      increments = (double*)malloc( maxTrajLength * sizeof(double) );
    }
  }

  // Sample from initial distribution
  curState = _initDistribution->sample();

  time = 0.0;

  if ( stats ) {
    cumTime = (double*) calloc( (size_t)(1+maxState), sizeof( double ) );
    // allocation of proba is deferred until the correct size is known
  }

  do {
    // first check if there is room for more trajectory
    if ( curTrajLength >= maxTrajLength ) {
      maxTrajLength += 1024;
      dates = (double*)realloc( dates, maxTrajLength * sizeof(double) );
      states = (int*)realloc( states, maxTrajLength * sizeof(int) );
      if ( withIncrements ) {
	increments = (double*)realloc( increments,
				       maxTrajLength * sizeof(int) );
      }
    }

    adv = Distribution::exponential( 1.0/_lambda );
    nextState = curState+1;

    if ( stats ) {
      cumTime[ curState ] += ( time+adv >= tMax ? tMax - time : adv );
    }

    if ( traj ) {

      dates[curTrajLength] = time;
      states[curTrajLength] = curState;
      if ( withIncrements ) {
	increments[curTrajLength] = adv;
      }

      if ( trace ) {
	printf("[%4d] %14.8f %4d", curTrajLength, time, curState );

	if ( withIncrements )
	  printf(" %10.6f", adv );

	printf("\n");
      }
    }

    time += adv;
    curState = nextState; 

    // if ( trace ) {
    //   printf("%10.4f %4d\n", time, curState );
    // }

    curTrajLength++;
  } while ( time < tMax );

  if ( stats ) {
    // prepare the result as a discrete distribution
    proba = (double*)malloc( (size_t)(1+maxState) * sizeof( double ) );
    double *stateSpace = (double*)malloc( (size_t)(1+maxState) * sizeof( double ) );
    for ( int i = 0; i <= maxState; i++ ) {
      stateSpace[i] = i;
      proba[i] = (double)cumTime[i] / tMax;
    }

    discreteDistribution *dis;
    dis = new discreteDistribution( maxState, _stateSpace, proba );
    res->setDistribution( dis );

    free( cumTime );
    free( proba );
    free( stateSpace );
  }

  if ( traj ) {
    res->setTrajectorySize(curTrajLength);
    res->setTrajectory(dates,states);
  }

  return res;
}
