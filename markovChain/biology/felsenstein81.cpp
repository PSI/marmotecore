/* Marmote is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Marmote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Marmote. If not, see <http://www.gnu.org/licenses/>.

Copyright 2015 Alain Jean-Marie, Jean-Michel Fourneau, Jean-Marc Vincent, Issam Rabhi */

#include "felsenstein81.h"
#include "../../Distribution/Distribution.h"
#include "../../Distribution/diracDistribution.h"
#include "../../Distribution/exponentialDistribution.h"
#include "../../transitionStructure/sparseMatrix.h"
#include <stdlib.h>


felsenstein81::felsenstein81( double p[4], double mu )
  : markovChain( 4, CONTINUOUS )
{
  _size = 4;

  // need to create a table of arbitrary values for discreteDistribution objects
  // (should be something like ATGC...)
  _stateSpace = (double*) malloc( 4 *sizeof(double) );  
  for ( int i = 0; i < 4; i++ ) {
    _stateSpace[i] = (double)i;
  }

  _transDistrib = new discreteDistribution( 4, _stateSpace, p );
  _transSpeed = mu;

  int initState = 0;
  _initDistribution = new diracDistribution( initState );

}


felsenstein81::felsenstein81( discreteDistribution *d, double mu )
  : markovChain( 4, CONTINUOUS )
{
  _transDistrib = d->copy();
  _transSpeed = mu;

  // need to create a table of arbitrary values for discreteDistribution objects
  // (should be something like ATGC...)
  _stateSpace = (double*) malloc( 4 *sizeof(double) );  
  for ( int i = 0; i < 4; i++ ) {
    _stateSpace[i] = (double)i;
  }

  int initState = 0;
  _initDistribution = new diracDistribution( initState );

}


felsenstein81::~felsenstein81()
{
  free( _stateSpace );
  delete( _transDistrib );
}


void felsenstein81::makeMarkovChain()
{

  _generator = new sparseMatrix( 4 );
  _generator->setType( CONTINUOUS );

  for ( int i = 0; i < 4; i++ ) {
    for ( int j = 0; j < 4; j++ ) {
      if ( i == j ) {
	_generator->setEntry( i, i, -_transSpeed*(1.0 -_transDistrib->getProba(i)) );
      }
      else {
	_generator->setEntry( i, j, _transSpeed*_transDistrib->getProba(j) );
      }
    }
  }
}


discreteDistribution* felsenstein81::transientDistribution( double t )
{
  int		i;
  double	expmut;
  double	dist[4];
  double	vals[4];
  discreteDistribution *res;

  for ( i=0; i<4; i++ ) {
    vals[i] = _transDistrib->getValue(i);
    expmut = exp( - _transSpeed * t );
    dist[i] = expmut * _initDistribution->getProba(i) 
      + ( 1 - expmut ) * _transDistrib->getProba(i);
  }

  res = new discreteDistribution( 4, vals, dist );

  return res;
}


discreteDistribution* felsenstein81::transientDistribution( int fromState, double t )
{
  int		i;
  double	dist[4];
  double	vals[4];
  discreteDistribution *iDis_save = _initDistribution;
  discreteDistribution *res;

  // should check whether ifrom is correct
  
  // initialize initial distribution
  for ( i=0; i<4; i++ ) {
    vals[i] = _transDistrib->getValue(i);
    dist[i] = 0;
  }
  dist[fromState] = 1.0;
  _initDistribution = new discreteDistribution( 4, vals, dist );
  
  res = transientDistribution( t );

  delete _initDistribution;
  _initDistribution = iDis_save;
  
  return res;
}


discreteDistribution* felsenstein81::stationaryDistribution()
{
  discreteDistribution* res;

  res = _transDistrib->copy(); /* duplicate distrib */

  return res;
}

simulationResult* felsenstein81::simulateChain( double tMax,
						bool stats, bool traj,
						bool withIncrements,
						bool trace )
{
  int		curState;
  double	time;
  double	*cumTime;
  double	*rates; // transition rates from each state
  double	*proba;
  double	*dates;
  int		*states;
  double	*increments;
  simulationResult* res;
  int		maxTrajLength;
  int		curTrajLength = 0;

  res = new simulationResult( _size, CONTINUOUS, stats );
  maxTrajLength = 1024;
  if ( traj ) {
    /* allocate initial memory (1k words) for trajectory */
    res->setTrajectory(true);
    dates = (double*)malloc( maxTrajLength * sizeof(double) );
    states = (int*)malloc( maxTrajLength * sizeof(int) );
    if ( withIncrements ) {
      increments = (double*)malloc( maxTrajLength * sizeof(double) );
    }
  }

  // set up transition rates to speed up things a little
  rates = (double*) malloc( _size * sizeof(double) );
  for ( int i = 0; i < _size; i++ ) {
    rates[i] = _transSpeed * ( 1.0 - _transDistrib->getProba(i) );
  }

  // Sample from initial distribution
  curState = _initDistribution->sample();
  
  time = 0.0;

  if ( stats ) {
    cumTime = (double*) calloc( (size_t)_size, sizeof( double ) );
    proba = (double*) calloc( (size_t)_size, sizeof( double ) );
  }

  do {

    if ( traj && ( curTrajLength >= maxTrajLength ) ) {
      maxTrajLength += 1024;
      dates = (double*)realloc( dates, maxTrajLength * sizeof(double) );
      states = (int*)realloc( states, maxTrajLength * sizeof(int) );
      if ( withIncrements ) {
	increments = (double*)realloc( increments,
				       maxTrajLength * sizeof(double) );
      }
    }
    double nu = rates[curState];
    double adv = Distribution::exponential( 1.0/nu );
    double u = Distribution::u_0_1() * nu;
    int nextState;

    for ( int j=0; ( j < _size ) && ( 0.0 <= u ); j++ ) {
      if ( j != curState ) {
	u -= _transSpeed * _transDistrib->getProba(j);
      }
      if ( u < 0.0 ) {
	nextState = j;
      }
    }

    if ( stats ) {
      cumTime[ curState ] += ( time+adv >= tMax ? tMax - time : adv );
    }

    if ( traj ) {

      dates[curTrajLength] = time;
      states[curTrajLength] = curState;
      if ( withIncrements ) {
	increments[curTrajLength] = adv;
      }

      if ( trace ) {
	printf("[%4d] %12.6f %4d", curTrajLength, time, curState );

	if ( withIncrements )
	  printf(" %10.6f", adv );

	printf("\n");
      }
    }

    time += adv;
    curState = nextState;

    curTrajLength++;
  } while ( time < tMax );

  // missing: adjust end of simulation as in the general method

  if ( stats ) {
    for ( int i = 0; i < _size; i++ ) {
      proba[i] = cumTime[i] / tMax;
    }
    discreteDistribution *dis =
      new discreteDistribution( _size, _stateSpace, proba );
    res->setDistribution( dis );

    free( cumTime );
    free( proba );
  }

  if ( traj ) {
    res->setTrajectorySize(curTrajLength);
    res->setTrajectory(dates,states);

    free(dates);
  }

  free( rates );

  return res;
}

Distribution* felsenstein81::hittingTimeDistribution( int iState, bool* hittingSet )
{
  double pHit = 0.0;
  for ( int i = 0; i < _stateSpaceSize; i++ ) {
    if ( hittingSet[i] ) {
      pHit += _transDistrib->getProba(i);
    }
  }

  Distribution* res = new exponentialDistribution( _transSpeed * pHit );

  return res;
}

double* felsenstein81::averageHittingTime( bool* hittingSet )
{
  double pHit = 0.0;
  for ( int i = 0; i < _stateSpaceSize; i++ ) {
    if ( hittingSet[i] ) {
      pHit += _transDistrib->getProba(i);
    }
  }

  double* res = (double*)malloc( _stateSpaceSize*sizeof(double) );
  for ( int i = 0; i < _stateSpaceSize; i++ ) {
    if ( hittingSet[i] ) {
      res[i] = 0.0;
    }
    else {
      res[i] = 1.0 / _transSpeed / pHit;
    }
  }

  return res;
}
