/*
 * markovChain_lumping.cpp
 *
 *  Created on: May 31, 2017
 *      Author: hmykhail
 */

#include "markovChain.h"
#include "block.h"
#include "state.h"
#include <set> // for std::set
#include <cfloat> // for DBL_MAX

double pmc(double * arr, int size);

struct CompareBasedW {
  double * w;
  CompareBasedW(double * w) {
    this->w = w;
  }
  bool operator()(lumping::state * &a, lumping::state * &b) const {
    return w[a->id] < w[b->id];
  }
};

int totalStates(std::set<lumping::block *> st) {
  int c = 0;
  for (std::set<lumping::block *>::iterator it = st.begin(); it != st.end();
      ++it) {
    c += (*it)->size();
  }
  return c;
}
int totalStates(std::vector<lumping::block *> st) {
  int c = 0;
  for (std::vector<lumping::block *>::iterator it = st.begin(); it != st.end();
      ++it) {
    c += (*it)->size();
  }
  return c;
}



std::vector<std::vector<int> > * markovChain::computeLumpings() {
  double UNUSED = DBL_MAX;
  double IGNORE = 0.0;

  int newBlockId = 0;

  double * w;
  lumping::state ** states;
  std::set<lumping::block *> Ub;  // unprocessed blocks
  std::set<lumping::block *> Bt;  // touched blocks
  std::set<lumping::block *> result;  // results?

  // initialization
  states = new lumping::state*[stateSpaceSize()];

  lumping::block * b = new lumping::block(newBlockId++);
  for (int i = 0; i < stateSpaceSize(); ++i) {
    lumping::state * s = new lumping::state(i);
    states[i] = s;
    b->addUnmarkedState(s);
  }
  Ub.insert(b);

  w = new double[stateSpaceSize()];
  for (int i = 0; i < stateSpaceSize(); ++i) {
    w[i] = UNUSED;
  }
  sparseMatrix * sparse = dynamic_cast<sparseMatrix *>(_generator);
  if (!sparse) {
    throw "Error: stronglyConnectedComponents works only with sparseMatrix as a transitionStructure";
  }
  sparseMatrix * reverted = sparse->getReverted();


  while (!Ub.empty()) {

    lumping::block * Bp = *(Ub.begin());
    Ub.erase(Ub.begin());
    std::set<lumping::state *> St;  // touched states

    // traverse all the states // TOOD traverse also marked
    for (lumping::block::const_iterator it = Bp->begin(); it != Bp->end();
        ++it) {
      lumping::state * current = *it;
      int n = reverted->getNbElts(current->id);
      for (int i = 0; i < n; ++i) {
        int col = reverted->getCol(current->id, i);
        double prob = reverted->getEntry(current->id, col);
        if (prob > IGNORE) {
          if (w[col] == UNUSED) {
            St.insert(states[col]);
            w[col] = prob;
          } else {
            w[col] += prob;
          }
        }
      }
    }
    for (std::set<lumping::state *>::iterator it = St.begin(); it != St.end();
        ++it) {
      lumping::state * s = *it;
      if (w[s->id] != 0) {
        lumping::block * B = s->owner;
        if (B->marked == NULL) {
          Bt.insert(B);
        }
        B->markState(s);
      }

    }
    while (!Bt.empty()) {
      lumping::block * B = *(Bt.begin());
      Bt.erase(Bt.begin());

      std::vector<lumping::block *> blocks;
      lumping::block * B1;

      if (B->unmarked == NULL) {
        B1 = B;
      } else {
        B1 = new lumping::block(newBlockId++);
        B1->marked = B->marked;
        B->marked = NULL;
        B1->sizeMarked = B->sizeMarked;
        B->sizeMarked = 0;
        B->marked = NULL;
      }
      blocks.push_back(B1);
      int index = 0;
      double* arrPmc = new double[B1->size()];
      for (lumping::block::const_iterator it = B1->begin(); it != B1->end();
          ++it) {
        arrPmc[index] = w[(*it)->id];
        index++;
      }
      double valuePmc = pmc(arrPmc, B1->size());
      lumping::block * B2 = new lumping::block(newBlockId++);
      for (lumping::block::const_iterator it = B1->begin(); it != B1->end();
          ++it) {
        lumping::state * s = *it;
        if (w[s->id] != valuePmc) {
          B2->addUnmarkedState(s);
        }
      }
      if (B2->size() != 0) {
        blocks.push_back(B2);
        int B2size = B2->size();
        lumping::state ** states = new lumping::state*[B2->size()];
        int i = 0;
        for (lumping::block::const_iterator it = B2->begin(); it != B2->end();
            ++it) {
          states[i++] = *it;
        }
        // sort and partition B2 according to w[s], yielding B2, ..., Bl
        std::sort(states, states + B2->size(), CompareBasedW(w));

        B2->clear();
        double firstW = w[states[0]->id];
        int j = 0;
        for (; j < B2size; ++j) {
          if (w[states[j]->id] == firstW) {
            B2->addUnmarkedState(states[j]);
          } else {
            break;
          }
        }
        while (j < B2size) {
          lumping::block * newBlock = new lumping::block(newBlockId++);
          blocks.push_back(newBlock);
          firstW = w[states[j]->id];
          for (; j < B2size; ++j) {
            if (w[states[j]->id] == firstW) {
              newBlock->addUnmarkedState(states[j]);
            } else {
              break;
            }
          }
        }
      }

      if (Ub.find(B) != Ub.end()) {  // if B in Ub
          // add B1,..., Bl

        for (std::vector<lumping::block *>::iterator it = blocks.begin();
            it != blocks.end(); ++it) {
          Ub.insert(*it);

        }
      } else {
        lumping::block * largest = *(blocks.begin());
        for (std::vector<lumping::block *>::iterator it = blocks.begin();
            it != blocks.end(); ++it) {
          lumping::block * curr = *it;
          if (curr->size() > largest->size()) {
            largest = curr;
          }
        }
        for (std::vector<lumping::block *>::iterator it = blocks.begin();
            it != blocks.end(); ++it) {
          if (*it != largest) {
            Ub.insert(*it);
          }
        }
        result.insert(largest);
      }

    }
    for (std::set<lumping::state *>::iterator it = St.begin(); it != St.end();
        ++it) {
      lumping::state * s = *it;
      w[s->id] = UNUSED;
    }
  }

  std::vector < std::vector<int> > *r = new std::vector<std::vector<int> >();
  for (std::set<lumping::block *>::iterator it = result.begin();
      it != result.end(); ++it) {
    std::vector<int> * v = new std::vector<int>();

    for (lumping::block::const_iterator cit = (*it)->begin();
        cit != (*it)->end(); ++cit) {
      v->push_back((*cit)->id);
    }

    r->push_back(*v);
  }
  return r;
}

double pmc(double * arr, int size) {
  double pmc = 0;
  int count = 0;
  for (int i = 0; i < size; ++i) {
    if (count == 0) {
      pmc = arr[i];
      count = 1;
    } else if (pmc == arr[i]) {
      ++count;
    } else {
      --count;
    }
  }
  return pmc;
}
