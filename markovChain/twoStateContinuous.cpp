/* Marmote is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Marmote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Marmote. If not, see <http://www.gnu.org/licenses/>.

Copyright 2015 Alain Jean-Marie, Jean-Michel Fourneau, Jean-Marc Vincent, Issam Rabhi */

/*****************************************************************************/
/* Ajout de fonctionalites sur les matrices BD1D (1-Dimensional Birth&Death) */
/* Ce sont des matrices qui sont caracterisees par:
   - leur taille N
   - un taux de saut a droite lambda
   - un taux de saut a gauche mu
*/

#include "twoStateContinuous.h"
#include "../Distribution/diracDistribution.h"
#include "../transitionStructure/sparseMatrix.h"
#include <stdlib.h>


twoStateContinuous::twoStateContinuous( double alpha, double beta )
  : markovChain( 2, CONTINUOUS ),
    _alpha(alpha), _beta(beta)
{
_stateSpace = (double*)calloc(2,sizeof(double));
_stateSpace[0] = 0; _stateSpace[1] = 1;
_initDistribution = new diracDistribution( 0 );

}

twoStateContinuous::~twoStateContinuous()
{
  if ( (double*)NULL != _stateSpace ) {
    free( _stateSpace );
  }
  // delete _initDistribution; // NO: is done in mother class desctructor
}

void twoStateContinuous::makeMarkovChain()
{
  _generator = new sparseMatrix( _stateSpaceSize );
  _generator->setType( CONTINUOUS );

  _generator->setEntry( 0, 0, -_alpha );
  _generator->setEntry( 0, 1, _alpha );
  _generator->setEntry( 0, 1, _beta );
  _generator->setEntry( 1, 1, -_beta );

}

bernoulliDistribution* twoStateContinuous::transientDistribution( double t )
{
  double	totRate = _alpha + _beta;
  double	rho = _beta / totRate;
  double	expo = exp( -totRate * t );
  double	pi0 = _initDistribution->getValue(0);

  double v;
  v = rho - expo / totRate * ( - _alpha * pi0 + _beta * ( 1 - pi0 ) );

  bernoulliDistribution *res;
  res = new bernoulliDistribution( v );

  return res;
}


bernoulliDistribution* twoStateContinuous::stationaryDistribution()
{
  double	totRate = _alpha + _beta;
  double	rho = _beta / totRate;

  bernoulliDistribution *res;
  res = new bernoulliDistribution( rho );

  return res;
}

simulationResult* twoStateContinuous::simulateChain( double tMax,
			 bool stats, bool traj,
			 bool withIncrements,
			 bool trace )
{
  int		curState;
  int		nextState;
  double	time;
  double	adv;
  double	*cumTime;
  double	*proba;
  double	*dates;
  int		*states;
  double	*increments;
  simulationResult* res;
  int		maxTrajLength;
  int		curTrajLength = 0;

  res = new simulationResult( _stateSpaceSize, CONTINUOUS, stats );
  maxTrajLength = 1024;
  if ( traj ) {
    /* allocate initial memory (1k words) for trajectory */
    res->setTrajectory(true);
    dates = (double*)malloc( maxTrajLength * sizeof(double) );
    states = (int*)malloc( maxTrajLength * sizeof(int) );
    if ( withIncrements ) {
      increments = (double*)malloc( maxTrajLength * sizeof(double) );
    }
  }

  // Sample from initial distribution
  curState = _initDistribution->sample();

  time = 0.0;

  if ( stats ) {
    cumTime = (double*) calloc( (size_t)_stateSpaceSize, sizeof( double ) );
  }

  do {
    // first check if there is room for more trajectory
    if ( curTrajLength >= maxTrajLength ) {
      maxTrajLength += 1024;
      dates = (double*)realloc( dates, maxTrajLength * sizeof(double) );
      states = (int*)realloc( states, maxTrajLength * sizeof(int) );
      if ( withIncrements ) {
	increments = (double*)realloc( increments,
				       maxTrajLength * sizeof(int) );
      }
    }
    switch( curState ) {
    case 0:
      adv = Distribution::exponential( 1.0/_alpha );
      nextState = 1;
      break;

    case 1:
      adv = Distribution::exponential( 1.0/_beta );
      nextState = 0;
      nextState = 1;
    }

    if ( stats ) {
      cumTime[ curState ] += ( time+adv >= tMax ? tMax - time : adv );
    }

    if ( traj ) {

      dates[curTrajLength] = time;
      states[curTrajLength] = curState;
      if ( withIncrements ) {
	increments[curTrajLength] = adv;
      }

      if ( trace ) {
	printf("[%4d] %14.8f %4d", curTrajLength, time, curState );

	if ( withIncrements )
	  printf(" %10.6f", adv );

	printf("\n");
      }
    }

    time += adv;
    curState = nextState; 

    // if ( trace ) {
    //   printf("%10.4f %4d\n", time, curState );
    // }

    curTrajLength++;
  } while ( time < tMax );

  if ( stats ) {
    // prepare the result as a discrete distribution
    proba = (double*)malloc( (size_t)_stateSpaceSize * sizeof( double ) );
    for ( int i = 0; i < _stateSpaceSize; i++ ) {
      proba[i] = (double)cumTime[i] / tMax;
    }

    discreteDistribution *dis;
    dis = new discreteDistribution( _stateSpaceSize, _stateSpace, proba );
    res->setDistribution( dis );

    free( cumTime );
    free( proba );
  }

  if ( traj ) {
    res->setTrajectorySize(curTrajLength);
    res->setTrajectory(dates,states);
  }

  return res;
}

// Methods of structural analysis
std::vector<int> twoStateContinuous::absorbingStates()
{
  std::vector<int> res;

  if ( _alpha == 0.0 )
    res.push_back(0);
  if ( _beta == 0.0 )
    res.push_back(1);

  return res;
}

std::vector< std::vector<int> > twoStateContinuous::recurrentClasses()
{
  std::vector< std::vector<int> > res;

  if ( ( _alpha > 0.0 ) && ( _beta > 0.0 ) ) {
    // this is the ergodic case: one recurrent class
    std::vector<int> clas;
    clas.push_back(0);
    clas.push_back(1);
    res.push_back(clas);
  }
  else if ( ( _alpha == 0.0 ) && ( _beta == 0.0 ) ) {
    // then there are two classes
    std::vector<int> clas0;
    clas0.push_back(0);
    std::vector<int> clas1;
    clas1.push_back(1);
    res.push_back(clas0);
    res.push_back(clas1);
  }
  else if ( _alpha == 0.0 ) {
    // one recurrent class: 0
    std::vector<int> clas;
    clas.push_back(0);
    res.push_back(clas);
  }
  else if ( _beta == 0.0 ) {
    // one recurrent class: 1
    std::vector<int> clas;
    clas.push_back(1);
    res.push_back(clas);
  }

  return res;
}

std::vector< std::vector<int> > twoStateContinuous::communicatingClasses()
{
  std::vector< std::vector<int> > res;

  if ( ( _alpha > 0.0 ) && ( _beta > 0.0 ) ) {
    // this is the ergodic case
    std::vector<int> clas;
    clas.push_back(0);
    clas.push_back(1);
    res.push_back(clas);
  }
  else {
    // then there are two classes
    std::vector<int> clas0;
    clas0.push_back(0);
    std::vector<int> clas1;
    clas1.push_back(1);
    res.push_back(clas0);
    res.push_back(clas1);
  }

  return res;
}

bool twoStateContinuous::isirreducible()
{
  bool res;

  res = ( _alpha > 0.0 ) && ( _beta > 0.0 );

  return res;
}

bool twoStateContinuous::isaccessible( int stateFrom, int stateTo )
{
  bool res = false;

  if ( stateFrom == stateTo ) {
    res = true;
  }
  else if ( stateFrom == 0 ) {
    // then stateTo = 1
    res = ( _alpha > 0.0 );
  }
  else if ( stateFrom == 1 ) {
    // then stateTo = 0
    res = ( _beta > 0.0 );
  }

  return res;
}
