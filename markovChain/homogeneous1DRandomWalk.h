/* Marmote is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Marmote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Marmote. If not, see <http://www.gnu.org/licenses/>.

Copyright 2015 Alain Jean-Marie, Jean-Michel Fourneau, Jean-Marc Vincent, Issam Rabhi */

#ifndef HOMOGENOUS1DRANDOMWALK_H
#define HOMOGENOUS1DRANDOMWALK_H

#include "markovChain.h"
#include "../Distribution/discreteDistribution.h"
#include "../Distribution/geometricDistribution.h"

/**
 * @brief The 1-dimensional random walk with homogeneous transition probabilities.
 * This model is characterized by:
 * - the size N, possibly INFINITE_STATE_SPACE_SIZE: number of states
 * - the probability to jump to the right, p
 * - the probability to jump to the left, q
 * with p + q <= 1. The probability to stay at the same position is 1 - p - q.
*/

class homogeneous1DRandomWalk : public markovChain 
{
 protected:
  int _size;		/**< @brief the number of states; possibly infinite */
  double* _stateSpace;	/**< @brief representation of the state space when finite */
  double _p;            /**< @brief the probability to jump to the right */
  double _q;            /**< @brief the probability to jump to the left */
  double _r; 		/**< @brief r = 1 - p - q is the proba of staying in the same state */

 public:

/**
 * @brief Constructor for a homogeneous1DRandomWalk chain with infinite state space
 * @author	Alain Jean-Marie
 * @param p	right-jump probability
 * @param q	left-jump probability
 * @return	an object of type homogeneous1DRandomWalk, with initial state set arbitrarily to 0.
 */
  homogeneous1DRandomWalk( double p, double q );
  /**
   * @brief Constructor for a homogeneous1DRandomWalk chain
   * @author	Alain Jean-Marie
   * @param n	size of the state space of the chain
   * @param p	right-jump probability
   * @param q	left-jump probability
   * @return	an object of type markovChain, with initial state set arbitrarily to 0.
  */
  homogeneous1DRandomWalk( int n, double p, double q );

  /**
   * @brief Destructor for a homogeneous1DRandomWalk chain
   * @author	Alain Jean-Marie
   */
  ~homogeneous1DRandomWalk();

  /**
   * @brief Instantiation of the generator for the markovChain ancestor
   * @author	Alain Jean-Marie
   */
  void makeMarkovChain();

  /**
   * @brief Computes the transient distribution for a homogeneous1DRandomWalk chain
   * @author	Alain Jean-Marie
   * @param t	time at which the distribution is to be computed
   * @param nMax maximal state for the range on which the distribution is to be computed
   * @return a discrete distribution where the result is stored
   */
  discreteDistribution* approxTransientDistribution( int t, int nMax );
  /**
   * @brief Computes the stationary distribution for a homogeneous1DRandomWalk
   * chain. When the state space is infinite, the Geometric distribution is returned.
   * If the chain is not stable, the (improper) geometric distribution with
   * parameter 1 is returned. When the state space is finite, the truncated
   * Geometric distribution is returned as a discreteDistribution object.
   * @author Alain Jean-Marie
   * @return a distribution
   */
  Distribution* stationaryDistribution();
  /**
   * @brief Computes the stationary distribution for a homogeneous1DRandomWalk
   * chain. When the state space is infinite, the Geometric distribution is returned.
   * If the chain is not stable, the (improper) geometric distribution with
   * parameter 1 is returned. When the state space is finite, the truncated
   * Geometric distribution is returned as a discreteDistribution object.
   * @author Alain Jean-Marie 
   * @param nMax the actual maximal state in the random walk
   * @return a distribution
   */
  // discreteDistribution* stationaryDistribution( int nMax );
  /**
   * @brief Simulates the evolution of a homogeneous1DRandomWalk chain
   * @author	Alain Jean-Marie
   * @param tMax	time until which the Markov chain is simulated
   * @param stats	indicates whether occupancy statistics are collected and returned
   * @param traj	indicates whether a trajectory is returned
   * @param trace	indicates whether the trajectory should be printed along the way (on stdout)
   * @return a structure with the results of the simulation
   */
  simulationResult*
    simulateChain( long int tMax, bool stats, bool traj, bool trace );

 public:
  void write(string format, string prefix);

};

#endif // HOMOGENOUS1DRANDOMWALK_H
