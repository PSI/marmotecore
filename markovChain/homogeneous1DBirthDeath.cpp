/* Marmote is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Marmote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Marmote. If not, see <http://www.gnu.org/licenses/>.

Copyright 2015 Alain Jean-Marie, Jean-Michel Fourneau, Jean-Marc Vincent, Issam Rabhi */

/*****************************************************************************/
/* Ajout de fonctionalites sur les matrices BD1D (1-Dimensional Birth&Death) */
/* Ce sont des matrices qui sont caracterisees par:
   - leur taille N
   - un taux de saut a droite lambda
   - un taux de saut a gauche mu
*/

#include "homogeneous1DBirthDeath.h"
#include "../Distribution/diracDistribution.h"
#include "../transitionStructure/sparseMatrix.h"
#include <stdlib.h>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

homogeneous1DBirthDeath::homogeneous1DBirthDeath( double lambda, double mu )
: markovChain( INFINITE_STATE_SPACE_SIZE, CONTINUOUS ),
				 _size(INFINITE_STATE_SPACE_SIZE), _lambda(lambda), _mu(mu)
{
  _stateSpace = (double*)NULL;
  _initDistribution = new diracDistribution( 0 );

}

homogeneous1DBirthDeath::homogeneous1DBirthDeath( int n, 
						  double lambda, double mu )
  : markovChain( n, CONTINUOUS ),
    _size(n), _lambda(lambda), _mu(mu)
{

  _stateSpace = (double*)malloc( _size * sizeof(double) );
  for ( int i = 0; i < _size; i++ ) {
    _stateSpace[i] = i;
  }

  _initDistribution = new diracDistribution( 0 );

}

homogeneous1DBirthDeath::~homogeneous1DBirthDeath()
{
  if ( (double*)NULL != _stateSpace ) {
    free( _stateSpace );
  }
  // delete _initDistribution; // NO: is done in mother class desctructor
}

void homogeneous1DBirthDeath::makeMarkovChain()
{
  if ( _size == INFINITE_STATE_SPACE_SIZE ) {
    fprintf( stderr, "Warning in homogeneous1DRandomWalk::makeMarkovChain():" );
    fprintf( stderr, " cannot handle infinite chains. Ignored.\n" );
    return;
  }
  
  _generator = new sparseMatrix( _size );
  _generator->setType( CONTINUOUS );

  // have to check that _size >= 2
  _generator->setEntry( 0, 1, _lambda );

  for ( int i = 1; i < _size; i++ ) {
    _generator->setEntry( i, i+1, _lambda );
    _generator->setEntry( i, i-1, _mu );
  }

  _generator->setEntry( _size-1, _size-2, _mu );

}

discreteDistribution* homogeneous1DBirthDeath::approxTransientDistribution( 
								    double t, int nMax )
{
  int		i;
  double	expt;
  double	*distribution;
  double	*stateSpace;
  discreteDistribution *res;

  /* perform an interpolation between stationary distribution and initial
     distribution: this is wrong, but enough for the initial DTK experiment
  */
  distribution = (double*)malloc( (size_t)(1+nMax) * sizeof(double) );
  stateSpace = (double*)malloc( (size_t)(1+nMax) * sizeof( double ) );

  expt = exp( -(_lambda+_mu)*t );
  
  geometricDistribution *statDis = stationaryDistribution();

  for ( i=0; i<=nMax; i++ ) {
    /* printf( "%8.6f\n", expmut * p0[i] + ( 1 - expmut ) * p[i] ); */
    stateSpace[i] = i;
    distribution[i] = expt * _initDistribution->getProba(i)
      + ( 1 - expt ) * statDis->getProba(i);
  }

  res = new discreteDistribution( 1+nMax, stateSpace, distribution );

  free( distribution );

  return res;
}


discreteDistribution* homogeneous1DBirthDeath::transientDistribution( 
								    double t, int nMax )
{
  int		sz;		// size of the matrix
  double	rho;
  double	*distribution;
  double	*stateSpace;
  double	*gamma;
  double	*omega;
  discreteDistribution *res;

  sz = 1+nMax;

  // use the formulas of the AMM class notes
  distribution = (double*)malloc( (size_t)(sz) * sizeof(double) );
  stateSpace = (double*)malloc( (size_t)(sz) * sizeof( double ) );
  gamma = (double*)malloc( (size_t)(sz) * sizeof(double) );
  omega = (double*)malloc( (size_t)(sz) * sizeof(double) );

  rho = _lambda / _mu;
  gamma[0] = ( rho == 1.0 ? sz : (1.0-pow(rho,_size))/(1.0-rho) );
  omega[0] = 0.0;

  for ( int i = 1; i < _size; i++ ) {
    double cosipi = cos(i*M_PI/sz);
    gamma[i] = sz/2.0 * ( 1.0 - 2.0/sqrt(rho)*cosipi + 1.0/rho );
    omega[i] = -(_lambda+_mu) + 2.0*sqrt(_lambda*_mu)*cosipi;
  }
  
  res = new discreteDistribution( 1+nMax, stateSpace, distribution );

  free( distribution );

  return res;
}


geometricDistribution* homogeneous1DBirthDeath::stationaryDistribution()
{
  double rho;
  geometricDistribution* res;

  rho = _lambda / _mu;

  if ( rho < 1.0 ) {
    res = new geometricDistribution( rho );
  }
  else {
    res = new geometricDistribution( 1.0 );
  }

  return res;

}


discreteDistribution* homogeneous1DBirthDeath::stationaryDistribution( int nMax )
{
  double	*distribution;
  double	*stateSpace;
  discreteDistribution *res;

  distribution = (double*)malloc( (size_t)(1+nMax) * sizeof(double) );
  stateSpace = (double*)malloc( (size_t)(1+nMax) * sizeof( double ) );

  // Computing the non-normalized distribution, together with
  // the normalization constant.
  distribution[0] = 1.0;
  double sum = distribution[0];
  for ( int i=1; i<=nMax; i++ ) {
    distribution[i] = distribution[i-1] * (_lambda/_mu);
    sum += distribution[i];
  }
  /* Normalization of the distribution */
  for ( int i=0; i<=nMax; i++ ) {
    stateSpace[i] = i;
    distribution[i] = distribution[i] / sum;
  }

  res = new discreteDistribution( 1+nMax, stateSpace, distribution );

  return res;
}

simulationResult* homogeneous1DBirthDeath::simulateChain( double tMax,
			 bool stats, bool traj,
			 bool withIncrements,
			 bool trace )
{
  int		curState;
  int		nextState;
  int		maxState; // the maximum attained state. NOT the maximum possible value
  double	time;
  double	u;
  double	nu;
  double	adv;
  double	pArr;
  bool		isFinite;
  double	*cumTime;
  double	*proba;
  double	*dates;
  int		*states;
  double	*increments;
  simulationResult* res;
  int		maxTrajLength;
  int		curTrajLength = 0;

  nu = _lambda + _mu;
  pArr = _lambda/nu;
  isFinite = ( _size != INFINITE_STATE_SPACE_SIZE );

  if ( isFinite ) {
    maxState = _size-1;
  }
  else {
    // arbitrary initialization
    maxState = 1;
  }

  res = new simulationResult( _size, CONTINUOUS, stats );
  maxTrajLength = 1024;
  if ( traj ) {
    /* allocate initial memory (1k words) for trajectory */
    res->setTrajectory(true);
    dates = (double*)malloc( maxTrajLength * sizeof(double) );
    states = (int*)malloc( maxTrajLength * sizeof(int) );
    if ( withIncrements ) {
      increments = (double*)malloc( maxTrajLength * sizeof(double) );
    }
  }

  // Sample from initial distribution
  curState = _initDistribution->sample();

  time = 0.0;

  if ( stats ) {
    cumTime = (double*) calloc( (size_t)(1+maxState), sizeof( double ) );
    // allocation of proba is deferred until the correct size is known
  }

  do {
    // first check if there is room for more trajectory
    if ( curTrajLength >= maxTrajLength ) {
      maxTrajLength += 1024;
      dates = (double*)realloc( dates, maxTrajLength * sizeof(double) );
      states = (int*)realloc( states, maxTrajLength * sizeof(int) );
      if ( withIncrements ) {
	increments = (double*)realloc( increments,
				       maxTrajLength * sizeof(int) );
      }
    }
    if ( curState > 0 ) {
      if ( !isFinite || ( curState < _size-1 ) ) {
	adv = Distribution::exponential( 1.0/nu );
	u = Distribution::u_0_1();
	if ( u < pArr ) {
	  nextState = curState+1;
	  maxState = ( nextState > maxState ? nextState : maxState );
	}
	else {
	  nextState = curState-1;
	}
      }
      else {
	// Is finite and max value reached. Only departure is possible
	adv = Distribution::exponential( 1.0/_mu );
	nextState = curState-1;
      }
    }
    else {
      // curState == 0
      adv = Distribution::exponential( 1.0/_lambda );
      nextState = 1;
    }

    if ( stats ) {
      cumTime[ curState ] += ( time+adv >= tMax ? tMax - time : adv );
    }

    if ( traj ) {

      dates[curTrajLength] = time;
      states[curTrajLength] = curState;
      if ( withIncrements ) {
	increments[curTrajLength] = adv;
      }

      if ( trace ) {
	printf("[%4d] %14.8f %4d", curTrajLength, time, curState );

	if ( withIncrements )
	  printf(" %10.6f", adv );

	printf("\n");
      }
    }

    time += adv;
    curState = nextState; 

    // if ( trace ) {
    //   printf("%10.4f %4d\n", time, curState );
    // }

    curTrajLength++;
  } while ( time < tMax );

  if ( stats ) {
    // prepare the result as a discrete distribution
    proba = (double*)malloc( (size_t)(1+maxState) * sizeof( double ) );
    double *stateSpace = (double*)malloc( (size_t)(1+maxState) * sizeof( double ) );
    for ( int i = 0; i <= maxState; i++ ) {
      stateSpace[i] = i;
      proba[i] = (double)cumTime[i] / tMax;
    }

    discreteDistribution *dis;
    dis = new discreteDistribution( maxState, _stateSpace, proba );
    res->setDistribution( dis );

    free( cumTime );
    free( proba );
    free( stateSpace );
  }

  if ( traj ) {
    res->setTrajectorySize(curTrajLength);
    res->setTrajectory(dates,states);
  }

  return res;
}
