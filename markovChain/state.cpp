/* Marmote is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Marmote is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Marmote. If not, see <http://www.gnu.org/licenses/>.

 Copyright 2015 Alain Jean-Marie, Jean-Michel Fourneau, Jean-Marc Vincent, Issam Rabhi */

#include "state.h"
#include <iostream>

lumping::state::state(int id){

  this->id = id;
  this->backward = NULL;
  this->forward = NULL;
  this->owner = NULL;
  this->marked = false;
}
