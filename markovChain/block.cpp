/* Marmote is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Marmote is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Marmote. If not, see <http://www.gnu.org/licenses/>.

 Copyright 2015 Alain Jean-Marie, Jean-Michel Fourneau, Jean-Marc Vincent, Issam Rabhi */

#include "block.h"
#include <iostream>
#include "markovChain.h"

lumping::block::block(int id) {
  this->id = id;
  this->sizeMarked = 0;
  this->sizeUnmarked = 0;
  this->marked = NULL;
  this->unmarked = NULL;
}


void lumping::block::addUnmarkedState(state * s) {
  // delete from existing block
  if (s->owner != NULL) {
    if (s->backward != NULL && s->forward != NULL) {
      s->backward->forward = s->forward;
      s->forward->backward = s->backward;
    } else if (s->backward != NULL) {
      s->backward->forward = NULL;
    } else if (s->forward != NULL) {
      s->forward->backward = NULL;
      if (s->owner->marked == s) {
        s->owner->marked = s->forward;
      } else {
        s->owner->unmarked = s->forward;
      }
    } else{
      if(s->marked){
        s->owner->marked = NULL;
      }else{
        s->owner->unmarked = NULL;
      }
    }
    if (s->marked) {
      --(s->owner->sizeMarked);
    } else {
      --(s->owner->sizeUnmarked);
    }
  }

  s->backward = NULL;
  s->forward = NULL;
  s->owner = NULL;

  s->backward = NULL;
  s->forward = this->unmarked;
  if (s->forward != NULL) {
    s->forward->backward = s;
  }
  s->owner = this;
  s->marked = false;
  ++(this->sizeUnmarked);
  this->unmarked = s;
}

bool lumping::block::operator<(const block & other) const {
  return this->id < other.id;
}

void lumping::block::markState(state * s) {
  if (s->backward != NULL && s->forward != NULL) {
    s->backward->forward = s->forward;
    s->forward->backward = s->backward;
  } else if (s->backward != NULL) {
    s->backward->forward = NULL;
  } else if (s->forward != NULL) {
    s->forward->backward = NULL;
    s->owner->unmarked = s->forward;
  } else {
    s->owner->unmarked = NULL;
  }

  s->backward = NULL;
  s->forward = this->marked;
  if (s->forward != NULL) {
    s->forward->backward = s;
  }
  s->marked = true;
  --(this->sizeUnmarked);
  ++(this->sizeMarked);
  this->marked = s;
}

lumping::block::const_iterator& lumping::block::const_iterator::operator++() {

  if (curr->forward == NULL && passedMarked == false) {
    passedMarked = true;
    curr = unmarked;
  } else {
    curr = curr->forward;
  }

  return *this;
}

void lumping::block::clear() {
  lumping::state * curr = this->unmarked;
  while (curr != NULL) {
    lumping::state * s = curr;
    curr = curr->forward;
    s->backward = NULL;
    s->forward = NULL;
    s->owner = NULL;
    s->marked = false;
  }
  this->unmarked = NULL;
  curr = this->marked;
  while (curr != NULL) {
    lumping::state * s = curr;
    curr = curr->forward;
    s->backward = NULL;
    s->forward = NULL;
    s->owner = NULL;
    s->marked = false;
  }
  this->marked = NULL;
  this->sizeMarked = 0;
  this->sizeUnmarked = 0;
}

int lumping::block::size() {
  return sizeMarked + sizeUnmarked;
}

bool lumping::block::const_iterator::operator!=(const const_iterator & other) {
  return this->curr != other.curr;
}

lumping::state * lumping::block::const_iterator::operator*() {
  return curr;
}

lumping::block::const_iterator lumping::block::begin() {
  lumping::block::const_iterator result;
  if (this->marked != NULL) {
    result.curr = this->marked;
    result.passedMarked = false;
    result.unmarked = this->unmarked;
  } else {
    result.curr = this->unmarked;
    result.passedMarked = true;
    result.unmarked = NULL;
  }
  return result;
}

lumping::block::const_iterator lumping::block::end() {
  lumping::block::const_iterator result;
  result.curr = NULL;
  return result;

}

