/* Marmote is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Marmote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Marmote. If not, see <http://www.gnu.org/licenses/>.

Copyright 2015 Alain Jean-Marie, Jean-Michel Fourneau, Jean-Marc Vincent, Issam Rabhi */

#include "simulationResult.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
using namespace std;


simulationResult::simulationResult( int size, timeType t, bool stats )
{
  _type = t;
  _stateSpaceSize = size;
  _hasDistrib = stats;
  _hasTrajectory = false;
  _distrib = (discreteDistribution*)NULL;
  _states = (int*)NULL;
  _dates = (double*)NULL;
  _increments = (double*)NULL;
}

// Constructor from files. Handles formats "PSI" and "PSI-sample"
simulationResult::simulationResult( string format, string modelName, bool stats )
{
  _distrib = (discreteDistribution*)NULL;
  _states = (int*)NULL;
  _dates = (double*)NULL;
  _increments = (double*)NULL;

  int maxState = 0; // used to estimate state space size

  if ( format == "PSI" ) {
    // time type is discrete a priori
    _type = DISCRETE;
    _stateSpaceSize = 0; // to be determined
    _hasDistrib = stats;
    _hasTrajectory = true;

    // open file
    string fileName = modelName + ".traj";
    FILE *in = fopen( fileName.c_str(), "r" );
    if ( NULL == in ) {
      cerr << "Error in simulationResult(): cannot open trajectory file '"
	   << fileName << "' for reading. Empty trajectory returned." << endl;
      _trajectorySize = 0;
      _states = (int*)NULL;
      _distrib = (discreteDistribution*)NULL;
    }
    else {
      // parse input file
      char *line = (char*)NULL;
      size_t len = 0;
      ssize_t read;
      int iState;
      int stateNb = 0;

      read = getline(&line, &len, in);
      while ( read != -1 ) {
        // skip empty lines and comment lines
        if ( ( read > 0 ) && ( line[0] != '#' ) ) {
          // detect lines with metadata: have at least 2 characters
          if ( ( line[0] == ' ' ) && ( strlen(line) > 2 ) ) {

	    switch ( line[1] ) {
	    case 'I':
	      // this should be the information on initial state
	      sscanf( line, " Initial state : %d", &iState );
	      break;

	    case 'L': 
	      {
	      // this should be the information on the trajectory length
	      int nbRead = 
		sscanf( line, " Length of trajectory : %d", 
			&(_trajectorySize) );

	      if ( ( nbRead == 1 ) && ( _trajectorySize > 0 ) ) {
	      // add one: the initial state
		_trajectorySize ++;
		_states = (int*) malloc( _trajectorySize * sizeof(int) );
		_states[0] = iState;
	      }
	      else {
		cerr << "Warning in simulationResult(): bad line '" 
		     << line << "' in file '" << fileName << "'" << endl;
	      }
	      }
	      break;

	    default:
	      // anything else is a bad line
	      cerr << "Warning in simulationResult(): bad line '" 
		   << line << "' in file '" << fileName << "'" << endl;
	    }

	  }
	  else if ( strlen(line) > 2 ) {
	    // assuming that the line contains data
	    int theState;
	    int nbRead = sscanf( line, "%d", &theState );
	    if ( nbRead == 1 ) {
	      // looks good!
	      stateNb++;
	      _states[stateNb] = theState;
	      if ( theState > maxState ) maxState = theState;
	    }
	    else {
	     cerr << "Warning in simulationResult(): bad line '" 
		   << line << "' in file '" << fileName << "'" << endl;
	    } 
	  }
	  else {
	    // skip silently
	  }
	}

	// read next line
	read = getline(&line, &len, in);
      }

      if ( (char*)NULL != line ) free(line);
      fclose( in );
    }
    if ( stats ) {
      if ( _trajectorySize > 0 ) {
	// state space size is unknown. Estimate it with maximal state
	_stateSpaceSize = maxState + 1;

	int *count = 
	  (int*) calloc( (size_t)_stateSpaceSize, sizeof( int ) );
	double *proba = 
	  (double*) calloc( (size_t)_stateSpaceSize, sizeof( double ) );
	double *values = 
	  (double*) calloc( (size_t)_stateSpaceSize, sizeof( double ) );

	for ( int i = 0; i < _trajectorySize; i++ ) {
	  count[ _states[i] ]++;
	}
	for ( int i = 0; i < _stateSpaceSize; i++ ) {
	  values[i] = (double)i;
	  proba[i] = (double)count[i] / _trajectorySize;
	}

	_distrib = new discreteDistribution( _stateSpaceSize, values, proba );

	free( values );
	free( proba );
	free( count );
      }
      else {
        _distrib = (discreteDistribution*)NULL;
      }
    }
  }
  else if ( format == "PSI-sample" ) {
    // time type is discrete a priori
    _type = DISCRETE;
    _stateSpaceSize = 0; // to be determined
    _hasDistrib = stats;
    _hasTrajectory = true;

    // open file
    string fileName = modelName + ".sample";
    FILE *in = fopen( fileName.c_str(), "r" );
    if ( NULL == in ) {
      cerr << "Error in simulationResult(): cannot open sample file '"
       << fileName << "' for reading. Empty sample returned." << endl;
      _trajectorySize = 0;
      _states = (int*)NULL;
      _distrib = (discreteDistribution*)NULL;
    }
    else {
      // parse input file
      char *line = (char*)NULL;
      size_t len = 0;
      ssize_t read;
      int stateNb = 0;
      _trajectorySize = 0;

      // First: locate the size of the sample.
      read = getDataLine(&line, &len, in);
      if ( 1 == sscanf( line, "%d", &_trajectorySize ) ) {
        // that should be it
        cerr << "+++ read sample size: " << _trajectorySize << endl;
      }
      else {
        cerr << "Error in simulationResult(): failed to find sample size. Abort." << endl;
      }

      // allocate memory
      _states = (int*) malloc( _trajectorySize*sizeof(int) );
      _dates = (double*) malloc( _trajectorySize*sizeof(double) );

      // continue scanning the file
      read = getDataLine( &line, &len, in );
      while( ( read != -1 ) && ( stateNb < _trajectorySize ) ) {

        // well formed lines should contain two integers
        int theState;
        double theDate;
        if ( 2 == sscanf( line, "%d %lf", &theState, &theDate ) ) {
          _states[stateNb] = theState;
          _dates[stateNb] = theDate;
          stateNb++;
          if ( theState > maxState ) maxState = theState;
        }
        else {
          cerr << "Warning in simulationResult(): bad line '"
               << line << "' in file '" << fileName << "'" << endl;
        }

        // read next line
        read = getDataLine(&line, &len, in);
      }

      if ( (char*)NULL != line ) free(line);
      fclose( in );

      if ( stats ) {
        if ( _trajectorySize > 0 ) {
          // state space size is unknown. Estimate it with maximal state
          _stateSpaceSize = maxState + 1;

          int *count =
              (int*) calloc( (size_t)_stateSpaceSize, sizeof( int ) );
          double *proba =
              (double*) calloc( (size_t)_stateSpaceSize, sizeof( double ) );
          double *values =
              (double*) calloc( (size_t)_stateSpaceSize, sizeof( double ) );

          for ( int i = 0; i < _trajectorySize; i++ ) {
            count[ _states[i] ]++;
          }
          for ( int i = 0; i < _stateSpaceSize; i++ ) {
            values[i] = (double)i;
            proba[i] = (double)count[i] / _trajectorySize;
          }

          _distrib = new discreteDistribution( _stateSpaceSize, values, proba );

          free( values );
          free( proba );
          free( count );
        }
        else {
          _distrib = (discreteDistribution*)NULL;
        }
      }
    }
  }
  else {
    cerr << "Error in simulationResult(): format '" << format << "' not supported. Ignored."
         << endl;
  }
}

simulationResult::~simulationResult() 
{
  if ( (discreteDistribution*)NULL != _distrib ) {
    delete _distrib;
  }
  if ( (int*)NULL != _states ) {
    free( _states );
  }
  if ( (double*)NULL != _dates ) {
    free( _dates );
  }
  if ( (double*)NULL != _increments ) {
    free( _increments );
  }
}

// Output procedure for trajectories. No need for an output procedure for
// distributions, since getDistribution() returns the distribution at no cost,
// and the discreteDistribution object has an output procedure.
void simulationResult::writeTrajectory(FILE* out, string format)
{
  if ( !_hasTrajectory || ( (double*)NULL == _dates ) || ( (int*)NULL == _states ) ) {
    fprintf( stderr, "warning in simulationResult::writeTrajectory(): no trajectory found. Ignored.\n" );
  }
  else {
    if ( ( format == "" ) || ( format == "standard" ) || ( format == "Ers" ) ) {
      for ( int i = 0; i < _trajectorySize; i++ ) {
        if ( _type == DISCRETE ) {
        fprintf( out, "%6d %4d\n", (int)round(_dates[i]), _states[i] );
        }
        else {
          printf("[%4d] %12.6f %4d\n", i, _dates[i], _states[i] );
        }
      }
    }
    else {
      fprintf( stderr, "warning in simulationResult::writeTrajectory(): format '%s' not supported. Ignored.\n",
               format.c_str() );
    }
  }
}

// improvement of readline that skips comments and empty lines
ssize_t simulationResult::getDataLine(char** line, size_t *n, FILE* stream)
{
  bool containsData = false;
  int read;

  while( !containsData && ( read != -1 ) ) {
    read = getline( line, n, stream );
    containsData = ( read > 0 ) && ( (*line)[0] != '#' ) && ( (*line)[0] != '%' ) && ( (*line)[0] != '\n' );
    if ( containsData ) {
      // skip white space
      bool isEmpty = true;
      for ( int i = 0; isEmpty && ( i < read ); i++ ) {
        isEmpty = ( (*line)[i] == ' ' ) || ( (*line)[i] == '\t' ) || ( (*line)[i] == '\n' );
      }
      containsData = !isEmpty;
    }
  }

  return read;
}
