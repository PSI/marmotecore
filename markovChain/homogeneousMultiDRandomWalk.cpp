/* Marmote is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Marmote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Marmote. If not, see <http://www.gnu.org/licenses/>.

Copyright 2015 Alain Jean-Marie, Jean-Michel Fourneau, Jean-Marc Vincent, Issam Rabhi */

#include "homogeneousMultiDRandomWalk.h"
#include "../Distribution/diracDistribution.h"
#include "../Distribution/discreteDistribution.h"
#include "../transitionStructure/sparseMatrix.h"
#include "../transitionStructure/multiDimHomTransition.h"
#include <stdlib.h>

// Constructor for a random walk with infinite state space
homogeneousMultiDRandomWalk::homogeneousMultiDRandomWalk( int nbDims, double* p, double* q )
: markovChain( INFINITE_STATE_SPACE_SIZE, DISCRETE ), _nbDims(nbDims), _p(p), _q(q)
{
  _r = 1.0;
  for ( int d = 0; d < _nbDims; d++ ) {
    _r = _r - ( _p[d] + _q[d] ); // should test the values of these three probas
  }
  _stateSpace = (double*)NULL;

  _dimSize = (int*)malloc( _nbDims*sizeof(int) );
  for ( int d = 0; d < _nbDims; d++ ) {
    _dimSize[d] = INFINITE_STATE_SPACE_SIZE;
  }

  _generator = new multiDimHomTransition( _nbDims, _dimSize, _p, _q );

  _initDistribution = new diracDistribution( 0 );
  
  // creating and filling the _mu array for consistency. Probably not very useful.
  _mu = (int*)malloc( _nbDims*sizeof(int) );
  for ( int d = 0; d < _nbDims; d++ ) {
    _mu[d] = INFINITE_STATE_SPACE_SIZE;
  }

  _isFinite = false;
  _modelName = "multiDRW";
}

// Constructor for a random walk with finite state space.
// The super constructor is called with INFINITE because we can't compute the exact size now.
homogeneousMultiDRandomWalk::homogeneousMultiDRandomWalk(int nbDims, int* sz, double* p, double* q )
  : markovChain( INFINITE_STATE_SPACE_SIZE, DISCRETE ), _p(p), _q(q)
{
  _nbDims = nbDims;
  _stateSpaceSize = 1;
  _isFinite = true;
  for ( int d = 0; d < _nbDims; d++ ) {
    if ( sz[d] != INFINITE_STATE_SPACE_SIZE ) {
      _stateSpaceSize *= sz[d];
    }
    else {
      _isFinite = false;
      _stateSpaceSize = INFINITE_STATE_SPACE_SIZE;
    }
  }

  _r = 1.0;
  for ( int d = 0; d < _nbDims; d++ ) {
    _r = _r - ( _p[d] + _q[d] ); // should test the values of these three probas
  }

  // Deferring the creation of the state space to when it is really needed
  _stateSpace = (double*)NULL;

  _dimSize = sz;

  _generator = new multiDimHomTransition( _nbDims, _dimSize, _p, _q );

  _initDistribution = new diracDistribution( 0 );

  // creating and filling the _mu array
  _mu = (int*)malloc( _nbDims*sizeof(int) );
  _mu[_nbDims-1] = 1;
  for ( int d = _nbDims-2; d >= 0; d-- ) {
    _mu[d] = _mu[d+1] * _dimSize[d+1];
  }

  _modelName = "multiDRW";
}

// Standard destructor.
homogeneousMultiDRandomWalk::~homogeneousMultiDRandomWalk()
{
  if ( _stateSpace != NULL )
    free( _stateSpace );
  free( _mu );
  // delete _initDistribution; // NO: is done in mother class desctructor
}

void homogeneousMultiDRandomWalk::makeMarkovChain()
{
  if ( !_isFinite ) {
    fprintf( stderr, "Warning in homogeneousMultiDRandomWalk::makeMarkovChain():" );
    fprintf( stderr, " cannot handle infinite chains. Ignored.\n" );
    return;
  }

  if ( _nbDims != 2 ) {
    fprintf( stderr, "Warning in homogeneousMultiDRandomWalk::makeMarkovChain():" );
    fprintf( stderr, " cannot handle dimensions larger than 2. Ignored.\n" );
    return;
  }

  _stateSpace = (double*)malloc( _stateSpaceSize * sizeof(double) );
  for ( int i = 0; i < _stateSpaceSize; i++ ) {
    _stateSpace[i] = i;
  }
  sparseMatrix* newgen = new sparseMatrix( _stateSpaceSize );
  newgen->setType( DISCRETE );

  for ( int i = 0; i < _dimSize[0]; i++ ) {
    for ( int j = 0; j < _dimSize[1]; j++ ) {
      int stateNum = j*_dimSize[0]+i;
      newgen->addToEntry( stateNum, (i==_dimSize[0]-1?stateNum:stateNum+1), _p[0] );
      newgen->addToEntry( stateNum, (i==0?stateNum:stateNum-1), _q[0] );
      newgen->addToEntry( stateNum, (j==_dimSize[1]?stateNum:stateNum+_dimSize[0]), _p[1] );
      newgen->addToEntry( stateNum, (j==0?stateNum:stateNum-_dimSize[0]), _q[1] );
      newgen->addToEntry( stateNum, stateNum, _r );
    }
  }

  delete _generator;
  _generator = newgen;

}

discreteDistribution* homogeneousMultiDRandomWalk::stationaryDistribution()
{
  discreteDistribution* res = (discreteDistribution*)NULL;

  if ( !_isFinite ) {
    fprintf( stderr, "Warning in homogeneousMultiDRandomWalk::stationaryDistribution():" );
    fprintf( stderr, " cannot handle infinite chains. Null result returned.\n" );
    return res;
  }

  double* probas = (double*)malloc( _stateSpaceSize * sizeof(double) );
  int* stateBuffer = (int*)malloc( _nbDims*sizeof(int) );
  double *rho = (double*)malloc( _nbDims*sizeof(double) );
  double *norm = (double*)malloc( _nbDims*sizeof(double) );

  // initialize frequently used quantities
  for ( int d = 0; d < _nbDims; d++ ) {
    rho[d] = _p[d] / _q[d];
    if ( _p[d] == _q[d] ) {
      norm[d] = 1.0 / ( 1.0 + _dimSize[d] );
    }
    else {
      norm[d] = ( 1.0 - rho[d] )/( 1.0 - pow( rho[d], _dimSize[d] ) );
    }
  }

  // initialize state buffer
  for ( int d = 0; d < _nbDims; d++ ) {
    stateBuffer[d] = 0;
  }

  for ( int i = 0; i < _stateSpaceSize; i++ ) {
    double pro = 1.0;
    for ( int d = 0; d < _nbDims; d++ ) {
      pro *= pow( rho[d], stateBuffer[d] ) * norm[d];
    }
    probas[i] = pro;

    // increment state
    stateBuffer[_nbDims-1]++;
    for ( int d =_nbDims-1; ( d >= 0 ) && ( stateBuffer[d] == _dimSize[d] ); d-- ) {
      if ( d > 0 )
        stateBuffer[d-1]++;
      stateBuffer[d] = 0;
    }
  }

  // finished
  res = new discreteDistribution( _stateSpaceSize, _stateSpace, probas );

  // cleanup
  free( stateBuffer );
  free( probas );

  return res;
}

int* homogeneousMultiDRandomWalk::simulateHittingTime( int iState, bool *hittingSet, int nbSamples, int tMax )
{
  multiDimHomTransition* nGen = dynamic_cast<multiDimHomTransition*>(_generator);
  discreteDistribution* jump = nGen->getJumpDistribution();
  int* res = (int*)malloc( nbSamples*sizeof(int) );
  int* stateBuffer = (int*)malloc( nbSamples*sizeof(int) );

  for ( int i = 0; i < nbSamples; i++ ) {
    int curState = iState;
    // decode initial state in the state buffer
    int idx = iState;
    for ( int d = _nbDims-1; d >= 0; d-- ) {
      stateBuffer[d] = idx % _dimSize[d];
      idx = idx / _dimSize[d];
    }
    int hitT = 0;

    // fprintf( stdout, "Traj #%d: %d", i, curState );

    while( !hittingSet[curState] && ( hitT < tMax ) ) {
      hitT++;
      int theJump = jump->sample();
      // fprintf( stderr, "%d\n", theJump );
      if ( theJump != 0 ) {
        int theDim = abs(theJump)-1;
        int theDir = theJump/abs(theJump);
        if ( ( stateBuffer[theDim] + theDir >= 0 ) && ( stateBuffer[theDim] + theDir < _dimSize[theDim] ) ) {
          // accept the jump
          curState += theDir*_mu[theDim];
          stateBuffer[theDim] += theDir;
        }
      }
      // fprintf( stdout, " %d", curState );
    }
    res[i] = hitT;
    // fprintf( stdout, "\n");
  }

  // cleanup
  delete jump;
  free ( stateBuffer );

  //finished
  return res;
}

void homogeneousMultiDRandomWalk::write(string format)
{
  FILE* out;

  if ( format == "XBORNE" ) {
    string filename = modelName() + ".Rii";
    out = fopen( filename.c_str(), "w" );
    multiDimHomTransition* gen = dynamic_cast<multiDimHomTransition*>(_generator);
    gen->write( out, format );
    fclose(out);

    filename = modelName() + ".sz";
    out = fopen( filename.c_str(), "w" );
    int stSz = stateSpaceSize();
    int nbTr = 1 + 2*_nbDims;
    for ( int i = 0; i < _nbDims; i++ ) nbTr *= _dimSize[i];
    for ( int i = 0; i < _nbDims; i++ ) nbTr -= (2*stSz)/_dimSize[i];
    fprintf( out, "\t%d\n", nbTr );
    fprintf( out, "\t%d\n", stSz );
    fprintf( out, "\t%d\n", _nbDims );
    fclose(out);
  }
  else {
    fprintf( stderr, "Error: in homogeneousMultiDRandomWalk::write(): format '%s'' not supported.\n",
             format.c_str() );
  }

}

