/* Marmote is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Marmote is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Marmote. If not, see <http://www.gnu.org/licenses/>.

Copyright 2015 Alain Jean-Marie, Jean-Michel Fourneau, Jean-Marc Vincent, Issam Rabhi */

/*****************************************************************************/
/* Ajout de fonctionalites sur les matrices RW1D (1-Dimensional Random Walk) */
/* Ce sont des matrices qui sont caracterisees par:
   - leur taille N
   - une proba de saut a droite p
   - une proba de saut a gauche q
*/
#include "homogeneous1DRandomWalk.h"
#include "../Distribution/diracDistribution.h"
#include "../transitionStructure/sparseMatrix.h"
#include <stdlib.h>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

homogeneous1DRandomWalk::homogeneous1DRandomWalk( double p, double q )
: markovChain( INFINITE_STATE_SPACE_SIZE, DISCRETE ),
				 _size(INFINITE_STATE_SPACE_SIZE), _p(p), _q(q)
{
  _r = 1.0 - _p - _q; // should test the values of these three probas
  _stateSpace = (double*)NULL;
  _initDistribution = new diracDistribution( 0 );

}

homogeneous1DRandomWalk::homogeneous1DRandomWalk( int n, double p, double q )
  : markovChain( n, DISCRETE ),
    _size(n), _p(p), _q(q)
{

  _r = 1.0 - _p - _q; // should test the values of these three probas
  _stateSpace = (double*)malloc( _size * sizeof(double) );
  for ( int i = 0; i < _size; i++ ) {
    _stateSpace[i] = i;
  }

  _initDistribution = new diracDistribution( 0 );

}

homogeneous1DRandomWalk::~homogeneous1DRandomWalk()
{
  if ( (double*)NULL != _stateSpace ) {
    free( _stateSpace );
  }
  // delete _initDistribution; // NO: is done in mother class desctructor
}

void homogeneous1DRandomWalk::makeMarkovChain()
{
  if ( _size == INFINITE_STATE_SPACE_SIZE ) {
    fprintf( stderr, "Warning in homogeneous1DRandomWalk::makeMarkovChain():" );
    fprintf( stderr, " cannot handle infinite chains. Ignored.\n" );
    return;
  }
  
  _generator = new sparseMatrix( _size );
  _generator->setType( DISCRETE );

  // have to check that _size >= 2
  _generator->setEntry( 0, 0, _q + _r );
  _generator->setEntry( 0, 1, _p );

  for ( int i = 1; i < _size-1; i++ ) {
    _generator->setEntry( i, i+1, _p );
    _generator->setEntry( i, i-1, _q );
    _generator->setEntry( i, i, _r );
  }

  _generator->setEntry( _size-1, _size-2, _q );
  _generator->setEntry( _size-1, _size-1, _p + _r );

}

discreteDistribution* homogeneous1DRandomWalk::approxTransientDistribution( int t,
								      int nMax )
{
  int		i;
  double	expt;
  double	*distribution;
  double	*stateSpace;
  discreteDistribution *res;

  /* perform an interpolation between stationary distribution and initial
     distribution: this is wrong, but enough for the initial DTK experiment
  */
  distribution = (double*)malloc( (size_t)(1+nMax) * sizeof(double) );
  stateSpace = (double*)malloc( (size_t)(1+nMax) * sizeof( double ) );

  expt = pow( _r + 2.0*sqrt(_p*_q)*cos(M_PI/nMax), t );
  
  discreteDistribution *statDis = dynamic_cast<discreteDistribution*>(stationaryDistribution());

  for ( i=0; i<=nMax; i++ ) {
    /* printf( "%8.6f\n", expmut * p0[i] + ( 1 - expmut ) * p[i] ); */
    stateSpace[i] = i;
    distribution[i] = expt * _initDistribution->getProba(i)
      + ( 1 - expt ) * statDis->getProba(i);
  }

  res = new discreteDistribution( 1+nMax, stateSpace, distribution );

  free( distribution );

  return res;
}

Distribution* homogeneous1DRandomWalk::stationaryDistribution()
{
  double rho = _p/_q;
  Distribution* res;

  if ( _stateSpaceSize == INFINITE_STATE_SPACE_SIZE ) {

    if ( rho < 1.0 ) {
      res = new geometricDistribution( rho );
    }
    else {
      res = new geometricDistribution( 1.0 );
    }

  }
  else {

    double	*distribution;
    double	*stateSpace;

    distribution = (double*)malloc( (size_t)(_stateSpaceSize) * sizeof(double) );
    stateSpace = (double*)malloc( (size_t)(_stateSpaceSize) * sizeof( double ) );

    // Computing the non-normalized distribution, together with
    // the normalization constant.
    distribution[0] = 1.0;
    double sum = distribution[0];
    for ( int i=1; i<_stateSpaceSize; i++ ) {
      distribution[i] = distribution[i-1] * rho;
      sum += distribution[i];
    }
    /* Normalization of the distribution */
    for ( int i=0; i<_stateSpaceSize; i++ ) {
      stateSpace[i] = i;
      distribution[i] = distribution[i] / sum;
    }

    res = new discreteDistribution( _stateSpaceSize, stateSpace, distribution );

    free( stateSpace );
    free( distribution );
  }

  return res;
}

simulationResult* homogeneous1DRandomWalk::simulateChain( long int tMax,
							  bool stats, 
							  bool traj, 
							  bool trace )
{
  int		curState;
  int		nextState;
  int		maxState; // the maximum attained state. NOT the maximum possible value
  long int	time;
  double	u;
  bool		isFinite;
  long int	*cumTime;
  double	*proba;
  int		*states;
  simulationResult* res;
  long int	maxTrajLength = tMax;

  isFinite = ( _size != INFINITE_STATE_SPACE_SIZE );

  if ( isFinite ) {
    maxState = _size-1;
  }
  else {
    // arbitrary initialization
    maxState = 1;
  }

  res = new simulationResult( _size, DISCRETE, stats );

  if ( traj ) {
    res->setTrajectory(true);
    states = (int*)malloc( maxTrajLength * sizeof(int) );
  }

  // Sample from initial distribution
  curState = _initDistribution->sample();

  time = 0;

  if ( stats ) {
    cumTime = (long int*) calloc( (size_t)(1+maxState), sizeof( long int ) );
    // allocation of proba is deferred until the correct size is known
  }

  for ( time = 0; time <= tMax; time++ ) {

    if ( curState > 0 ) {
      if ( !isFinite || ( curState < _size-1 ) ) {
	u = Distribution::u_0_1();
	if ( u < _p ) {
	  nextState = curState+1;
	  maxState = ( nextState > maxState ? nextState : maxState );
	}
	else if ( u > _p + _r ) {
	  nextState = curState-1;
	}
	// else no change
      }
      else {
	// Is finite and max value reached. Only going down is possible
	u = Distribution::u_0_1();
	if ( u < _q ) {
	  nextState = curState-1;
	}
	// else no change
      }
    }
    else {
      // curState == 0
      u = Distribution::u_0_1();
      if ( u < _p ) {
	nextState = 1;
      }
      // else no change
    }

    if ( stats ) {
      cumTime[ curState ] ++;
    }

    if ( traj ) {

      states[time] = curState;

      if ( trace ) {
	printf("%10ld %4d", time, curState );
	printf("\n");
      }
    }

    curState = nextState;

    if ( trace ) {
      printf("%10ld %4d\n", time, curState );
    }

  }

  if ( stats ) {
    // prepare the result as a discrete distribution
    proba = (double*)malloc( (size_t)(1+maxState) * sizeof( double ) );
    double *stateSpace = (double*)malloc( (size_t)(1+maxState) * sizeof( double ) );
    for ( int i = 0; i <= maxState; i++ ) {
      stateSpace[i] = i;
      proba[i] = (double)cumTime[i] / tMax;
    }

    discreteDistribution *dis;
    dis = new discreteDistribution( maxState, _stateSpace, proba );
    res->setDistribution( dis );

    free( cumTime );
    free( proba );
    free( stateSpace );
  }

  if ( traj ) {
    res->setTrajectorySize(tMax);
    res->setTrajectory((double*)NULL,states);
  }

  return res;
}

/**
 * @brief Writes a homogeneous1DRandomWalk chain in files with some specified format.
 * Supported formats:
 * - Xborne: both Rii and Cuu files are written
 * - Psi, Ers, "R"
 * @author Alain Jean-Marie
 * @param format format/language to be used
 * @param modelName the name of the model, usually used as prefix for the different files
 */

void homogeneous1DRandomWalk::write( string format, string modelName )
{
  if ( _size == INFINITE_STATE_SPACE_SIZE ) {
    cerr << "Warning in multiDimHomTransition::write(): cannot write infinite chains. Ignored."
         << endl;
    return;
  }

#define OPENERROR(name)  cerr << "Error in Homogenous1DRandomWalk::write(): Cannot open file \""\
       << name << "\" for writing." << endl;

  // Assert: chain is finite
  if ( format == "Xborne" ) {
    // write "size", "Rii", "Cuu" and "cd" files.
    string name = modelName + ".sz";
    FILE* out = fopen( name.c_str(), "w" );
    if ( NULL != out ) {
      fprintf( out, "%16d\n", 3*_size - 2 );
      fprintf( out, "%16d\n", _size );
      fprintf( out, "%16d\n", 1 );
      fclose(out);
    }
    else {
      OPENERROR(name);
    }
    
    name = modelName + ".Rii";
    out = fopen( name.c_str(), "w" );
    if ( NULL != out ) {
      fprintf( out, "%10d %10d", 0, 2 );
      fprintf( out, " %12e %10d", _q+_r, 0 );
      fprintf( out, " %12e %10d", _p, 1 );
      fprintf( out, "\n" );
      for ( int i = 1; i < _size-1; i++ ) {
        fprintf( out, "%10d %10d", i, 3 );
        fprintf( out, " %12e %10d", _q, i-1 );
        fprintf( out, " %12e %10d", _r, i );
        fprintf( out, " %12e %10d", _p, i+1 );
        fprintf( out, "\n" );
      }
      fprintf( out, "%10d %10d", _size-1, 2 );
      fprintf( out, " %12e %10d", _q, _size-2 );
      fprintf( out, " %12e %10d", _r+_p, _size-1 );
      fprintf( out, "\n" );
      fclose(out);
    }
    else {
      OPENERROR(name);
    }

    name = modelName + ".Cuu";
    out = fopen( name.c_str(), "w" );
    if ( NULL != out ) {
      fprintf( out, "%10d %10d", 0, 2 );
      fprintf( out, " %12e %10d", _q+_r, 0 );
      fprintf( out, " %12e %10d", _q, 1 );
      fprintf( out, "\n" );
      for ( int i = 1; i < _size-1; i++ ) {
        fprintf( out, "%10d %10d", i, 3 );
        fprintf( out, " %12e %10d", _q, i+1 );
        fprintf( out, " %12e %10d", _r, i );
        fprintf( out, " %12e %10d", _p, i-1 );
        fprintf( out, "\n" );
      }
      fprintf( out, "%10d %10d", _size-1, 2 );
      fprintf( out, " %12e %10d", _p, _size-2 );
      fprintf( out, " %12e %10d", _r+_p, _size-1 );
      fprintf( out, "\n" );
      fclose(out);
    }
    else {
      OPENERROR(name);
    }

    name = modelName + ".cd";
    out = fopen( name.c_str(), "w" );
    if ( NULL != out ) {
      for ( int i = 0; i < _size; i++ ) {
        fprintf( out, "%10d%10d", i, i );
        fprintf( out, "\n" );
      }
      fclose(out);
    }
    else {
      OPENERROR(name);
    }
  }
  else if ( ( format == "MARCA" ) || ( format == "MatrixMarket-sparse" ) ) {
    // MARCA and MM formats are 1-based but the index shift is done in the call to FORMAT

#define FORMAT(ii,jj,vv) fprintf( out, "%10d %10d %12e\n", ii, jj, vv );

    string name;
    if ( format == "MARCA" ) {
      name = modelName + ".marca";
    }
    else { // must be Matrix Market
      name = modelName + ".mm";
    }
    FILE* out = fopen( name.c_str(), "w" );
    if ( NULL != out ) {
      fprintf( out, "%6d %6d %6d\n", _size, _size, 3*_size - 2 );
      fprintf( out, "\n" );
      FORMAT( 1, 1, _q+_r );
      FORMAT( 1, 2, _p );
      for ( int i = 1; i < _size-1; i++ ) {
        FORMAT( i+1, i, _q );
        FORMAT( i+1, i+1, _r );
        FORMAT( i+1, i+2, _p );
      }
      FORMAT( _size, _size-1, _q );
      FORMAT( _size, _size, _r+_p );
      fclose(out);
    }
    else {
      OPENERROR(name);
    }
  }
  else if ( format == "Ers" ) {

#undef FORMAT
#define FORMAT(ii,jj,vv) fprintf( out, "%10d %10d %12e\n", ii, jj, vv );

    string name = modelName + ".mcl";
    FILE* out = fopen( name.c_str(), "w" );
    if ( NULL != out ) {
      fprintf( out, "discrete sparse\n" );
      fprintf( out, "%d\n", _size );
      FORMAT( 0, 0, _q+_r );
      FORMAT( 0, 1, _p );
      for ( int i = 1; i < _size-1; i++ ) {
        FORMAT( i, i-1, _q );
        FORMAT( i, i, _r );
        FORMAT( i, i+1, _p );
      }
      FORMAT( _size-1, _size-2, _q );
      FORMAT( _size-1, _size-1, _r+_p );
      fprintf( out, "stop\n" );
      fprintf( out, "%d", (int)rint( _initDistribution->sample() ) );
      fclose(out);
    }
    else {
      OPENERROR(name);
    }
  }
  else if ( format == "PSI3" ) {
    // write several yaml files
    string name = modelName + "_chain.yaml";
    FILE* out = fopen( name.c_str(), "w" );
    if ( NULL != out ) {
      fprintf( out, "%-24s%s\n", "Version:", "1.0" );
      fprintf( out, "\n" );
      fprintf( out, "%-24s%s\n", "MyLib:", "~");
      fprintf( out, "#\n" );
      fprintf( out, "%-24s\n", "Queues:" );
      fprintf( out, "    %-20s%s\n", "- id:", "queue" );
      fprintf( out, "      %-18s%d\n", "min:", 0 );
      fprintf( out, "      %-18s%d\n", "max:", _size-1 );
      fprintf( out, "\n" );
      fprintf( out, "# Table of events\n" );
      fprintf( out, "%-24s\n", "Events:" );
      fprintf( out, "    %-20s%s\n", "- id:", "jumpRight" );
      fprintf( out, "      %-18s%s\n", "type:", "Default$ext_arrival_reject" );
      fprintf( out, "      %-18s%f\n", "rate:", _p );
      fprintf( out, "      %-18s%s\n", "from:", "[outside]" );
      fprintf( out, "      %-18s%s\n", "IndexFctOrigin", "~" );
      fprintf( out, "      %-18s%s\n", "to:", "[drop]" );
      fprintf( out, "      %-18s%s\n", "IndexFctDest", "~" );
      fprintf( out, "      %-18s%s\n", "parameters", "~" );
      fprintf( out, "\n" );
      fprintf( out, "    %-20s%s\n", "- id:", "jumpLeft" );
      fprintf( out, "      %-18s%s\n", "type:", "Default$ext_departure" );
      fprintf( out, "      %-18s%f\n", "rate:", _q );
      fprintf( out, "      %-18s%s\n", "from:", "[outside]" );
      fprintf( out, "      %-18s%s\n", "IndexFctOrigin", "~" );
      fprintf( out, "      %-18s%s\n", "to:", "[drop]" );
      fprintf( out, "      %-18s%s\n", "IndexFctDest", "~" );
      fprintf( out, "      %-18s%s\n", "parameters", "~" );
      fprintf( out, "\n" );
      fprintf( out, "    %-20s%s\n", "- id:", "stay" );
      fprintf( out, "      %-18s%s\n", "type:", "Default$nothing" );
      fprintf( out, "      %-18s%f\n", "rate:", _r );
      fprintf( out, "      %-18s%s\n", "from:", "[outside]" );
      fprintf( out, "      %-18s%s\n", "IndexFctOrigin", "~" );
      fprintf( out, "      %-18s%s\n", "to:", "[drop]" );
      fprintf( out, "      %-18s%s\n", "IndexFctDest", "~" );
      fprintf( out, "      %-18s%s\n", "parameters", "~" );
      fclose(out);
    }
    else {
      OPENERROR(name);
    }
  }
  else if ( format == "R" ) {
    string name = modelName + ".R";
    FILE* out = fopen( name.c_str(), "w" );
    if ( NULL != out ) {
      string str = toString("R");
      fprintf( out, "%s\n", str.c_str() );
      fclose(out);
    }
    else {
      OPENERROR(name);
    }
  }
  else {
    cerr << "Error in Homogenous1DRandomWalk::write(): Format \""
         << format << "\" not handled." << endl;
  }

}
