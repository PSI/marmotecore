ifeq ($(OS),Windows_NT)
CFLAGS += -std=gnu++11
endif


ifdef WITH_R
RFLAGS = -DWITH_R -I/usr/include/R -I/usr/lib64/R/library/Rcpp/include -I/usr/lib64/R/library/RInside/include -L/usr/lib64/R/lib -lR  -L/usr/lib64/R/library/RInside/lib -lRInside
else
RFLAGS = 
endif

#GPPCOMPILER=clang
GPPCOMPILER=g++
 
GTEST_DIR = ./tests/googletest/googletest
GMOCK_DIR = ./tests/googletest/googlemock

CFLAGS = -I${GTEST_DIR}/include/ -I${GMOCK_DIR}/include/ -ansi -Wall -Wformat-security  -g 


DISTRIBS=bernoulliDistribution Distribution \
	uniformDistribution diracDistribution \
	exponentialDistribution discreteDistribution  \
	uniformDiscreteDistribution geometricDistribution \
	poissonDistribution

TRANSITIONS=sparseMatrix sparseMatrix_scc multiDimHomTransition eventMixture transitionStructure
SETS=marmoteSet marmoteBox marmoteInterval binarySimplex binarySequence
MODELS=felsenstein81 homogeneous1DRandomWalk homogeneousMultiDRandomWalk homogeneous1DBirthDeath poissonProcess twoStateContinuous
CORE=markovChain markovChain_lumping simulationResult system
LUMPING=state block 

XBORNE=SOR gthLD
PSI=hbf cost alea alias psiSampler

COREOBJECTS=\
	$(addprefix lib/, $(addsuffix .o, $(DISTRIBS) ) ) \
	$(addprefix lib/, $(addsuffix .o, $(MODELS) ) ) \
	$(addprefix lib/, $(addsuffix .o, $(TRANSITIONS) ) ) \
	$(addprefix lib/, $(addsuffix .o, $(LUMPING) ) ) \
	$(addprefix lib/, $(addsuffix .o, $(SETS) ) ) \
	$(addprefix lib/, $(addsuffix .o, $(CORE) ) )
XBORNEOBJECTS=\
	$(addprefix lib/, $(addsuffix .o, $(XBORNE) ) )
PSIOBJECTS=\
	$(addprefix lib/, $(addsuffix .o, $(PSI) ) )


OBJECTS=$(COREOBJECTS) $(XBORNEOBJECTS) $(PSIOBJECTS)

LIBRARIES=$(addprefix lib/, $(addsuffix .a, \
	libMarmoteCore libXborne libpsi) )




all: objects libraries run-all-tests


objects: $(OBJECTS)

# Making the core lib objects
lib/%.o: Distribution/%.cpp
	$(GPPCOMPILER) $(CFLAGS) -c $< -o $@

lib/%.o: transitionStructure/%.cpp
	$(GPPCOMPILER) $(CFLAGS) -c $< -o $@
	
lib/%.o: lumping/%.cpp
	$(GPPCOMPILER) $(CFLAGS) -c $< -o $@

lib/%.o: Set/%.cpp
	$(GPPCOMPILER) $(CFLAGS) -c $< -o $@

lib/%.o: markovChain/%.cpp
	$(GPPCOMPILER) $(CFLAGS) -c $< -o $@
	
lib/%.o: markovChain/biology/%.cpp
	$(GPPCOMPILER) $(CFLAGS) -c $< -o $@

lib/%.o: tests/%.cpp
	$(GPPCOMPILER) $(CFLAGS) -c $< -o $@

lib/%.o: %.cpp
	$(GPPCOMPILER) $(CFLAGS) -c $< -o $@

lib/%.o: XBORNE/SOR/%.c
	gcc -c $< -o $@

lib/%.o: XBORNE/gthLD/%.c
	gcc -c $< -o $@

#Making the PSI lib objects
lib/%.o: PSI/%.cpp
	$(GPPCOMPILER) -c $(CFLAGS) $< -o $@

libraries: $(OBJECTS)
	ar crv lib/libMarmoteCore.a $(COREOBJECTS)
#	ar crv lib/libPsi.a $(PSIOBJECTS)
	ar crv lib/libpsi.a $(PSIOBJECTS)
	ar crv lib/libXborne.a $(XBORNEOBJECTS)
	
	
	

#testing part


GTEST_HEADERS = $(GTEST_DIR)/include/gtest/*.h \
                $(GTEST_DIR)/include/gtest/internal/*.h
                
GMOCK_HEADERS = $(GMOCK_DIR)/include/gmock/*.h \
                $(GMOCK_DIR)/include/gmock/internal/*.h \
                $(GTEST_HEADERS)
                
                                
                
GTEST_SRCS_ = $(GTEST_DIR)/src/*.cc $(GTEST_DIR)/src/*.h $(GTEST_HEADERS)
GMOCK_SRCS_ = $(GMOCK_DIR)/src/*.cc $(GMOCK_HEADERS)
                

CXXFLAGS += -g -Wall -Wextra -lpthread
CPPFLAGS += -isystem $(GTEST_DIR)/include -isystem $(GMOCK_DIR)/include


UTEST=markovChainTest sparseMatrixTest

TESTS=$(addprefix lib/, $(addsuffix .o, $(UTEST) ) )


LIBRARIES2=$(addprefix -l, MarmoteCore Xborne psi boost_thread boost_filesystem)

gtest-all.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR)  $(CXXFLAGS) -c \
            $(GTEST_DIR)/src/gtest-all.cc

gtest_main.o : $(GTEST_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) $(CXXFLAGS) -c \
            $(GTEST_DIR)/src/gtest_main.cc

gtest_main.a : gtest-all.o gtest_main.o
	$(AR)  $@ $^


gmock-all.o : $(GMOCK_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) -I$(GMOCK_DIR) $(CXXFLAGS) \
            -c $(GMOCK_DIR)/src/gmock-all.cc

gmock_main.o : $(GMOCK_SRCS_)
	$(CXX) $(CPPFLAGS) -I$(GTEST_DIR) -I$(GMOCK_DIR) $(CXXFLAGS) \
            -c $(GMOCK_DIR)/src/gmock_main.cc


gmock_main.a : gmock-all.o gtest-all.o gmock_main.o
	$(AR) $(ARFLAGS) $@ $^

gmock_test.o : $(USER_DIR)/gmock_test.cc $(GMOCK_HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $(USER_DIR)/gmock_test.cc

run-all-tests : $(COREOBJECTS) $(TESTS)  gmock_main.a
	$(CXX)  $(CPPFLAGS) $(CXXFLAGS)  $^ -o run-all-tests -Llib/ $(LIBRARIES2)
	
#end of testing part
	
	
	
	
	
cat := $(if $(filter $(OS),Windows_NT),type,cat)
VERSION := $(shell $(cat) version.txt)
DISTDIR=marmotecore_$(VERSION)

tarball_distrib:
	@/bin/rm -rf $(DISTDIR)
	@/bin/mkdir $(DISTDIR)
	@/bin/mkdir $(DISTDIR)/include
	@/bin/mkdir $(DISTDIR)/include/Distribution
	@/bin/mkdir $(DISTDIR)/include/transitionStructure
	@/bin/mkdir $(DISTDIR)/include/Set
	@/bin/mkdir $(DISTDIR)/include/markovChain
	@/bin/mkdir $(DISTDIR)/include/markovChain/biology
	@/bin/mkdir $(DISTDIR)/lib
	@/bin/mkdir $(DISTDIR)/doc
	@/bin/cp -p *.h $(DISTDIR)/include
	@/bin/cp -p Distribution/*.h $(DISTDIR)/include/Distribution
	@/bin/cp -p transitionStructure/*.h $(DISTDIR)/include/transitionStructure
	@/bin/cp -p Set/*.h $(DISTDIR)/include/Set
	@/bin/cp -p markovChain/*.h $(DISTDIR)/include/markovChain
	@/bin/cp -p markovChain/biology/*.h $(DISTDIR)/include/markovChain/biology
	@/bin/cp -p lib/*.a $(DISTDIR)/lib
	@/bin/cp -p doc/* $(DISTDIR)/doc
ifdef WITH_R
	@/bin/mkdir $(DISTDIR)/lib/R
	@/bin/mkdir $(DISTDIR)/lib/R/uvand
	@/bin/cp -p ../uvand-master/* $(DISTDIR)/lib/R/uvand
endif
	@/bin/sed s/XXXX/$(VERSION)/ Makefile_install > $(DISTDIR)/Makefile
	@tar cvzf $(DISTDIR).tgz ./$(DISTDIR) > /dev/null
	@/bin/rm -rf $(DISTDIR)

distribs:
	@make -i clean
	@make
	@make tarball_distrib
	@/bin/mv $(DISTDIR).tgz $(DISTDIR)_noR.tgz
	@make -i clean
	@make WITH_R=true
	@make tarball_distrib WITH_R=true
	@/bin/mv $(DISTDIR).tgz $(DISTDIR)_R.tgz

clean:
	@/bin/rm -f $(OBJECTS) $(LIBRARIES) $(TESTS) run-all-tests gtest_main.a gtest_main.o gmock_main.a gmock_main.o gtest-all.o 
