#include "system.h"
#include <iostream>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <boost/filesystem.hpp>
#include <string>
#include <algorithm>


 using namespace std;
 namespace fs = ::boost::filesystem;


 std::string system::pwd()
 {

 	fs::path cwd = fs::current_path();
 	//cout<<"path: "<<cwd.generic_string()<<"\n"; 
 	return cwd.generic_string();
 }


/**
 * @brief An auxiliary class for testing equality of file names
 *
 */
 class FileEquals: public std::unary_function<boost::filesystem::path, bool>
 {
 public:
    explicit FileEquals( const boost::filesystem::path& fname ) : file_name(fname) {}
 
    bool operator()( const boost::filesystem::directory_entry& entry ) const
    {
        return entry.path().filename() == file_name;
    }
 
 private:
 
  boost::filesystem::path file_name;
 };



 std::string system::existFile(std::string fileName, std::string rootDirectory)
 {

    boost::filesystem::path fname;
 
    const boost::filesystem::path file = fileName;
    const boost::filesystem::path directory=rootDirectory;

    const boost::filesystem::recursive_directory_iterator end;
    const boost::filesystem::recursive_directory_iterator dir_iter( directory );
 
    const boost::filesystem::recursive_directory_iterator it =
        std::find_if( boost::filesystem::recursive_directory_iterator( directory ),
                      end,
                      FileEquals( fileName ) );
   
    if ( it != end )         
    { 
        fname = it->path();
        return fname.generic_string(); 
    } else
       {
        return fileName+" not found in "+rootDirectory+"\n";
        }

 }


 std::string system::existDirectory(std::string directoryName, std::string rootDirectory)
 {

    boost::filesystem::path fname;
 
    const boost::filesystem::path file = directoryName;
    const boost::filesystem::path directory=rootDirectory;

    const boost::filesystem::recursive_directory_iterator end;
    const boost::filesystem::recursive_directory_iterator dir_iter( directory );
 
    const boost::filesystem::recursive_directory_iterator it =
        std::find_if( boost::filesystem::recursive_directory_iterator( directory ),
                      end,
                      FileEquals(directoryName) );
   
    if ( it != end )         
    { 
        fname = it->path();
        return fname.generic_string(); 
    } else
       {
        return directoryName+" not found in "+rootDirectory+"\n";
        }

 }

 
bool system::hasR()
 {
  struct stat sb;
  std::string delimiter = ":";
  std::string path = std::string(getenv("PATH"));
  size_t start_pos = 0, end_pos = 0;

  while ((end_pos = path.find(':', start_pos)) != std::string::npos)
    {

	//cout<<"HERE: "<<path.substr(start_pos, end_pos - start_pos)<<"\n";
        std::string current_path =
        path.substr(start_pos, end_pos - start_pos) + "/R";
        std::string mathsat_path = current_path;


      if ((stat(mathsat_path.c_str(), &sb) == 0) && (sb.st_mode & S_IXOTH))
        {
          // cout<<"HERE: "<<mathsat_path <<"\n";
          return true;
         }

      start_pos = end_pos + 1;
     }

  return false;
 }


bool system::hasScilab()

 {
  struct stat sb;
  std::string delimiter = ":";
  std::string path = std::string(getenv("PATH"));
  size_t start_pos = 0, end_pos = 0;

  while ((end_pos = path.find(':', start_pos)) != std::string::npos)
    {

	//cout<<"HERE: "<<path.substr(start_pos, end_pos - start_pos)<<"\n";
        std::string current_path =
        path.substr(start_pos, end_pos - start_pos) + "/scilab";
        std::string mathsat_path = current_path;


      if ((stat(mathsat_path.c_str(), &sb) == 0) && (sb.st_mode & S_IXOTH))
        {
          // cout<<"HERE: "<<mathsat_path <<"\n";
          return true;
         }

      start_pos = end_pos + 1;
     }

  return false;
 } 


 std::vector<std::string> system::getFiles(std::string path, std::string extension)
 {
    std::vector<std::string> v;

    const boost::filesystem::path root = path;

    if(!fs::exists(root) || !fs::is_directory(root)) return v;

    fs::recursive_directory_iterator it(root);
    fs::recursive_directory_iterator endit;
    //we can use this directory_iterator to avoid recursivity
    while(it != endit)
    {    
 
        if(fs::is_regular_file(*it) && it->path().extension() == extension) { v.push_back(it->path().filename().string());  }
        ++it;

    }

  return v;
 }



 bool system::isExecutable(std::string name)
  {
    const char *pa = name.c_str();
    int rval = access (pa, X_OK); 
    if (rval == 0) return true; else return false;
  }

 bool system::isReadable(std::string name)
  {
    const char *pa = name.c_str();
    int rval = access (pa, R_OK); 
    if (rval == 0) return true; else return false;
  }

 bool system::isWritable(std::string name)
  {
    const char *pa = name.c_str();
    int rval = access (pa, W_OK); 
    if (rval == 0) return true; else return false;
  }


