
/* Copyright(C) (2002) (ID - IMAG) <Florent.Morata@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 

/*! \file hbf.h
 * \brief Header of HBF Class.
 * \author - Florent.Morata@imag.fr
           - Corine.Marchand@imag.fr
	       - Jean-Marc.Vincent@imag.fr
 * \version November 26, 2002
 */ 

#ifndef _HBF_H_
#define _HBF_H_


#include <vector>
#include <string>
#include <fstream>
#include <cmath>
#include "../transitionStructure/sparseMatrix.h"

/*! \namespace std */
using namespace std;

/*! Iterator of integer vector. */
typedef vector<int>::iterator iteri;
/*! Iterator of double vector. */
typedef vector<double>::iterator iterd;
/*! Type psi_name is used to manipulate name of PSI model. */
typedef string psi_name;

/*! \enum psi_type
* Time type of input markovian model : discrete or continuous.
*/
enum psi_type{continuous,discrete};
	

/*! \class HBF
* \brief Storage of matrix in Harwell Boeing Format.
*
* There exists three possibilities to import a matrix : 
*             - hbf_r : row hbf format.(classical data structure)  
*             - hbf_c : column hbf format. (like PEPS format)
*             - marca : like import file from MARCA software.
*
* The state numerotation can start from zero or from one. 
*/
class HBF{
 private:
	
  /*! Model's name */
  psi_name model_name;
  /*! Time type model */	
  psi_type model_type;	
  /*! States number of the model */
  int dimension;
  /*! Number of non zero elements of generator matrix */
  int nz_number;
  /*! Storage of non zero values */
  vector<double> Val;
  /*! Storage of column indexes */
  vector<int> Col;
  /*! Storage of row beginning */
  vector<int> Row;
  /*! Storage of the non-zero values number by row */
  vector<int> ValbyRow;	
  /*! Largest element (in modulo) of the generator */
  double biggest;

 public:
 
  // static HBF hbf; // elimination of the static part

  // constructors
public:
  HBF();
  HBF( sparseMatrix* mat );
  ~HBF();

public:
  void create(const int d,const int n);
  int size() const;
  int order() const;
  psi_type type() const;
  double maxi() const;
  bool is_empty() const;
  double val(const int i) const;
  int col(const int i) const;
  int row(const int i) const;
  int valbyrow(const int i) const;
  int zero_or_one();
  void update();
  void read_hbf_r(const psi_name path);
  void read_marca(const psi_name path);
  void read_hbf_c(const psi_name path);
  void uniformization();
  void write_unif(const psi_name file);
  void col2row();
  void convert();
  void scale(const double d);
  void give_a_name(const psi_name file);
  psi_name name() const;
  int out_degree() const;
  void transform(const int d);
};


#include "hbf.inl"


#endif

