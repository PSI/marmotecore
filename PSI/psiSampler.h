#ifndef PSISAMPLER_H
#define PSISAMPLER_H

#include "algorithmicSampler.h"
#include "../markovChain/markovChain.h"
#include "alias.h"
#include "cost.h"

/**
 * @brief Abstraction for algorithms that produce samples of some unspecified
 * distribution.
 **/
class psiSampler : public algorithmicSampler {

private:
  Alias* _A;
  Cost* _CostStr;
  int _outDegree;

public:
  /**
   * @brief constructor from a markovChain object
   * @param model the Markov Chain
   */
  psiSampler( markovChain* model );

  /**
   * @brief standard destructor
   */
  ~psiSampler();

  /**
   * @copydoc algorithmicSampler::sample()
   */
  virtual double sample();

  /**
   * @copydoc algorithmicSampler::iidSample()
   */
  virtual void iidSample( int n, double* sample);

  /**
   * @brief Version of iidSample with a cost function.
   * @param n size of the sample
   * @param sample array where to store the sample
   * @param cost array containing the cost function
   */
  void iidSample( int n, double* sample, int* cost );

private:
  /**
   * @brief The backward simulation algorithm for generating iid samples of the
   * stationary distribution. This is the exact simulation kernel of PSI.
   * @param size size of the sample
   * @param smpl array where to store the sample. Must have been allocated to "size".
   * @param times optional array where to store the number of steps needed to obtain the sample.
   * Not used if NULL. If not NULL, must have been allocated to "size".
   * @param cost optional array where to store the cost function from which to sample.
   * If NULL, the index of the state is used as cost. If not NULL, must have been allocated to "size".
   */
  void backward(const int size, double* smpl, int* times, int* cost);


};

#endif // PSISAMPLER_H
