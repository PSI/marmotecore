
/* Copyright(C) (2002) (ID - IMAG) <Florent.Morata@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 

/*! \file cost.cpp
 * \brief Methods of Cost class.
 * \author - Florent.Morata@imag.fr
           - Corine.Marchand@imag.fr
	       - Jean-Marc.Vincent@imag.fr 
 * \version November 26, 2002
 *
 * This version of Cost class considers the full path of input cost files.
 */

// DOXYGEN comments


/*! \fn void Cost::init_state(const int nb)
* \brief Creation of Cout vector without functional.
* \param nb an integer corresponding to the space state dimension.
*/

/*! \fn void Cost::init()
* \brief Initialization of the trajectories.
*/

/*! \fn void Cost::clear()
* \brief Reinitialization of the object.
*/

/*! \fn inline int Cost::value(const int i) const
 * \brief Give the ith value of Class vector.
 *
 * It be useful to MCMC simulation to estimate a functional of stationary distribution.
 */

/*! \fn inline int Cost::value() const
* \brief Give the first element of Class vector.
*
* Its public method is used to obtain the generate value of each realization.
*/

/*! \fn inline int Cost::value_next(const int i) const
 * \brief Give the ith element of Class_next vector.
 */
 
/*! \fn inline int Cost::value_next() const
 * \brief Give the first element of Class_next vector.
 *
 * Its public method is used to obtain the generate value of each realization in Forward simulation.
 */

/*! \fn void Cost::swap()
* \brief Update of the trajectories after each CFTP iteration.
*/

/*! \fn bool Cost::test()
* \brief Test the global backward trajectories coupling.
* \return true if there is coupling, false otherwise.
*/

/*! \fn bool Cost::test_next()
 * \brief Test the global forward trajectories coupling.
 * \return true if there is coupling, false otherwise.
 */

/*! \fn void Cost::free_cl()
* \brief Memory liberation of simulation vector.
*/

/*! \fn void Cost::change(const int i,const int j)
* \brief Modification of the ith Class_next element with the value of the jth element of Class vector.
*
* This public method is used to generate next state of trajectories by the Walker aliasing algorithm. 
* \param i an integer
* \param j an integer
*/

/*! \fn void Cost::mk_cost(int* cost, int sz)
* \brief Constucting the cost vector from an array
* \param cost an array where costs are stored
* \param sz the size of this array
*/

/*! \fn int Cost::nb_cl()
* \brief Give the class (according to the study functional) number.
*/

#ifndef _COST_CPP_
#define _COST_CPP_


#include "cost.h"

//-----------------------------------------------------------------------------------------
// Recycling of the init() function as constuctor: apparently not a good idea.
Cost::Cost()
{
  // This instruction appears to be useless since it happens before the vectors
  // are initialized, and these are not pointers.
  Class = Class_next = Cout;
}

void Cost::init()
{
  // copy cost vector in Class vectors
  Class = Cout;
  Class_next = Cout;
}

//-----------------------------------------------------------------------------------------

void Cost::init_state(const int nb)
{
  for(int i = 0; i < nb; i++) Cout.push_back(i);
}

//-----------------------------------------------------------------------------------------

void Cost::clear()
{
  Cout.clear();
  Class.clear();
  Class_next.clear();
}

//-----------------------------------------------------------------------------------------

void Cost::swap()
{
  Class = Class_next;
}

//-----------------------------------------------------------------------------------------

bool Cost::test()
{
  bool continu = false;
  
  for(unsigned int i = 0; i < Class.size(); i++)
    {
      continu = continu || (Class[i] != Class[0]); 
    }
  
  return continu;
}

//-----------------------------------------------------------------------------------------

bool Cost::test_next()
{
  bool continu = false;
  
  for(unsigned int i = 0; i < Class_next.size(); i++)
    {
      continu = continu || (Class_next[i] != Class_next[0]); 
    }
  
  return continu;
}

//-----------------------------------------------------------------------------------------

void Cost::free_cl()
{
  Class.clear();
  Class_next.clear();
}

//-----------------------------------------------------------------------------------------

void Cost::change(const int i,const int j)
{
  Class_next[i] = Class[j] ;
}

//-----------------------------------------------------------------------------------------

void Cost::mk_cost(int* cost, int sz)
{
  for ( int i = 0; i < sz; i++ ) {
    Cout.push_back(cost[i]);
  }
  
  // Cout.pop_back();
}

//-----------------------------------------------------------------------------------------

int Cost::nb_cl()
{
  int maxi = 0;
	
  for(unsigned int i = 0; i < Cout.size(); i++)
	if(maxi < Cout[i]) maxi = Cout[i];  
	
  return (maxi + 1);
}

//-----------------------------------------------------------------------------------------

#endif



