
/* Copyright(C) (2002) (ID - IMAG) <Florent.Morata@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 

/*! \file hbf.cpp
 * \brief Methods of HBF Class.
 * \author - Florent.Morata@imag.fr
           - Corine.Marchand@imag.fr
           - Jean-Marc.Vincent@imag.fr
 * \version November 26, 2002		   
 */


// DOXYGEN comments


/*! \fn HBF::HBF() 
* \brief Empty constructor.
*/ 

/*! \fn void HBF::create(const int d,const int n)
* \brief Full constructor with input parameters. 
* \param d an integer corresponding to the dimension.
* \param n an integer corresponding to the non-null values number.
*/ 

/*! \fn HBF::~HBF() 
* \brief Destructor. 
*/

/** \fn void HBF::update()
* \brief Transformation of HBF numerotation.
*
* PSI numerotation begin from zero and so needs a modification for models started from one.
*/

/*! \fn void HBF::read_hbf_r(const psi_name path)
* \brief Reading of a 'row' HBF model. 
* \param path an input psi_name path.
*/

/*! \fn void HBF::read_marca(const psi_name path)
* \brief Reading of a MARCA model. 
* \param path an input psi_name path.
*/

/*! \fn void HBF::read_hbf_c(const psi_name path)
* \brief Reading of a PEPS model. 
* \param path an input psi_name path.
*/

/*! \fn void HBF::uniformization()
* \brief Uniformization of a continuous time model.
*
* \f$\pi Q=0 \Leftrightarrow \pi P=\pi\f$.
*/

/*! \fn void HBF::write_unif(const psi_name file)
* \brief Writing of a unif file in HBF (classical) format.
* \param file a string corresponding to the output file.
*/

/*! \fn void HBF::col2row()
* \brief Conversion of a column HBF model to a row HBF model.
*/

/*! \fn void HBF::convert()
* \brief Writing of a col2row() conversion. 
*/

/*! \fn void HBF::scale(const double d)
* \brief Multiplication of matrix by a double input value.
*/

/*! \fn int HBF::out_degree() const
* \brief Give the maximum out_degree of matrix. 
*/

/*! \fn void HBF::transform(const int d)
* \brief Width transformation of matrix. 
* \param d an integer corresponding to the new matrix width.
*/

/*! \fn inline bool HBF::is_empty() const
* \brief Test if a current HBF structure exists.
* \return true if an HBF structure is in memory, false otherwise.
*/

/*! \fn inline int HBF::size() const
* \brief Give the non-zero values number of matrix.
*/

/*! \fn inline int HBF::order() const
* \brief Give the dimension of matrix.
*/

/*! \fn inline double HBF::maxi() const
* \brief Give the largest element (in modulo) of continuous time matrix. 
*/

/*! \fn inline double HBF::val(const int i) const
* \brief Give the ith non-zero value of matrix.
*/

/*! \fn inline int HBF::col(const int i) const
* \brief Give the ith column index.
*/

/*! \fn inline int HBF::row(const int i) const
* \brief Give the ith row beginning index of matrix.
*/

/*! \fn inline int HBF::valbyrow(const int i) const
* \brief Give the non-null values number of the ith row.
*/

/*! \fn inline int HBF::zero_or_one()
* \brief Test the numerotation of input model. 
*
* This method permit to the PSI user to construct his model with numerotation choice.
* \return 0 if the input model start from zero and 1 if the input model start from one.
*/

/*! \fn void HBF::give_a_name(const psi_name file)
* \brief Baptise HBF storage. 
* \param file an input psi_name.
*/

/*! \fn inline psi_name HBF::name() const
* \brief Give the current model's name. 
*/

/*! \fn inline psi_type HBF::type() const
* \brief Give the current model type. 
*
* PSI accepts discrete and continuous time model.
*/


#ifndef _HBF_CPP_
#define _HBF_CPP_

#include <stdlib.h>
#include "hbf.h"

//----------------------------------------------------------------------------------------------------

HBF::HBF() 
{
  dimension = 0;
  nz_number = 0;
}

// Method to construct a HBF from a sparse Matrix. Code adapted from that
// of HBF::read_marca()
//
HBF::HBF(sparseMatrix* mat)
{
  double maxi = 0;

  int NbRow = mat->origSize(); // rows number
  // int NbCol = mat->destSize(); // columns number, not used
  int n = 0; // non zero values number
  for ( int i = 0; i < mat->origSize(); i++ ) {
    n += mat->getNbElts(i);
  }

  create(NbRow,n);
  // give_a_name(path); // not really useful to name the HBF

  // Scanning all entries of the matrix
  for ( int i = 0; i < mat->origSize(); i++) {

    ValbyRow.push_back( mat->getNbElts(i) );

    for ( int j = 0; j < mat->getNbElts(i); j++ ) {

      int ent = mat->getCol( i, j );
      Col.push_back(ent);
      double value = mat->getEntryByCol( i, j );
      fprintf( stdout, "+++ value at row %2d num %2d col %2d = %8.4f\n", i, j, ent, value );

      if( maxi < fabs(value) )
        maxi = fabs(value);

      Val.push_back(value);
    }

  }

  // Post-processing
  Row.push_back(0);
  for(int i = 1; i < dimension + 1; i++)
    Row.push_back(ValbyRow[i-1] + Row[i-1]);

  model_type = discrete;

  biggest = maxi;

}
//----------------------------------------------------------------------------------------------------


void HBF::create(const int d,const int n)
{
  // elimination of static features
  // if( (dimension != d) || (nz_number != n) )
    {
      dimension = d;
      nz_number = n;
    }		
}

//----------------------------------------------------------------------------------------------------


HBF::~HBF()   
{
  if(dimension != 0)
    {
      dimension = nz_number = 0;
      Col.clear();
      Val.clear();
      Row.clear();
      ValbyRow.clear();
    }
}

//----------------------------------------------------------------------------------------------------

void HBF::update()
{
  if(zero_or_one() == 1)
    {
      for(int i = 0; i < nz_number; i++)
	Col[i] --;
      for(int i = 0; i < dimension + 1; i++)
	Row[i] --;
    }
}

//----------------------------------------------------------------------------------------------------


void HBF::read_hbf_r(const psi_name path)
{
  double value;
  double maxi = 0;
  int d,n,ent,aux;
  
  ifstream fp(path.c_str());
  fp >> d;
  fp >> n;
  
  create(d,n);
  give_a_name(path);
  
  for(int i = 0; i < nz_number; i++) // non zero values of the generator
    {
      fp >> value;
      Val.push_back(value);
			  
      if(maxi < fabs(value)) maxi = fabs(value);
    }
  
  biggest = maxi;
  
  for (int i = 0; i < nz_number ; i++) // column indexes
    {
      fp >> ent;
      Col.push_back(ent); 
    }
  
  fp >> ent;
  Row.push_back(ent);  
  
  for(int i = 1; i < dimension + 1; i++) 
    {
      fp >> aux;
      Row.push_back(aux);      
      ValbyRow.push_back(aux - ent); 
      ent = aux;
    }
 
  fp.close();
	
	if(*(Val.end()-1) < 0) model_type = continuous;
		else model_type = discrete;
  
}

//----------------------------------------------------------------------------------------------------


void HBF::read_marca(const psi_name path)
{
  ifstream fp(path.c_str());
  double value;
  double maxi = 0;	
  int in,ent,NbRow,NbCol,n;
  int compt = 0;
  int out = 1;
  
  fp >> NbRow; // rows number
  fp >> NbCol; // columns number
  fp >> n; // non zero values number	
 	
  create(NbRow,n);
  give_a_name(path);
  	
  for(int i = 0; i < nz_number; i++)
    {
      fp >> in;
	  fp >> ent;
      Col.push_back(ent-1); // conversion for a numerotation from 0
      fp >> value;  
		
      if( maxi < fabs(value) ) maxi = fabs(value); 
      Val.push_back(value);
	  
      
	  if (out == in) 
	  {
	   compt ++; // number of non zero values by row
	   out = in;
	  
	     if(i == nz_number - 1) ValbyRow.push_back(compt);  
	  }
	  
      else 
	  {
	    ValbyRow.push_back(compt);
	    compt = 1;
	    out = in;
	  }
      
    }

	fp.close();
	
	
  Row.push_back(0);
  for(int i = 1; i < dimension + 1; i++)
    Row.push_back(ValbyRow[i-1] + Row[i-1]);

  
  bool continu = true;
  int count = 0;
  
  while( (continu == true) || (count < nz_number)) // type of the marca model
  {
   for(int i = 0; i < nz_number; i++)
    {
	   if(Val[i] < 0) continu = false;
		   count ++;
    }
   
  }	  
  
  if(continu == false) model_type = continuous;
	  else model_type = discrete;

  biggest = maxi;
}

//----------------------------------------------------------------------------------------------------


void HBF::read_hbf_c(const psi_name path)
{
  ifstream fp(path.c_str());
  double value;
  double maxi = 0;	
  int ent,d,n;
  
  fp >> d;
  fp >> n;
  
  create(d,n);
  give_a_name(path);	
  
  for(int i = 0; i < nz_number; i++) // non zero values of the generator
    {
      fp >> value;
      Val.push_back(value);
		
      if(maxi < fabs(value)) maxi = fabs(value);
    }
  
  biggest = maxi;
  
  for (int i = 0; i < nz_number ; i ++) // column indexes
    {
      fp >> ent;
      Col.push_back(ent);  
    }
  
  
  for(int i = 0; i < dimension + 1; i++) 
    {
      fp >> ent;
      Row.push_back(ent);       
    }
	
	fp.close();
	
	
	if(*(Val.end()-1) < 0) model_type = continuous;
		else model_type = discrete;
	
}

//----------------------------------------------------------------------------------------------------


void HBF::uniformization()
{
  iterd Diag;
  iteri newcol;  
  iteri itCol = Col.begin(); 
  iterd itVal = Val.begin(); 
	
  int compt_max = 0; // counting of apparition of biggest value
  
  for(int i = 0; i < dimension; i++)
    {		
      double sumrow = 0; // counting of sum p(i,j)  i != j
      
      for(int j = 0; j < ValbyRow[i];  j++)
	{
       if(*itVal> 0.0) // non diagonal element
         {
           *itVal /= biggest;
            sumrow += *itVal;
         }
       else
         {
           Diag = itVal; // saving the iterators of the diagonal element for line i 
           newcol = itCol; 
         }
       
       itVal ++; // we continue with line i in vector Val & Col
       itCol++;
       
	}
      
      if(*Diag == -1*biggest) // diagonal value is the biggest element
	{
	  Val.erase(Diag);     
	  itVal --;
	  Col.erase(newcol);
	  itCol --;
	  compt_max ++;
	  nz_number --;
	  ValbyRow[i] --;
	  Row[i+1] -= compt_max; 
	}
      
      else
	{
	  *Diag = 1 - sumrow ; // diagonal value isn't the biggest element
	  Row[i+1] -= compt_max; 
	}
	
  }
  
}

//----------------------------------------------------------------------------------------------------


void HBF::write_unif(const psi_name file)
{
    string path = file + ".unif";
    ofstream fp(path.c_str());
  
    fp << "################################################" << endl;
    fp << "#               PSI 2002                       #" << endl;
    fp << "#                                              #" << endl;
    fp << "#      HBF format of the discreted process     #" << endl;
    fp << "#                                              #" << endl;
    fp << "################################################" << endl;
    fp << " " << endl;
    fp << " " << endl;
  
    fp << " Non-null values of the transition matrix :  " << endl;
    fp << " " << endl;
  
    for(int i = 0; i < nz_number; i++)
    {
      fp << Val[i] << " , ";
    }
  
    fp << " " << endl;
    fp << " " << endl;
	
    fp << " Column indexes : " << endl;
    fp << " " << endl;
  
  
    for(int i = 0; i < nz_number; i++)
    {
      fp << Col[i] << " , " ;
    }
  
  fp << " " << endl;
  fp << " " << endl;
  fp<< " Beginning row indexes : " << endl;
  fp << " " << endl;
  
  for(int i = 0; i < dimension + 1; i++)
    {
      fp << Row[i] << " , " ; 
    }
}

//----------------------------------------------------------------------------------------------------


void HBF::col2row()
{
  vector<double> newVal(nz_number);
  vector<int> newCol(nz_number);
  vector<int> newRow(dimension + 1);
  
  for(int i = 0; i < nz_number; i++)  // counts the elements by rows
    newRow[Col[i]+1]++;
  
  for(int i = 1; i < dimension; i++)
    newRow[i+1] += newRow[i];
  
  for(int i = 0; i < dimension; i++)  // copy of the elements
    
    for(int j = Row[i]; j < Row[i+1]; j++)
      {
		newCol[newRow[Col[j]]] = i;
		newVal[newRow[Col[j]]] = Val[j];
		newRow[Col[j]]++;
      }
  
  
  for(int i = dimension-1; i >= 0; i--) 
		newRow[i+1] = newRow[i];
  
  newRow[0] = 0;
  
  
  for(int i = 0; i < dimension; i++)      // construction of ValbyRow vector
    ValbyRow.push_back(Row[i+1]-Row[i]);
  
  
  Val.clear();
  Val = newVal;
  Col.clear();
  Col = newCol;
  Row.clear();
  Row = newRow;
}

//----------------------------------------------------------------------------------------------------

void HBF::convert()
{
  string path = model_name.substr(0,model_name.find('.')) + ".hbf_r";
  ofstream fp(path.c_str());
  
  col2row();
  fp << dimension << " " << nz_number << endl;
	
  for(int i = 0; i < nz_number; i ++)
    fp << Val[i] << endl;
  for(int i = 0; i < nz_number; i++)
    fp << Col[i] << " " ;
  
  fp << " " << endl;
  
  for(int i = 0; i < dimension + 1; i++)
    fp << Row[i] << " " ;
  
  fp.close();
}

//----------------------------------------------------------------------------------------------------

void HBF::scale(const double d)
{
  for(int i = 0 ; i < nz_number; i++)
    Val[i]*= d;
} 

//----------------------------------------------------------------------------------------------------

int HBF::out_degree() const
{
  int deg = 0;
  
  for(int i = 0; i < dimension; i++)
    if(deg < ValbyRow[i]) deg = ValbyRow[i];
  
  return (deg);
}

//----------------------------------------------------------------------------------------------------

void HBF::transform(const int d)
{
  vector<double> NewVal;
  
  iterd it = Val.begin();
  
  for(int i = 0; i < dimension; i++)
    {
      for(int j = 0; j < ValbyRow[i]; j++){
	   NewVal.push_back(*it);
       it++;}
		  
      for(int j = ValbyRow[i]; j < d; j++)
	   NewVal.push_back(0.0);
      
    }
  
  Val.clear();
  Val = NewVal;			
}

//----------------------------------------------------------------------------------------------------

// HBF HBF::hbf=HBF();


//----------------------------------------------------------------------------------------------------

 #endif
















