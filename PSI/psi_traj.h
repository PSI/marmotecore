#ifndef PSI_TRAJ_H
#define PSI_TRAJ_H


// C++ includes
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>

// C include
#include <cstdlib>

/*! \namespace std
 * \brief Utilization of the Standard Template Library.
 */
using namespace std;

// Unix include
#include <unistd.h>

// local includes
#include "alea.h"
#include "alias.h"
#include "cost.h"
#include "hbf.h"

void help_traj();
void version_traj();
void traj(const string path,const string file_out,int state,const int length);
int main_psi_traj(int argc,char **argv);


#endif // PSI_TRAJ_H
