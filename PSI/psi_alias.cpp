

/* Copyright(C) (2002) (ID - IMAG) <Florent.Morata@imag.fr>

*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */


#include"psi_alias.h"

// Global variable
/*! Executable name. */
char *prog_alias=NULL;


//-------------------------------------------------------------------------------------------------------------

/*! \fn void precomputation(const string path,const string file_out,const string ext)
 * \brief Precomputation of PSI.
 *
 * This is the preliminary treatment for perfect simulation :
 * - HBF construction.
 * - Uniformization (if necessary).
 * - Alias construction.
 * \param path a string corresponding to the model path.
 * \param file_out a string corresponding to new (optional) model name.
 * \param ext a string corresponding to the model extension :
 *                                                            - ".hbf_r"
 *                                                            - ".hbf_c"
 *                                                            - ".marca"
 */



void precomputation(const string path,const string file_out,const string ext)
{

  Alias A;

  if(ext == ".hbf_r")  // different kind of input files
    {
      HBF::hbf.read_hbf_r(path);
      HBF::hbf.update();
    }

  if(ext == ".hbf_c")
 {

   HBF::hbf.read_hbf_c(path);
   HBF::hbf.col2row();

 }

  if(ext == ".marca")
    {
      HBF::hbf.read_marca(path);

    }

  if (HBF::hbf.type() == continuous) HBF::hbf.uniformization(); // uniformization

  HBF::hbf.transform(HBF::hbf.out_degree()); // format dimension*dmax

  A.init(HBF::hbf.out_degree()); // first step of aliasing construction

  A.mk_walker(HBF::hbf.out_degree()); // Walker's construction

  A.transpose(HBF::hbf.out_degree());  // transposition of tables

  A.write_construct(file_out,HBF::hbf.out_degree()); // writing of aliasing arrays (by column)

  cout << " File : '" << file_out << ".simu' created ...\n";
}

//-------------------------------------------------------------------------------------------------------------


/*! \fn void help_alias()
 * \brief Help message of psi_alias executable.
 */
void help_alias()
{
  cerr << " USAGE : " << prog_alias << " [-io] argument [-hv]\n";
  cerr << "\n  -i : input model (full path)\n";
  cerr << "\n  -o : output file (automatically generate with 'simu' extension)\n";
  cerr << "\n       By default, output file has the same name than input file.\n";
  cerr << "\n  -h : help\n";
  cerr << "\n  -v : version\n";
  cerr << " " << endl;
}


//-------------------------------------------------------------------------------------------------------------

/*! \fn void version_alias()
 * \brief Give PSI informations.
 */
void version_alias()
{
  cout << " P.S.I. version 1.0 - Aliasing coding\n";
  cout << " Authors : Florent.Morata@imag.fr\n";
  cout << "           Corine.Marchand@imag.fr\n";
  cout << "           Jean-Marc.Vincent@imag.fr\n";
  cout << " Web : http://www-id.imag.fr\n\n";
}


//-------------------------------------------------------------------------------------------------------------

/*! \fn int main_psi_alias(int argc,char **argv)
 * \brief PSI Precomputation Main Program.
 *
 * Unix like version of PSI.
 */
int main_psi_alias(int argc,char **argv)
{
  char c;
  string ext;
  prog_alias = argv[0];
  string opt_o,opt_i; // input and output options
  int compt_o = 0;
  int compt_h = 0;
  int compt_i = 0;

  cout << " " << endl;
  while((c = getopt(argc,argv,"hvi:o:")) != -1)
    {
      switch(c)
    {
    case 'h': help_alias(); // help option
      compt_h ++;
      compt_i ++; // to avoid help message repetition
      break;

    case 'v': version_alias();
      compt_h ++;
      compt_i ++; // to avoid help message repetition
      break;

    case 'i' : opt_i = optarg;
      compt_i ++;
      ext = opt_i.substr(opt_i.find('.'),opt_i.length() - opt_i.find('.')); // ext contains extension model file
      break;

    case 'o' : opt_o = optarg;
      compt_o ++; // utilization of -o option
      break;
    }
    }

  if((c == -1) && (compt_i == 0)) // no option specified
    {
      help_alias();
      compt_h++;
    }
  else
    {

      // PSI Precomputation called
      if(compt_h == 0)
    {
      if(compt_o == 1) precomputation(opt_i,opt_o,ext); // output specified
      else
        {
          int pos = opt_i.find('.'); // index of '.' character in input path
          precomputation(opt_i,opt_i.substr(0,pos),ext);
        }
    }

    }

  return 0;
}

//-------------------------------------------------------------------------------------------------------------







