
/* Copyright(C) (2002) (ID - IMAG) <Florent.Morata@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 


/*! \file hbf.inl
 * \brief Inline methods of HBF Class.
 * \author - Florent.Morata@imag.fr
           - Corine.Marchand@imag.fr
	       - Jean-Marc.Vincent@imag.fr
 */ 
 
 
 #ifndef _HBF_INL_
 #define _HBF_INL_
 
 
 #include "hbf.h"
 
 
 //----------------------------------------------------------------------------------------------------------------------------
 
 
 inline int HBF::size() const
{
  return (nz_number);

}

 //----------------------------------------------------------------------------------------------------------------------------


inline int HBF::order() const
{
  return (dimension);
}

 //----------------------------------------------------------------------------------------------------------------------------


inline double HBF::maxi() const
{
  return (biggest);
}

 //----------------------------------------------------------------------------------------------------------------------------


inline double HBF::val(const int i) const
{
  return (Val[i]);
}

 //----------------------------------------------------------------------------------------------------------------------------


inline bool HBF::is_empty() const
{
  return (dimension == 0);
}

 //----------------------------------------------------------------------------------------------------------------------------


inline int HBF::col(const int i) const
{
  return (Col[i]);
}

 //----------------------------------------------------------------------------------------------------------------------------

inline int HBF::row(const int i) const
{
 return (Row[i]);
}

 //----------------------------------------------------------------------------------------------------------------------------

inline int HBF::valbyrow(const int i) const
{
  return (ValbyRow[i]);
}

 //----------------------------------------------------------------------------------------------------------------------------

inline int HBF::zero_or_one()
{
  return (Row[0]);
}

 //----------------------------------------------------------------------------------------------------------------------------

inline psi_name HBF::name() const
{
  return (model_name);
}

 //----------------------------------------------------------------------------------------------------------------------------

inline psi_type HBF::type() const
{
	return(model_type);
}

 //----------------------------------------------------------------------------------------------------------------------------

inline void HBF::give_a_name(const psi_name file)
{
  model_name = file;
}

 //----------------------------------------------------------------------------------------------------------------------------

#endif

