#ifndef ALGORITHMICSAMPLER_H
#define ALGORITHMICSAMPLER_H

/**
 * @brief Abstraction for algorithms that produce samples of some unspecified
 * distribution.
 **/
class algorithmicSampler {

  public:
  virtual ~algorithmicSampler() {}

  /**
   * @brief drawing a (pseudo)random value according to the distribution.
   *
   * @return a sample
   */
  virtual double sample() = 0;

  /**
   * @brief drawing an i.i.d. sample from the distribution. The result is returned
   * in an array (that must have been already allocated) passed as a parameter.
   * The Distribution class offers the default implementation with repeated call to
   * sample().
   *
   * @param n the number of values to sample
   * @param sample an array to be filled with the sample
   */
  virtual void iidSample( int n, double* sample) = 0;

};
#endif // ALGORITHMICSAMPLER_H
