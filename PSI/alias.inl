
/* Copyright(C) (2002) (ID - IMAG) <Florent.Morata@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 

/*! \file alias.inl
 * \brief Inline methods of Alias class.
 * \author - Florent.Morata@imag.fr
           - Corine.Marchand@imag.fr
	       - Jean-Marc.Vincent@imag.fr
 */
 
 
 #ifndef _ALIAS_INL_
 #define _ALIAS_INL_
 
 
 #include "alias.h"
 
 //-----------------------------------------------------------------------------------
 
 
 inline double Alias::threshold(const int i) const
{
	return (S[i].t);
}

 //-----------------------------------------------------------------------------------

inline int Alias::col(const int i) const
{
	return (S[i].col);
}

 //-----------------------------------------------------------------------------------

inline int Alias::alias(const int i) const
{
	return (S[i].alias);
}

 //-----------------------------------------------------------------------------------

inline int Alias::size() const
{
  return (S.size());
}

 //-----------------------------------------------------------------------------------

#endif



