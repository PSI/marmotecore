
/* Copyright(C) (2002) (ID - IMAG) <Florent.Morata@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 
 
/*! \file alias.h
* \brief Header of Alias class.
* \author - Florent.Morata@imag.fr
          - Corine.Marchand@imag.fr
	      - Jean-Marc.Vincent@imag.fr
*/


#ifndef _ALIAS_H_
#define _ALIAS_H_

#include <vector>
#include <string>
#include <fstream>
#include <cmath>

/*! \namespace std */
using namespace std;

#include "../transitionStructure/sparseMatrix.h"
#include "hbf.h"

/*! Permit to test if a double value equal to 1.0. */
const double EPS = 1e-12;

/*! Iterator of integer vector. */
typedef vector<int>::iterator iteri;
/*! Iterator of double vector. */
typedef vector<double>::iterator iterd;

/*! Type Threshold to manipulate double value in AliasStruct. */
typedef double Threshold;
/*! Type Index to manipulate integer value in AliasStruct. */
typedef int Index;


/*! \class Alias
*\brief Aliasing data.
*
* Walker's algorithm is used to generate internal transitions in the simulation kernel.
*/
class Alias{
 private:
	/*! \struct AliasStruct
	* \brief Data structure to avoid a contiguous representation of aliasing data.
	*/
  struct AliasStruct{
	/*! Reject threshold. */  
    Threshold t;
	/*! Column index : standard value. */  
    Index col;
	/*! Alias index : second value. */  
    Index alias;
  };
  
  /*! Type stock to manipulate AliasStruct in PSI processes. */
  typedef struct AliasStruct stock;
  /*! Storage of aliasing arrays. */	  
  vector<stock> S;
  /*! The transition matrix, as HBF object */
  HBF* _matrix;
  // Constructors
 public:
  /**
   * @brief Contructor of an alias table from a transitionStructure/sparseMatrix object.
   * @param mat the transition structure of the Markov Chain
   */
  Alias( sparseMatrix* mat );

  /**
    * @brief Standard destructor
    */
  ~Alias();

 public:
  
  int size() const;
  double threshold(const int i) const;
  int col(const int i) const;
  int alias(const int i) const;
  void init(const int d);
  void mk_walker(const int d);
  void transpose(const int d);
  int read(const string path);
  void write_construct(const string file_name,const int d);
  int walk(const double rand,const int j,const int n,const int d,const int i);
};



#include "alias.inl"


#endif

