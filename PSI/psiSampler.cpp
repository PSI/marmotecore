

/* Copyright(C) (2002) (ID - IMAG) <Florent.Morata@imag.fr> 
 */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 


/*! \file psiSampler.cpp
 * \brief Adaptation to marmoteCore of the PSI Simulation Main.
 * \author - Florent.Morata@imag.fr
 *         - Corine.Marchand@imag.fr
 *         - Jean-Marc.Vincent@imag.fr
 *         - Alain Jean-Marie
 * \version November 26, 2002 - December 2015
 */
 
#include "psiSampler.h"
#include "../transitionStructure/sparseMatrix.h"

 // C++ includes
 #include <iostream>
 #include <string>
 #include <vector>
 #include <algorithm>
 #include <fstream>
 
 // C include 
 #include <cstdlib>
 
 /*! \namespace std
  * \brief Utilization of the Standard Template Library.
  */
 using namespace std;
 
 
 // Unix include 
 #include <unistd.h>
 
 // local includes
 #include "alea.h"
 #include "alias.h"
 #include "cost.h"
 #include "hbf.h"

// constructor
psiSampler::psiSampler(markovChain* model )
{
  sparseMatrix *mat = static_cast<sparseMatrix*>( model->generator() );

  // compute outdegree
  _outDegree = 0;
  for ( int i = 0; i < mat->origSize(); i++ ) {
    if ( mat->getNbElts(i) > _outDegree ) _outDegree = mat->getNbElts(i);
  }

  // create alias table and cost structure (unused as of now)
  _A = new Alias( mat );
  _CostStr = new Cost();

  // trace
  _A->write_construct( "alias", _outDegree );
}

// destructor
psiSampler::~psiSampler()
{
  delete _A;
  delete _CostStr;
}

 //--------------------------------------------------------------------------------------------------------------

void psiSampler::backward(const int size, double* smpl, int* times, int* cost)
{

  int D = _outDegree;      // D is the maximal number of transitions from states
  int N = _A->size() / D;

  // Clear cost vector in case it has already been initialized. Will be really
  // useful when cost can be specified to the method
  _CostStr->clear();
  if(cost != (int*)NULL)
    _CostStr->mk_cost(cost,N);
  else
    _CostStr->init_state(N);
  
  
  init_generator(); // initialization of random generator

  int d = 0; // current index of sample
  
  while(d != size)
    {
      _CostStr->init();
      int stop_time = 0;
      
      do
	{
	  stop_time++;
	  
	  double rand = uniform();   // Random called
	  int j = (int) (D*uniform());
	  
	  for(int i = 0; i < N; i++)         
            _CostStr->change(i,_A->walk(rand,j,N,D,i)); // Aliasing generation of (backward) trajectories
	  
          _CostStr->swap(); // cost vectors update
	  
        } while(_CostStr->test() == true);
      
      // storing the result
      smpl[d] = _CostStr->value();
      if ( (int*)NULL != times ) {
        times[d] = stop_time;
      }

      // cleanup, next sample
      _CostStr->free_cl();
      d++;
      
    }

}

//--------------------------------------------------------------------------------------------------------------


/*! \fn void help_sample()
 * \brief Help message of psi_sample executable.
 */
void help_sample()
{
  cerr << " USAGE :  [-idoc] argument [-hv]\n";
  cerr << "\n  -i : input 'simu' file (full path)\n";
  cerr << "\n  -d : sample size\n";
  cerr << "\n  -o : output file (automatically generate with 'sample' extension)\n";
  cerr << "\n       By default, output file has the same name than input file.\n";
  cerr << "\n  -c : input cost file (full path)\n";
  cerr << "\n       If -c option is not specified, cost function is the space states.\n";
  cerr << "\n  -h : help.\n";
  cerr << "\n  -v : version\n"; 
  cerr << endl;
}

void psiSampler::iidSample( int n, double* sample )
{ 	
  backward( n, sample, (int*)NULL, (int*)NULL ); // no optional arguments: no costs and no collection of times
}

void psiSampler::iidSample(int n, double* sample, int* cost )
{
  backward( n, sample, (int*)NULL, cost ); // no optional arguments: no costs and no collection of times
}

double psiSampler::sample()
{
  double res;

  iidSample( 1, &res );

  return res;
}







