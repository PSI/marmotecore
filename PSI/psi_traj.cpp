
/* Copyright(C) (2002) (ID - IMAG) <Florent.Morata@imag.fr>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

 /*! \file psi_traj.cpp
  * \brief PSI  Trajectory Simulation Main.
  * \author - Florent.Morata@imag.fr
  *         - Corine.Marchand@imag.fr
  *         - Jean-Marc.Vincent@imag.fr
  * \date December 11, 2002.
  */


#ifdef HAVE_CONFIG_H
#include "config.h"
#endif


#include"psi_traj.h"

 // global variable
 /*! Executable name. */
 char *prog_traj;


 //--------------------------------------------------------------------------------------------------------------


 /*! \fn void help_traj()
 * \brief Help message of psi_traj executable.
 */
void help_traj()
{
  cerr << " USAGE : " << prog_traj << " [-iosl] argument [-hv]\n";
  cerr << "\n  -i : input 'simu' file (full path)\n";
  cerr << "\n  -o : output file (automatically generate with 'traj' extension)\n";
  cerr << "\n       By default, output file has the same name than input file.\n";
  cerr << "\n  -s : initial state\n";
  cerr << "\n       If -s option is not specified, initial state is the first state.(number 0)\n";
  cerr << "\n  -l : length of the trajectory.\n";
  cerr << "\n  -h : help.\n";
  cerr << "\n  -v : version\n";
  cerr << " " << endl;
}


//--------------------------------------------------------------------------------------------------------------

/*! \fn void version_traj()
 * \brief Give PSI informations.
 */
void version_traj()
{
  cout << " P.S.I. version 1.0 - Trajectory Simulation\n";
  cout << " Authors : Florent.Morata@imag.fr\n";
  cout << "           Corine.Marchand@imag.fr\n";
  cout << "           Jean-Marc.Vincent@imag.fr\n";
  cout << " Web : http://www-id.imag.fr\n\n";
}

//--------------------------------------------------------------------------------------------------------------

/*! \fn void traj(const string path,const string file_out,int state,const int length)
 * \brief PSI Trajectory Simulation.
 *
 * This modulate permits to obtain a trajectory of Markov chains with choice of initial state.
 * \param path a string corresponding to the model name.
 * \param file_out a string corresponding to the (optional) output sample path.
 * \param state an integer corresponding to the initial state of the trajectory.
 * \param length an integer corresponding to the length of the trajectory.
 */
void traj(const string path,const string file_out,int state,const int length)
{
  Alias A;
  Cost C;
  int stop_time;
  ofstream out;

  string path_out = file_out + ".traj"; // output path
  out.open(path_out.c_str());

  // aliasing data reading
  int D = A.read(path);
  int N = A.size() / D;

  C.init_state(N); // cost vector creation


  out << "################################################### " << endl;
  out << "#                PSI 2002                         # " << endl;
  out << "#                                                 # " << endl;
  out << "#            Trajectory simulation                # " << endl;
  out << "#                                                 # " << endl;
  out << "#   cost + reject threshold + column index        # " << endl;
  out << "################################################### " << endl;
  out << " " << endl;
  out << " " << endl;
  out << " Initial state : " << state << endl;
  out << " Length of trajectory : " << length << endl;
  out << " " << endl;

  stop_time = 0;
  C.init();

  do
    {
      stop_time++;
      double rand = uniform();   // Random called
      int j = (int) (D*uniform());
      state = C.value(A.walk(rand,j,N,D,state));
      out << state << " " << rand << " " << j << endl;

    } while(stop_time < length);

  C.free_cl();
  out.close();
  cout << " " << endl;
  cout << "\n       :-) Final state : " << state << endl;
  cout << "\n       :-) See '" << path_out << "' file for complete trajectory. ";
  cout << " " << endl;

}

//--------------------------------------------------------------------------------------------------------------


 /*! \fn int main_psi_traj(int argc,char **argv)
  * \brief PSI Trajectory Simulation Main Program.
  *
  * Unix like version of PSI trajectory simulation.
  */
int main_psi_traj(int argc,char **argv)
{
  char c;
  string ext,opt_i,opt_o;
  char *opt_s,*opt_l;
  prog_traj = argv[0];
  int compt_o = 0;
  int compt_s = 0;
  int compt_h = 0;
  int compt_i = 0;
  int compt_l = 0;


  cout << " " << endl;
  while((c = getopt(argc,argv,"hvi:o:s:l:")) != -1)
    {
      switch(c)
     {
     case 'h': help_traj();
       compt_h ++;
       compt_i ++; // to avoid repetition help message
       break;

     case 'v': version_traj();
       compt_h ++;
       compt_i ++; // to avoid repetition help message
       break;

     case 'i' : opt_i = optarg;
       compt_i ++;
       break;

     case 'o' : opt_o = optarg;
       compt_o ++; // utilization of -o option
       break;

     case 's' : opt_s = optarg;
       compt_s ++; // utilization of -s option
       break;

     case 'l' : opt_l = optarg;
       compt_l ++; // utilization of -l option
       break;

     }
    }

  if((c == -1) && (compt_i == 0)) // no option specified
    {
      help_traj();
      compt_h++;
    }

  else
    {
      if(compt_h == 0)  // trajectory simulation called
    {
      if(compt_o == 0 && compt_s == 0) traj(opt_i,opt_i.substr(0,opt_i.find('.')),0,atoi(opt_l)); // no optional argument
      if(compt_o == 0 && compt_s == 1) traj(opt_i,opt_i.substr(0,opt_i.find('.')),atoi(opt_s),atoi(opt_l)); // -s argument
      if(compt_o == 1 && compt_s == 0) traj(opt_i,opt_o,0,atoi(opt_l)); // -o argument
      if(compt_o == 1 && compt_s == 1) traj(opt_i,opt_o,atoi(opt_s),atoi(opt_l)); // -o & -s arguments
    }

    }

  exit(0);
}


 //--------------------------------------------------------------------------------------------------------------














