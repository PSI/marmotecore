

#ifndef _PSI_ALIAS_H_
#define _PSI_ALIAS_H_

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

// C include
#include <cstdlib>
#include <unistd.h>

// local includes
#include "alias.h"
#include "hbf.h"

using namespace std;


void precomputation(const string path,const string file_out,const string ext);

void help_alias();

void version_alias();
int main_psi_alias(int argc,char **argv);

#endif



