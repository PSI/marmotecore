
/* Copyright(C) (2002) (ID - IMAG) <Florent.Morata@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 

/*! \file cost.h
 * \brief Header of Cost file.
 * \author - Florent.Morata@imag.fr
           - Corine.Marchand@imag.fr
	       - Jean-Marc.Vincent@imag.fr
 */

#ifndef _COST_H_
#define _COST_H_

#include <vector>
#include <algorithm>
#include <string>
#include <fstream>

/*! \namespace std */
using namespace std;


/*! \class Cost
* \brief Simulation data for CFTP algorithm.
*/
class Cost{
	
public:
  Cost();
  ~Cost() {}

private:
  /*! Storage of trajectory values at time -n*/	
  vector<int> Class;
  /*! Storage of trajectory values at time -n+1 */	
  vector<int> Class_next;
  /*! Storage of space states cost function */ 	
  vector<int> Cout;
  
public:
  void init();
  void init_state(const int nb);
  void clear();
  int value() const;
  int value(const int i) const;
  int value_next() const;
  int value_next(const int i) const;	
  void swap();
  bool test();
  bool test_next();
  void free_cl();
  void change(const int i,const int j);
  void mk_cost(int* cost, int sz);
  int nb_cl();
};



#include "cost.inl"


#endif

