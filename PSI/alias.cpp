
/* Copyright(C) (2002) (ID - IMAG) <Florent.Morata@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
 

/*! \file alias.cpp
 * \brief Methods of Alias class. 
 * \author - Florent.Morata@imag.fr
           - Corine.Marchand@imag.fr
 *         - Jean-Marc.Vincent@imag.fr 
 *
 * \date November 26, 2002
 */

// DOXYGEN comments 


/*! \fn inline int Alias::size() const
* \brief Give the size of aliasing array.
*/

/*! \fn inline double Alias::threshold(const int i) const
* \brief Give the threshold corresponding to the ith aliasing array index.
*/

/*! \fn inline int Alias::col(const int i) const
* \brief Give the column index corresponding to the ith aliasing array index.
*/

/*! \fn inline int Alias::alias(const int i) const
* \brief Give the alias index corresponding to the ith aliasing array index.
*/

/*! \fn void Alias::init(const int d)
* \brief Initialization of aliasing tables.
* \param d an integer corresponding to the matrix width.
*/

/*! \fn void Alias::mk_walker(const int d)
 *\brief Construction of Walker array.
 *
 * This array is built by rows and provides a representetion of the Markov chain to simulate.
 * \param d an integer corresponding to the matrix width.
 */

/*! \fn void Alias::transpose(const int d)
 * \brief Transposition of Walker array.
 * \param d an integer corresponding to the matrix width.
 * We obtain by this way a contiguous representation for the simulation step.
 */

/*! \fn void Alias::write_construct(const string file_name,const int d)
 * \brief Writing of definitive Walker array (stored by column).
 * \param file_name a string corresponding to the name of the output file '.simu'.
 * \param d an integer corresponding to the matrix width.
 */

/*! \fn int Alias::read(const string file)
* \brief Reading of aliasing data for simulation step.
* \param file a string corresponding to the name of the input file '.simu'. 
*/

/*! \fn int Alias::walk(const double rand,const int j,const int n,const int d,const int i)
* \brief Walker's algorithm for discrete distributions. 
* \param rand a double value according to uniform law on [0;1].
* \param j an integer corresponding to a random choice of column index. 
* \param n an integer corresponding to the space state dimension.
* \param d an integer corresponding to the matrix width.
* \param i an integer between 0 and N-1 ( where N:= matrix dimension) being the current cursor.
*/

#ifndef _ALIAS_CPP_
#define _ALIAS_CPP_


#include "alias.h"

// Constructor + initialization
Alias::Alias(sparseMatrix* mat)
{
  _matrix = new HBF( mat );
  // HBF::hbf = matrix;

  // Code from psi_alias.cpp
  _matrix->transform(_matrix->out_degree()); // format dimension*dmax

  init(_matrix->out_degree()); // first step of aliasing construction

  mk_walker(_matrix->out_degree()); // Walker's construction

  transpose(_matrix->out_degree());  // transposition of tables

  // write_construct(file_out,matrix->out_degree()); // writing of aliasing arrays (by column)

}

// Destructor
Alias::~Alias()
{
  delete _matrix;
}

//------------------------------------------------------------------------------------------------

void Alias::init(const int d)  
{
  stock s;
  vector<int> Tmp(_matrix->size()); // temporary storage of Col vector
	
        for(int i = 0; i < _matrix->size(); i++)
                Tmp[i] = _matrix->col(i);
	
	iteri it = Tmp.begin();
  
  for(int i = 0 ; i < _matrix->order()*d ; i++)  // Initialisation des seuils
    {
      s.t = _matrix->val(i) * d;
      
      if(s.t > 0.0) {s.col = *it; it++;}
      
      else s.col = -1;
      
      s.alias = -1;
      
      S.push_back(s);
    }
}

//------------------------------------------------------------------------------------------------

void Alias::mk_walker(const int d)
{
  int l,h,curseur_l;
  vector<int> L,H;
  int n = S.size()/d; 
  
	
  for (int i = 0; i < n; i++) // row by row treatment
    {
      curseur_l = 0;
     
      for (int j = 0; j < d ; j++)
	{
	  if (S[j+(i*d)].t > 1.0 )  H.push_back(j);
	  else L.push_back(j);
	}
      
      while (! H.empty() )
	{
	  l = L[curseur_l];
	  h= H[0];
	  
	  S[l+(i*d)].alias = S[h+(i*d)].col  ; // valeur alias
	  S[h+(i*d)].t += S[l+(i*d)].t - 1; // abaissement du seuil
	  
	  if (fabs(S[h+(i*d)].t -1) < EPS)  
	    {
	      S[h+(i*d)].t = 1.0;
	      H.erase(H.begin());
	    }
	  else
	    {
	      if ( S[h+(i*d)].t < 1.0)  
		  {
		    H.erase(H.begin());
		    L.push_back(h);
		  }
	      
	    }
	  curseur_l++;
      }
      
      L.clear();  // Suppression des vector de construction  L & H
      H.clear();
		
	}
  
}

//------------------------------------------------------------------------------------------------

void Alias::transpose(const int d)  
{
  vector<stock> St; // Temporary vector of transposed data
  stock s;
  int n = S.size()/d;
  
  for(int i = 0; i < d ; i++)
    {
      for(int j = 0 ; j < n ; j++)
	{
	  s.t = S[d*j+i].t ;
	  s.col = S[d*j+i].col ;
	  s.alias = S[d*j+i].alias ;
	  St.push_back(s);
	}
    }  

	S.clear();
	S = St; // Copy in S
}

//------------------------------------------------------------------------------------------------

void Alias::write_construct(const string file_name,const int d)
{
  string path;
  path = file_name + ".simu";
  
  ofstream fp(path.c_str());
  
  fp << S.size()/d << " " ;
  fp << d << endl;
  fp << endl;
  
  for(unsigned int i = 0 ; i < S.size(); i++)
    {
      fp << S[i].t << " " << S[i].col << " " << S[i].alias << endl; 
    }
  
  fp.close();
}

//------------------------------------------------------------------------------------------------

int Alias::read(const string path)
{
  int ent1,ent2,N,D;
  double value;
  ifstream fp(path.c_str());

  fp >> N ;
  fp >> D;
  stock s;

  for (int i = 0; i < D*N ; i++)
    {
      fp >> value;
      s.t = value;
      fp >> ent1;
      s.col = ent1;
      fp >> ent2;
      s.alias = ent2;
      S.push_back(s);
    }
  
  fp.close();
 
	return D;
}

//------------------------------------------------------------------------------------------------


int Alias::walk(const double rand,const int j,const int n,const int d,const int i)
{
  int ind = n*j+i; // current index
  
  if( (rand <= S[ind].t)) return (S[ind].col);   
  else return (S[ind].alias);     
}

//------------------------------------------------------------------------------------------------

#endif




