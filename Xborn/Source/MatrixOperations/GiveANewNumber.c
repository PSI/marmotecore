/* Give a new number for reordering the state space 
 We only create the permutation to apply
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "const.h"

/* The model state */ 

typedef int Etat[NEt];

#include "funnum.c"

long *bloc;
long *et;

void  usage()
{
    printf("usage : GiveANewNumber -f filename  \n");
	printf("filename.cd and filename.sz must exist before \n");
	printf("filename.renum will be created \n");
	exit(1);
}

void  ProblemeMemoire()
{
	printf("Sorry, not enougth memory for the vectors \n");
	exit(1);
}

int main(argc, argv)
int argc;
char *argv[];
{
    long NSommets, NArcs;
    long i, j, k, blocmax, b, nb;
    FILE *pf1 ;
    char s1[20];
    char s3[20];
    char s4[20];
    char s5[20];

	if (argc!=3) usage();
	--argc;
	++argv;
	if (**argv != '-') usage();
	switch (*++*argv) {
		case 'f' :  /* get the model name */
		{++argv;
            strcpy(s1,*argv);
            ++argv;             
            /* add .sz, abd .cd */
            strcpy(s4,s1);
            strcpy(s3,s1);
            strcpy(s5,s1);
            
            strcat(s3,".sz");
            strcat(s4,".cd");
            strcat(s5,".renum");
            
            /* does it exist ? */
            if ((pf1=fopen(s3,"r"))==NULL)
			{
                usage();printf("Problem with the .sz file\n");			
			}
            fclose(pf1);
            if ((pf1=fopen(s4,"r"))==NULL)
			{
                usage();printf("Problem with the .cd file\n");
			}
            fclose(pf1);
            break;
		}
		default	 : usage();
	}
    
    /*
     Get the sizes in filename.sz
     */
    
    pf1=fopen(s3,"r");
    fscanf(pf1,"%ld\n", &NArcs);
    fscanf(pf1,"%ld\n", &NSommets);
    fclose(pf1);	
    
    printf("Gettin the size of the model\n");
    
    /* 
     Object creation or exit exit(1) in case of memory pb
     */
    if (!(bloc=(long *)calloc(NSommets, sizeof(long)))) ProblemeMemoire();
    if (!(et=(long *)malloc(NEt*sizeof(long)))) ProblemeMemoire();

    for (k = 0; k < NSommets; k++) {
        bloc[k]=-1;
    }
    printf("Allocation completed \n");
    
    /* 
     get the state number (j) and the stade description  (et)
     */ 
    pf1=fopen(s4,"r");
    blocmax=0;
    for (k = 0; k < NSommets; k++) {
        fscanf(pf1,"%ld", &j); 
        for (i = 0; i < NEt; i++)
      	{	fscanf(pf1,"%ld",&nb );
      		et[i] = nb;
      	} 
        getc(pf1);
        
        b = NumerodeBlock(et, k);
        
        if (b<0)
  	  	{
            printf("the number must be positive \n");
            exit(1);
  	  	}
        if (b>=NSommets)
  	  	{
            printf("the number must be smaller than the number of nodes, %ld %ld \n",b,j);
            exit(1);
  	  	}
        
        if (bloc[k]!=-1) {
            printf("Warning, number %ld is allready used",b);
        }
        bloc[k]=b;
        if (b>blocmax) {blocmax=b;}
    }
    
    fclose(pf1);
    printf("Generation of new numbers is now completed \n");
        
    pf1=fopen(s5,"w");
    for (k = 0; k < NSommets; k++) {
        fprintf(pf1,"%ld \n ", bloc[k]);
    }
    fclose(pf1);
    exit(0);
}



