
/*---------------------------------------------*/
/*                                             */
/*partie de Code specifique a  chaque probleme */
/*                                             */
/*---------------------------------------------*/

/* matrice avec transition de taux connu 
mais destination aleatoire dans le triangle inf avec 
retour a zero sur et surdiagonale sure */



/* codage des evenements */
/* 1 : retour a 0  */
/* 2 : +1 */
/* 3 au dernier  : aleatoire entre 0 et i+1 quand on est a l'etat i */

void InitEtendue()
{
  Min[0] = 0;
  Max[0] = 100000;
}


void EtatInitial(E)
long *E;
{
  /*donne a E la valeur de l'etat racine de l'arbre de generation*/
  E[0] = 0;
  E[1] = 0;
}



double Probabilite(indexevt, E)
long indexevt;
long *E;
{
  /*
  retourne la probabilite d'apparition de l'evenement indexevt
  */
  double p1;
  switch (indexevt) {
  case 1:
    resi = (double)((rand()%100)/100);
    
    resi = resi/10.0;
    p1 = 0.1 + resi  ;
    break;
  case 2:
    p1 = 0.2;
    break;
    default:
    p1 = (0.7-resi)/(NbEvtsPossibles-2);
    break;
  }
  return(p1);
}


void Equation(E, indexevt, F, R)
long *E;
long indexevt;
long *F, *R;
{
  /*ecriture de l'equation d'evolution, transformation de E en F grace
  a l'evenemnt indexevt, mesure de la recompense R sur cette transition*/
  F[0] = E[0];
  switch (indexevt) {
   case 1: F[0]=0;
    break;
   case 2:  if (E[0]<Max[0]) {F[0]++;
       }
       break;
   default: F[0]= rand() % (E[0]+1);
   
    break;
  }
}


  
void InitParticuliere()
{
    srand (0); 
}

