#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <math.h>
#include "verif_ext_fic.h"

unsigned long int *degc;

FILE *Fin;
FILE *Fout;

int main (int argc, char *argv[])
{
char *nomout;
char *nomeclate;
char *nomtrie;
char *extout;
int qqchoseafaire;
pid_t pid;

int lnom, ROW;
unsigned long int N, nval, nbvall;
unsigned long int *degre;

unsigned long int i,j;
unsigned long int indl, indc;
unsigned long int ind1, ind2;
double proba;
unsigned long int c;
int premier;

if (argc != 3) 
	{
	fprintf(stderr,"usage: %s file.Abc Xyz\n",argv[0]);  
	usage_extension(argv[0]); 
	exit(1);
	}
if (!verif_ext_fic(argv[1],"Uuu") || (strlen(argv[2]) != 3))
	{
	fprintf(stderr,"usage: %s file.Abc Xyz\n",argv[0]);  
	usage_extension(argv[0]); 
	exit(1);
	}
extout = argv[2];
if (((extout[0] != 'R') && (extout[0] != 'C') && (extout[0] != 'U')) ||
	((extout[1] != 'i') && (extout[1] != 'd') && (extout[1] != 'u')) ||
	((extout[2] != 'i') && (extout[2] != 'd') && (extout[2] != 'u')))
	{
	fprintf(stderr,"usage: %s file.Abc Xyz\n",argv[0]);  
	usage_extension(argv[0]); 
	fprintf(stderr,"          Xyz must be of the form \"Uuu\"\n");
    fprintf(stderr,"              U means R, C or U\n");
    fprintf(stderr,"              u means i, d or d\n");
	exit(1);
	}
if (extout[0] == EXT[0]) ROW = 1; else ROW = 0;
qqchoseafaire = 0;
if ((extout[0] != 'U') && (extout[0] != EXT[0]))
	qqchoseafaire = 1;
if ((extout[1] != 'u') && (extout[1] != EXT[1]))
	qqchoseafaire = 1;
if ((extout[2] != 'u') && (extout[2] != EXT[2]))
	qqchoseafaire = 1;
if (!qqchoseafaire)
	{
	fprintf(stderr,"Nothing to do!\n");
	exit(0);
	}

lnom = strlen(NOMBASE);
// La matrice en en sortieument
nomout = (char *)malloc((lnom+5)*sizeof(char));
if (nomout == NULL) {perror("malloc nomout"); exit(3);}
sprintf(nomout,"%s.%s",NOMBASE,extout);
printf("nomout = %s\n",nomout);

// Lecture de la taille et du nombre de transitions
Fin = fopen(NOMsz,"r");
if (Fin == NULL) {perror("fopen .sz"); exit(2);}
fscanf(Fin,"%ld",&nval);
fscanf(Fin,"%ld",&N);
fclose(Fin);
printf("N = %ld, nval = %ld\n",N,nval);

// Tableau des degres entrants
degre = (unsigned long int *)calloc(N,sizeof(int));
if (degre == NULL) {perror("malloc degre"); exit(3);}


// La matrice des donnees
Fin = fopen(NOM,"r");
if (Fin == NULL) {perror("fopen NOM"); exit(2);}

//la matrice eclatee transition par transition
nomeclate = (char *)malloc((lnom+10)*sizeof(char));
if (nomeclate == NULL) {perror("malloc nomeclate"); exit(3);}
sprintf(nomeclate,"%s.eclate",NOMBASE);
Fout = fopen(nomeclate,"w");
if (Fout == NULL) {perror("fopen nomeclate"); exit(2);}

printf("%s -> %s (One transition per line)\n",NOM,nomeclate);
for (i=0;i<N;i++)
	{
	fscanf(Fin,"%ld",&indl);
	fscanf(Fin,"%ld",&nbvall);
//printf("indl = %ld, nbvall = %ld\n",indl,nbvall);
	for (j=0;j<nbvall;j++)
		{
		fscanf(Fin,"%le",&proba);
		fscanf(Fin,"%ld",&indc);
		if (indc > N) exit(5);
		if (ROW) {
				 if (extout[1] == 'd') ind1 = N-1-indl; else ind1 = indl;
				 if (extout[2] == 'd') ind2 = N-1-indc; else ind2 = indc;
				 (degre[indl])++;
				 }
		   else  {
				 if (extout[1] == 'd') ind1 = N-1-indc; else ind1 = indc;
				 if (extout[2] == 'd') ind2 = N-1-indl; else ind2 = indl;
				 (degre[indc])++;
				 }
		fprintf(Fout,"%ld %010ld %.15le\n",ind1,ind2,proba);
		}
	}
fclose(Fout);
fclose(Fin);

nomtrie = (char *)malloc((lnom+10)*sizeof(char));
if (nomtrie == NULL) {perror("malloc"); exit(3);}
sprintf(nomtrie,"%s.tri",NOMBASE);
pid = fork();
if (pid == -1) {perror("fork"); exit(4);}
if (pid == 0) 
	{
	printf("%s -> %s (Sorting)\n",nomeclate,nomtrie);
	execlp("sort","sort","-n","-o",nomtrie,nomeclate,NULL);
	}
wait(NULL);
unlink(nomeclate);
printf("%s is now erased\n",nomeclate);
//for (i=0;i<N;i++) printf("%ld ",degc[i]);
//printf("\n");

//###################
//RELECTURE
Fin = fopen(nomtrie,"r");
if (Fin == NULL) {perror("fopen nomtrie"); exit(2);}
//nomout = (char *)malloc((lnom+10)*sizeof(char));
//if (nomout == NULL) {perror("malloc"); exit(3);}

//sprintf(nomout,"%s_T.dt",argv[1]);
Fout = fopen(nomout,"w");
if (Fout == NULL) {perror("fopen nomout"); exit(2);}
if (extout[0] == 'R') printf("%s -> %s (One line per line)\n",nomtrie,nomout);
	else printf("%s -> %s (One column per line)\n",nomtrie,nomout);
premier = 1;
c = -1;
for (i=0;i<nval;i++)
	{
	fscanf(Fin,"%ld %ld %le",&ind1,&ind2,&proba);
	if (extout[1] == 'd') ind1 = N-1-ind1;
	if (extout[2] == 'd') ind2 = N-1-ind2;
	if (ind1 != c) 
		{
		c=ind1; 
		if (!premier) fprintf(Fout,"\n");
			else premier = 0;
		fprintf(Fout,"%ld %ld",c,degre[c]);
		}
	fprintf(Fout," %.15e %ld",proba,ind2);
	}
fprintf(Fout,"\n");
fclose(Fin);
fclose(Fout);
unlink(nomtrie);
printf("%s is now erased\n",nomtrie);

printf("\n%s contains %s in %s format\n",nomout,NOM,extout);
exit (0);
}
