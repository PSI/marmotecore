
/*---------------------------------------------*/
/*                                             */
/*partie de Code specifique a  chaque probleme */
/*                                             */
/*---------------------------------------------*/

/* Hypercube de dimension NEt pour test de lumpabilite  */
/* codage des evenements */
/* 1  : transition 0->1 composant  1 */
/* 2  : transition 1->0 composant  1 */
/* 3 : idem evt 1 mais pour composant 2 */
/* 4 : idem evt 2 mais pour composant 2 */
/* de 5 a 2*NEt : idem mais pour le composant j   */

void InitEtendue()
{
    int ii;
    for (ii=0; ii< NEt; ii++) { 
        Min[ii] = 0;
        Max[ii] = 1;
    }
}


void EtatInitial(E)
int *E;
{
    int ii;
    /*donne a E la valeur de l'etat racine de l'arbre de generation*/
    for (ii=0; ii< NEt; ii++) { 
        E[ii] = 0;
    }
}

double Probabilite(indexevt, E)
int indexevt;
int *E;
{
    /*
     retourne la probabilite d'apparition de l'evenement indexevt
     */
    double p1;
    if ((indexevt%2)==0) {p1 = 0.6/NEt;
    } 
    else {p1=0.4/NEt;
    }
    return(p1);
}

void Equation(E, indexevt, F, R)
int *E;
int indexevt;
int *F, *R;
{
    int ii,jj;
    for (ii=0; ii< NEt; ii++) { 
        F[ii] = E[ii];
    }
    ii = (indexevt-1)/2;
    jj = indexevt%2;
    if (jj==0) {F[ii] = 0;} 
    else {F[ii] = 1;} 
}

void InitParticuliere()
{
    
}
