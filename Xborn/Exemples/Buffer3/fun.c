
void InitEtendue()
{
    Min[0] = 0;
    Max[0] = 10;
    Min[1] = 0;
    Max[1] = 10;
    Min[2] = 0;
    Max[2] = 10;
}


void EtatInitial(E)
int *E;
{
    E[0] = 0;
    E[1] = 0;
    E[2] = 0;
}


double pro1(inx)
int  inx;
{
    double p1;
    switch (inx) {
        case 0:
            p1 = 0.5;
            break;
        case 1:
            p1 = 0.3;
            break;
        case 2:
            p1 = 0.2;
            break;
    }
    return(p1);
}

double pro2(inx)
int  inx;
{
    double p2;
    switch (inx) {
        case 0:
            p2 = 0.3;
            break;
        case 1:
            p2 = 0.4;
            break;
        case 2:
            p2 = 0.3;
            break;
    }
    return(p2);
}

double pro3(inx)
int inx;
{
    double p3;
    if (inx==1) {p3=0.5;} else  {p3=0.5;} 
    return(p3);
}

double pro4(inx)
int inx;
{
    double p4;
    if (inx==1) {p4=0.8;} else  {p4=0.2;} 
    return(p4);
}

double Probabilite(indexevt, E)
int indexevt;
int *E;
{
   double p1,p2,p3,p4;
    double p;
    int a1,a2,r1,r2;
    int i,k;
    /* description de l'evenement en 2 groupes d'arrivees et 2 routages */
    i=(indexevt-1)%4; k=(indexevt-1)/4;
    a1=k%3;  a2=k/3; r1=i%2; r2=i/2;    
    /* Calcul des probabilites elementaires et Produit */
    p1= pro1(a1);
    p2= pro2(a2);
    p3= pro3(r1);
    p4= pro4(r2);
    p=p1*p2*p3*p4;
    return p;
}


void Equation(E, indexevt, F, R)
int *E;
int indexevt;
int *F, *R;
{
    /*variables d'etats :
     0) nombre de clients A
     1) nombre de clients B
     2) nombre de clients X
     */    
    int a1,a2,r1,r2;
    int i,k,ar;
    /* description de l'evenement en 2 groupes d'arrivees et 2 routages */
    i=(indexevt-1)%4; k=(indexevt-1)/4;
    a1=k%3;  a2=k/3; r1=i%2; r2=i/2;
    
    ar =0;
    if (E[0]>0) {
        F[0]=E[0]-1+a1; 
        ar=r1;
    }
    else {F[0]=a1;} /* entree de la file 1 et sortie vers la file X */
    
    if (E[1]>0) {
        F[1]=E[1]-1+a2;
        ar=ar+r2;
    }
    else {F[1]=a2;} /* entree de la file 2 et sortie vers la file X */

    if (E[2]>0) {F[2]=E[2]-1+ar;} else {F[2]=ar;}

    /* gestion des débordements dans les files */ 
    for (i = 0; i < NEt; i++) {
        if (F[i]>Max[i]) {R[i]=F[i]-Max[i]; F[i]=Max[i];}
        else {R[i]=0;}
    }
}



void InitParticuliere()
{
}
