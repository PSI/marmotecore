/*
 Les deux fonctions fournies par l'utilisateur pour
 calculer des recompenses non classiques 
 a partir de la distribution stationnaire 
 et des marginales 
 */

/* taux de perte pour le buffer 1 et 2 */ 
/* Uniquement si le Batch est de taille 2 */
void MarginaleMesCalculs(comp,size)
int comp;
int size;
{
    long double pr,loss;
    switch (comp) {
        case 0:
            pr=pro1(2);
            loss=marginale[size-1]*pr;
            fprintf(pf1,"perte Buffer 0 : %Lg \n",loss);  
            break;
        case 1:
            pr=pro2(2);
            loss=marginale[size-1]*pr;
            fprintf(pf1,"perte Buffer 1 : %Lg \n", loss);  
            break;
        case 2:
            break;
    }
}

/* perte de client dans le buffer 3
 si buffer 3 plein et buffer 1 non vide et buffer 2 non vide 
 et routage des deux clients sortis vers le buffer 3
 */
void GlobalMesCalculs(numberofstates)
int numberofstates;
{
    long double pr,loss;
    int i, i1,i2,i3, size;
    size=Max[2];
    pr = pro3(1)*pro4(1);
    loss=0.0L;
    for (i = 0; i < numberofstates; i++) {
        /* states of the three queues */ 
        i1 = et[i*NEt]; 
        i2 = et[i*NEt+1];
        i3 = et[i*NEt+2];
        if ((i1>0) && (i2>0) && (i3==size)) {
            /* file 1  non vide, file 2 non vide et file 3 pleine */ 
            fprintf(pf1,"p: %d %d %Lf %Lf \n",size, i , pr, pi[i]);  
            loss+=pi[i]*pr;
        }
    }
    fprintf(pf1,"perte BUFFER 3  : %Lg \n",loss);  
}
