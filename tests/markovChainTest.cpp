/* Marmote is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 Marmote is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Marmote. If not, see <http://www.gnu.org/licenses/>.

 Copyright 2015 Alain Jean-Marie, Jean-Michel Fourneau, Jean-Marc Vincent, Issam Rabhi */

#include "../markovChain/markovChain.h"
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <iostream>

class MockSparseMatrix: public sparseMatrix {
 public:
  MockSparseMatrix(int sz) :  sparseMatrix(sz) {   }
  MOCK_METHOD0(period, int());
};


class MarkovChainTest : public ::testing::Test {
 protected:
  virtual void SetUp() {
    chain = new markovChain(15, DISCRETE);

    sparseMatrix * sm = new sparseMatrix(15);
    chain->setGenerator(sm);

    chain->generator()->setEntry(0, 1, 1.0);
    chain->generator()->setEntry(1, 0, 1.0);

    chain->generator()->setEntry(1, 2, 1.0);

    chain->generator()->setEntry(2, 3, 1.0);
    chain->generator()->setEntry(3, 4, 1.0);
    chain->generator()->setEntry(4, 2, 1.0);

    chain->generator()->setEntry(4, 11, 1.0);

    chain->generator()->setEntry(5, 6, 1.0);
    chain->generator()->setEntry(6, 7, 1.0);
    chain->generator()->setEntry(7, 8, 1.0);
    chain->generator()->setEntry(8, 9, 1.0);
    chain->generator()->setEntry(9, 5, 1.0);

    chain->generator()->setEntry(7, 10, 1.0);

    chain->generator()->setEntry(7, 14, 1.0);

    chain->generator()->setEntry(10, 11, 1.0);
    chain->generator()->setEntry(11, 12, 1.0);
    chain->generator()->setEntry(12, 13, 1.0);
    chain->generator()->setEntry(13, 10, 1.0);
  }
  markovChain * chain;

};

TEST_F(MarkovChainTest, Isaccesible) {
  EXPECT_TRUE(chain->isaccessible(0, 11));
  EXPECT_FALSE(chain->isaccessible(0, 7));
}



TEST_F(MarkovChainTest, Isirreducible) {
  EXPECT_FALSE(chain->isirreducible());
}

TEST_F(MarkovChainTest, periodCalled) {
  markovChain m(1, DISCRETE);
  MockSparseMatrix * sm = new MockSparseMatrix (1);
  m.setGenerator(sm);
  EXPECT_CALL(*sm, period()).Times(::testing::AtLeast(1));
  m.period();
}
